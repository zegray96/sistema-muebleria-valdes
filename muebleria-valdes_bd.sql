-- phpMyAdmin SQL Dump
-- version 5.0.2
-- https://www.phpmyadmin.net/
--
-- Servidor: 127.0.0.1
-- Tiempo de generación: 15-12-2020 a las 14:27:00
-- Versión del servidor: 10.4.13-MariaDB
-- Versión de PHP: 7.4.7

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de datos: `sistema_cobro`
--

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `clientes`
--

CREATE TABLE `clientes` (
  `id_cliente` bigint(20) NOT NULL,
  `apellido_nombre` varchar(50) NOT NULL,
  `dni` varchar(10) NOT NULL,
  `domicilio` varchar(60) NOT NULL,
  `telefono` varchar(60) NOT NULL,
  `barrio` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Volcado de datos para la tabla `clientes`
--

INSERT INTO `clientes` (`id_cliente`, `apellido_nombre`, `dni`, `domicilio`, `telefono`, `barrio`) VALUES
(1, 'Acuña Maria', '12.345.673', 'Nada', 'nada', 'Nada'),
(2, 'Perez Marta Perez Marta Perez Marta Perez Marta Pe', '21.821.211', 'Av San Martin 1382', '3769324212', 'Residencial'),
(3, 'Escobar Pablo', '34.123.415', 'Belgrano 1457', '3764325779', 'Santa Rita');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `configuracion`
--

CREATE TABLE `configuracion` (
  `id_configuracion` bigint(20) NOT NULL,
  `nombre_empresa` varchar(40) NOT NULL,
  `descripcion_corta_empresa` varchar(70) NOT NULL,
  `telefono` varchar(30) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Volcado de datos para la tabla `configuracion`
--

INSERT INTO `configuracion` (`id_configuracion`, `nombre_empresa`, `descripcion_corta_empresa`, `telefono`) VALUES
(1, 'Muebleria Valdes', 'Venta de Muebles y Electrodomésticos Art. del Hogar en gral.', '3764715655');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `cuotas`
--

CREATE TABLE `cuotas` (
  `id_venta` bigint(20) NOT NULL,
  `fecha_vencimiento` date NOT NULL,
  `nro_cuota` tinyint(2) NOT NULL,
  `estado` varchar(30) NOT NULL,
  `id_ultima_mod` bigint(20) DEFAULT NULL,
  `fecha_hora_ultima_mod` datetime DEFAULT NULL,
  `observacion` varchar(100) NOT NULL DEFAULT '',
  `monto_adicional` double NOT NULL DEFAULT 0
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Volcado de datos para la tabla `cuotas`
--

INSERT INTO `cuotas` (`id_venta`, `fecha_vencimiento`, `nro_cuota`, `estado`, `id_ultima_mod`, `fecha_hora_ultima_mod`, `observacion`, `monto_adicional`) VALUES
(1, '2020-12-10', 1, 'PAGADA', 1, '2020-11-01 16:25:38', '', 0),
(1, '2021-01-10', 2, 'PAGADA', 1, '2020-11-01 16:25:38', '', 0),
(1, '2021-02-10', 3, 'PAGADA', 1, '2020-11-01 16:25:38', '', 0),
(1, '2021-03-10', 4, 'PAGADA', 1, '2020-11-01 16:25:38', '', 0),
(2, '2020-12-08', 1, 'PAGADA', 1, '2020-11-01 16:30:12', '', 0),
(2, '2021-01-08', 2, 'PAGADA', 1, '2020-11-01 16:30:12', '', 0),
(2, '2021-02-08', 3, 'PAGADA', 1, '2020-11-01 16:30:12', '', 0),
(2, '2021-03-08', 4, 'PAGADA', 1, '2020-11-01 16:30:12', '', 0),
(3, '2020-12-06', 1, 'PAGADA', 1, '2020-11-01 16:37:03', '', 0),
(3, '2021-01-06', 2, 'PAGADA', 1, '2020-11-01 16:37:03', '', 0),
(3, '2021-02-06', 3, 'PAGADA', 1, '2020-11-01 16:37:03', '', 0),
(4, '2020-11-04', 1, 'VENCIDA', 2, '2020-11-23 23:58:37', '', 0),
(4, '2020-12-04', 2, 'VENCIDA', 2, '2020-11-23 23:58:37', '', 0),
(4, '2021-01-04', 3, 'PENDIENTE', 2, '2020-11-23 23:58:37', '', 0),
(4, '2021-02-04', 4, 'PENDIENTE', 2, '2020-11-23 23:58:37', '', 0),
(5, '2020-12-05', 1, 'VENCIDA', 2, '2020-11-02 21:21:41', '', 0),
(5, '2021-01-05', 2, 'PENDIENTE', 2, '2020-11-02 21:21:41', '', 0),
(5, '2021-02-05', 3, 'PENDIENTE', 2, '2020-11-02 21:21:41', '', 0),
(5, '2021-03-05', 4, 'PENDIENTE', 2, '2020-11-02 21:21:41', '', 0),
(5, '2021-04-05', 5, 'PENDIENTE', 2, '2020-11-02 21:21:41', '', 0),
(6, '2020-11-03', 1, 'PAGADA', 3, '2020-11-03 09:36:37', '', 0),
(6, '2020-12-03', 2, 'VENCIDA', 3, '2020-11-03 09:36:37', '', 0),
(6, '2021-01-03', 3, 'PENDIENTE', 3, '2020-11-03 09:36:37', '', 0),
(6, '2021-02-03', 4, 'PENDIENTE', 3, '2020-11-03 09:36:37', '', 0),
(7, '2020-11-14', 1, 'PAGADA', 2, '2020-11-12 09:41:35', '', 0),
(7, '2020-12-14', 2, 'PAGADA', 2, '2020-11-12 09:41:35', '', 0),
(7, '2021-01-14', 3, 'PENDIENTE', 2, '2020-11-12 09:41:35', '', 0),
(8, '2020-12-20', 1, 'PAGADA', 2, '2020-11-12 13:37:51', '', 0),
(8, '2021-01-20', 2, 'PENDIENTE', 2, '2020-11-12 13:37:51', '', 0),
(8, '2021-02-20', 3, 'PENDIENTE', 2, '2020-11-12 13:37:51', '', 0),
(9, '2020-11-09', 1, 'VENCIDA', 2, '2020-12-09 09:26:14', '', 0),
(9, '2020-12-09', 2, 'VENCIDA', 2, '2020-12-09 09:26:14', '', 0),
(9, '2021-01-09', 3, 'PENDIENTE', 2, '2020-12-09 09:26:14', '', 0),
(10, '2020-12-08', 1, 'PAGADA', 2, '2020-12-11 10:41:53', '', 0),
(10, '2021-01-08', 2, 'PENDIENTE', 2, '2020-12-11 10:41:53', '', 0),
(10, '2021-02-08', 3, 'PENDIENTE', 2, '2020-12-11 10:41:53', '', 0),
(10, '2021-03-08', 4, 'PENDIENTE', 2, '2020-12-11 10:41:53', '', 0);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `detalles_ventas`
--

CREATE TABLE `detalles_ventas` (
  `descripcion` varchar(70) NOT NULL,
  `precio_venta` double NOT NULL,
  `cantidad` int(11) NOT NULL,
  `subtotal` double NOT NULL,
  `id_venta` bigint(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Volcado de datos para la tabla `detalles_ventas`
--

INSERT INTO `detalles_ventas` (`descripcion`, `precio_venta`, `cantidad`, `subtotal`, `id_venta`) VALUES
('Algo', 12000, 1, 12000, 8),
('D', 120, 1, 120, 5),
('D10', 2.82, 1, 2.82, 5),
('D11', 12, 1, 12, 5),
('D2', 900, 1, 900, 5),
('D3', 90, 1, 90, 5),
('D4', 91, 2, 182, 5),
('D5', 90.9, 1, 90.9, 5),
('D6', 82, 1, 82, 5),
('D7', 120, 1, 120, 5),
('D8', 90, 1, 90, 5),
('D9', 90, 1, 90, 5),
('Dale2', 1200, 1, 1200, 5),
('Dale5', 500, 1, 500, 5),
('Detalle', 20000, 1, 20000, 3),
('Detalle', 1200, 1, 1200, 5),
('Detalle 1', 19000, 1, 19000, 1),
('Detalle 1', 12000, 1, 12000, 2),
('Detalle 1 ', 20000, 1, 20000, 4),
('Horno Electrico', 18000, 1, 18000, 6),
('Lavarropas', 5600, 1, 5600, 10),
('Otro Detalle ', 1200, 1, 1200, 5),
('Sommier', 12000, 1, 12000, 9),
('Un Detalle', 15000, 1, 15000, 7),
('Ventilador De Techo', 10000, 1, 10000, 10);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `pagos`
--

CREATE TABLE `pagos` (
  `id_pago` bigint(20) NOT NULL,
  `nro_comprobante` bigint(20) NOT NULL,
  `monto_pagado` double NOT NULL,
  `fecha_hora` datetime NOT NULL,
  `id_registrado_por` bigint(20) NOT NULL,
  `id_venta` bigint(20) NOT NULL,
  `nro_cuota` tinyint(2) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Volcado de datos para la tabla `pagos`
--

INSERT INTO `pagos` (`id_pago`, `nro_comprobante`, `monto_pagado`, `fecha_hora`, `id_registrado_por`, `id_venta`, `nro_cuota`) VALUES
(1, 1, 4750, '2020-11-01 16:25:58', 1, 1, 1),
(2, 2, 4750, '2020-11-01 16:29:50', 1, 1, 2),
(3, 3, 4750, '2020-11-01 16:34:42', 1, 1, 3),
(4, 4, 4750, '2020-11-01 16:34:53', 1, 1, 4),
(5, 5, 3000, '2020-11-01 16:35:04', 1, 2, 1),
(6, 6, 3000, '2020-11-01 16:35:14', 1, 2, 2),
(7, 7, 1500, '2020-11-01 16:35:25', 1, 2, 3),
(8, 8, 1500, '2020-11-01 16:35:34', 1, 2, 3),
(9, 9, 3000, '2020-11-01 16:35:48', 1, 2, 4),
(10, 10, 6666.67, '2020-11-01 16:37:19', 1, 3, 1),
(11, 11, 6666.67, '2020-11-01 16:37:28', 1, 3, 2),
(12, 12, 6666.67, '2020-11-01 16:37:38', 1, 3, 3),
(14, 13, 4500, '2020-11-03 09:49:32', 3, 6, 1),
(15, 14, 5000, '2020-11-12 09:41:56', 2, 7, 1),
(16, 15, 3333.33, '2020-12-09 09:23:14', 2, 8, 1),
(17, 16, 5000, '2020-12-14 12:13:46', 2, 7, 2),
(18, 17, 3900, '2020-12-15 10:13:18', 2, 10, 1);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `pedidos`
--

CREATE TABLE `pedidos` (
  `id_pedido` bigint(20) NOT NULL,
  `fecha_entrega` date NOT NULL,
  `descripcion` varchar(200) NOT NULL,
  `apellido_nombre` varchar(50) NOT NULL,
  `domicilio` varchar(60) NOT NULL,
  `telefono` varchar(60) NOT NULL,
  `estado` varchar(20) NOT NULL,
  `id_ultima_mod` bigint(20) NOT NULL,
  `fecha_hora_ultima_mod` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Volcado de datos para la tabla `pedidos`
--

INSERT INTO `pedidos` (`id_pedido`, `fecha_entrega`, `descripcion`, `apellido_nombre`, `domicilio`, `telefono`, `estado`, `id_ultima_mod`, `fecha_hora_ultima_mod`) VALUES
(2, '2020-12-26', 'mesa ratona v2', 'Cliente De Prueba', 'Chacabuco 2321', '3764577105', 'PENDIENTE', 3, '2020-12-03 09:39:27');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `reclamos`
--

CREATE TABLE `reclamos` (
  `id_reclamo` bigint(20) NOT NULL,
  `id_cliente` bigint(20) NOT NULL,
  `fecha_reclamo` date NOT NULL,
  `motivo` varchar(200) NOT NULL,
  `estado` varchar(20) NOT NULL,
  `id_ultima_mod` bigint(20) NOT NULL,
  `fecha_hora_ultima_mod` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Volcado de datos para la tabla `reclamos`
--

INSERT INTO `reclamos` (`id_reclamo`, `id_cliente`, `fecha_reclamo`, `motivo`, `estado`, `id_ultima_mod`, `fecha_hora_ultima_mod`) VALUES
(1, 2, '2020-12-03', 'llego roto su ventilador', 'PENDIENTE', 2, '2020-12-03 09:51:00');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `roles`
--

CREATE TABLE `roles` (
  `id_rol` bigint(20) NOT NULL,
  `nombre` varchar(20) NOT NULL,
  `v_acceso` tinyint(1) NOT NULL,
  `v_clientes` tinyint(1) NOT NULL,
  `v_ventas` tinyint(1) NOT NULL,
  `v_cuotas` tinyint(1) NOT NULL,
  `v_pedidos` tinyint(1) NOT NULL,
  `v_reclamos` tinyint(1) NOT NULL,
  `v_pagos` tinyint(1) NOT NULL,
  `alt_acceso` tinyint(1) NOT NULL,
  `alt_clientes` tinyint(1) NOT NULL,
  `alt_ventas` tinyint(1) NOT NULL,
  `alt_pagos` tinyint(1) NOT NULL,
  `alt_cuotas` tinyint(4) NOT NULL,
  `alt_configuracion` tinyint(1) NOT NULL,
  `alt_pedidos` tinyint(1) NOT NULL,
  `alt_reclamos` tinyint(1) NOT NULL,
  `new_acceso` tinyint(1) NOT NULL,
  `new_clientes` tinyint(1) NOT NULL,
  `new_ventas` tinyint(1) NOT NULL,
  `new_pagos` tinyint(1) NOT NULL,
  `new_pedidos` tinyint(1) NOT NULL,
  `new_reclamos` tinyint(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Volcado de datos para la tabla `roles`
--

INSERT INTO `roles` (`id_rol`, `nombre`, `v_acceso`, `v_clientes`, `v_ventas`, `v_cuotas`, `v_pedidos`, `v_reclamos`, `v_pagos`, `alt_acceso`, `alt_clientes`, `alt_ventas`, `alt_pagos`, `alt_cuotas`, `alt_configuracion`, `alt_pedidos`, `alt_reclamos`, `new_acceso`, `new_clientes`, `new_ventas`, `new_pagos`, `new_pedidos`, `new_reclamos`) VALUES
(1, 'Super Admin', 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1),
(2, 'Prueba', 0, 1, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `usuarios`
--

CREATE TABLE `usuarios` (
  `id_usuario` bigint(20) NOT NULL,
  `apellido_nombre` varchar(50) NOT NULL,
  `usuario` varchar(20) NOT NULL,
  `clave` varchar(100) NOT NULL,
  `estado` varchar(20) NOT NULL,
  `id_rol` bigint(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Volcado de datos para la tabla `usuarios`
--

INSERT INTO `usuarios` (`id_usuario`, `apellido_nombre`, `usuario`, `clave`, `estado`, `id_rol`) VALUES
(1, 'Super Admin', 'supadmin', 'F7D359D2D6C4F0B208AF1B3C61E184C9AD601FD7C6ECCAC048612420B1AB087E', 'ACTIVO', 1),
(2, 'Zeta', 'gzegray', '51f0a53f3f0f0edc0846b4247b343a28bae5d955ddcd607dbc33b71e17e5f7ef', 'ACTIVO', 1),
(3, 'Rodriguez Pablo', 'rpablo', 'fd152a574a27b0585b19667451158c99d9ae9104bba72fc11e8ebd112e492a00', 'ACTIVO', 1);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `ventas`
--

CREATE TABLE `ventas` (
  `id_venta` bigint(20) NOT NULL,
  `nro_comprobante` bigint(20) NOT NULL,
  `fecha_venta` date NOT NULL,
  `dia_cobro` varchar(5) NOT NULL,
  `id_cliente` bigint(20) NOT NULL,
  `monto_abonado` double NOT NULL,
  `cuotas` varchar(3) NOT NULL,
  `monto_cuota` double NOT NULL,
  `total_venta` double NOT NULL,
  `estado` varchar(50) NOT NULL,
  `id_cobrador` bigint(20) DEFAULT NULL,
  `saldo_pendiente` double NOT NULL,
  `id_ultima_mod` bigint(20) NOT NULL,
  `fecha_hora_ultima_mod` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Volcado de datos para la tabla `ventas`
--

INSERT INTO `ventas` (`id_venta`, `nro_comprobante`, `fecha_venta`, `dia_cobro`, `id_cliente`, `monto_abonado`, `cuotas`, `monto_cuota`, `total_venta`, `estado`, `id_cobrador`, `saldo_pendiente`, `id_ultima_mod`, `fecha_hora_ultima_mod`) VALUES
(1, 1, '2020-11-01', '10', 1, 0, '4', 4750, 19000, 'PAGADA', 1, 0, 1, '2020-11-01 16:25:38'),
(2, 2, '2020-11-01', '8', 1, 0, '4', 3000, 12000, 'PAGADA', 1, 0, 1, '2020-11-01 16:30:12'),
(3, 3, '2020-11-01', '6', 1, 0, '3', 6666.67, 20000, 'PAGADA', 1, 0, 1, '2020-11-01 16:37:03'),
(4, 4, '2020-11-01', '4', 1, 0, '4', 5000, 20000, 'PENDIENTE', 1, 20000, 2, '2020-11-23 23:58:37'),
(5, 5, '2020-11-01', '5', 1, 0, '5', 1175.94, 5879.719999999999, 'PENDIENTE', 1, 5879.72, 2, '2020-11-02 21:21:41'),
(6, 6, '2020-10-14', '3', 2, 0, '4', 4500, 18000, 'PENDIENTE', 3, 13500, 3, '2020-11-03 09:36:37'),
(7, 7, '2020-11-12', '14', 1, 0, '3', 5000, 15000, 'PENDIENTE', 2, 5000, 2, '2020-11-12 09:41:35'),
(8, 8, '2020-11-12', '20', 3, 2000, '3', 3333.33, 12000, 'PENDIENTE', 2, 6666.67, 2, '2020-11-12 13:37:51'),
(9, 9, '2020-11-06', '9', 1, 0, '3', 4000, 12000, 'PENDIENTE', 2, 12000, 2, '2020-12-09 09:26:14'),
(10, 10, '2020-12-11', '8', 1, 0, '4', 3900, 15600, 'PENDIENTE', 2, 11700, 2, '2020-12-11 10:41:53');

--
-- Índices para tablas volcadas
--

--
-- Indices de la tabla `clientes`
--
ALTER TABLE `clientes`
  ADD PRIMARY KEY (`id_cliente`);

--
-- Indices de la tabla `configuracion`
--
ALTER TABLE `configuracion`
  ADD PRIMARY KEY (`id_configuracion`);

--
-- Indices de la tabla `cuotas`
--
ALTER TABLE `cuotas`
  ADD PRIMARY KEY (`id_venta`,`nro_cuota`),
  ADD KEY `fk2_cuotas` (`id_ultima_mod`);

--
-- Indices de la tabla `detalles_ventas`
--
ALTER TABLE `detalles_ventas`
  ADD PRIMARY KEY (`descripcion`,`id_venta`),
  ADD KEY `fk1_detalles_ventas` (`id_venta`);

--
-- Indices de la tabla `pagos`
--
ALTER TABLE `pagos`
  ADD PRIMARY KEY (`id_pago`),
  ADD KEY `fk1_pagos` (`id_registrado_por`),
  ADD KEY `fk2_pagos` (`id_venta`,`nro_cuota`);

--
-- Indices de la tabla `pedidos`
--
ALTER TABLE `pedidos`
  ADD PRIMARY KEY (`id_pedido`),
  ADD KEY `fk1_pedidos` (`id_ultima_mod`);

--
-- Indices de la tabla `reclamos`
--
ALTER TABLE `reclamos`
  ADD PRIMARY KEY (`id_reclamo`),
  ADD KEY `fk1_reclamos` (`id_cliente`),
  ADD KEY `fk2_reclamos` (`id_ultima_mod`);

--
-- Indices de la tabla `roles`
--
ALTER TABLE `roles`
  ADD PRIMARY KEY (`id_rol`);

--
-- Indices de la tabla `usuarios`
--
ALTER TABLE `usuarios`
  ADD PRIMARY KEY (`id_usuario`),
  ADD KEY `id_rol` (`id_rol`);

--
-- Indices de la tabla `ventas`
--
ALTER TABLE `ventas`
  ADD PRIMARY KEY (`id_venta`),
  ADD KEY `fk1_ventas` (`id_cliente`),
  ADD KEY `fk2_ventas` (`id_ultima_mod`),
  ADD KEY `fk3_ventas` (`id_cobrador`);

--
-- AUTO_INCREMENT de las tablas volcadas
--

--
-- AUTO_INCREMENT de la tabla `clientes`
--
ALTER TABLE `clientes`
  MODIFY `id_cliente` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT de la tabla `configuracion`
--
ALTER TABLE `configuracion`
  MODIFY `id_configuracion` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT de la tabla `pagos`
--
ALTER TABLE `pagos`
  MODIFY `id_pago` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=19;

--
-- AUTO_INCREMENT de la tabla `pedidos`
--
ALTER TABLE `pedidos`
  MODIFY `id_pedido` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT de la tabla `reclamos`
--
ALTER TABLE `reclamos`
  MODIFY `id_reclamo` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT de la tabla `roles`
--
ALTER TABLE `roles`
  MODIFY `id_rol` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT de la tabla `usuarios`
--
ALTER TABLE `usuarios`
  MODIFY `id_usuario` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT de la tabla `ventas`
--
ALTER TABLE `ventas`
  MODIFY `id_venta` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;

--
-- Restricciones para tablas volcadas
--

--
-- Filtros para la tabla `cuotas`
--
ALTER TABLE `cuotas`
  ADD CONSTRAINT `fk1_cuotas` FOREIGN KEY (`id_venta`) REFERENCES `ventas` (`id_venta`),
  ADD CONSTRAINT `fk2_cuotas` FOREIGN KEY (`id_ultima_mod`) REFERENCES `usuarios` (`id_usuario`);

--
-- Filtros para la tabla `detalles_ventas`
--
ALTER TABLE `detalles_ventas`
  ADD CONSTRAINT `fk1_detalles_ventas` FOREIGN KEY (`id_venta`) REFERENCES `ventas` (`id_venta`);

--
-- Filtros para la tabla `pagos`
--
ALTER TABLE `pagos`
  ADD CONSTRAINT `fk1_pagos` FOREIGN KEY (`id_registrado_por`) REFERENCES `usuarios` (`id_usuario`),
  ADD CONSTRAINT `fk2_pagos` FOREIGN KEY (`id_venta`,`nro_cuota`) REFERENCES `cuotas` (`id_venta`, `nro_cuota`);

--
-- Filtros para la tabla `pedidos`
--
ALTER TABLE `pedidos`
  ADD CONSTRAINT `fk1_pedidos` FOREIGN KEY (`id_ultima_mod`) REFERENCES `usuarios` (`id_usuario`);

--
-- Filtros para la tabla `reclamos`
--
ALTER TABLE `reclamos`
  ADD CONSTRAINT `fk1_reclamos` FOREIGN KEY (`id_cliente`) REFERENCES `clientes` (`id_cliente`),
  ADD CONSTRAINT `fk2_reclamos` FOREIGN KEY (`id_ultima_mod`) REFERENCES `usuarios` (`id_usuario`);

--
-- Filtros para la tabla `usuarios`
--
ALTER TABLE `usuarios`
  ADD CONSTRAINT `fk1_roles` FOREIGN KEY (`id_rol`) REFERENCES `roles` (`id_rol`);

--
-- Filtros para la tabla `ventas`
--
ALTER TABLE `ventas`
  ADD CONSTRAINT `fk1_ventas` FOREIGN KEY (`id_cliente`) REFERENCES `clientes` (`id_cliente`),
  ADD CONSTRAINT `fk2_ventas` FOREIGN KEY (`id_ultima_mod`) REFERENCES `usuarios` (`id_usuario`),
  ADD CONSTRAINT `fk3_ventas` FOREIGN KEY (`id_cobrador`) REFERENCES `usuarios` (`id_usuario`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
