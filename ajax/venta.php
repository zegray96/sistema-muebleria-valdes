<?php
require ('../modelos/Venta.php');
require ('../modelos/Cliente.php');
require ('../modelos/Usuario.php');

$u=new Usuario();
$c=new Cliente();
$v = new Venta();

$idVenta = isset($_POST['idVenta']) ? limpiarCadena($_POST['idVenta']) : "";
$nroComprobante = isset($_POST['nroComprobante']) ? limpiarCadena($_POST['nroComprobante']) : "";
$fechaVenta = isset($_POST['fechaVenta']) ? limpiarCadena($_POST['fechaVenta']) : "";
$diaCobro = isset($_POST['diaCobro']) ? limpiarCadena($_POST['diaCobro']) : "";
$idCliente = isset($_POST['idCliente']) ? limpiarCadena($_POST['idCliente']) : "";
$idCobrador = isset($_POST['idCobrador']) ? limpiarCadena($_POST['idCobrador']) : "";
$montoAbonado = isset($_POST['montoAbonado']) ? limpiarCadena($_POST['montoAbonado']) : "";
$cuotas = isset($_POST['cuotas']) ? limpiarCadena($_POST['cuotas']) : "";
$montoCuota = isset($_POST['montoCuota']) ? limpiarCadena($_POST['montoCuota']) : "";
$total = isset($_POST['total']) ? limpiarCadena($_POST['total']) : "";


switch ($_GET['op']) {

	case 'nuevo_editar':
		if ($montoCuota<0) {
			echo "¡Verifique valores negativos!";
		}else{
			if ($fechaVenta=="" || $idCliente=="" || $idCobrador=="" || $diaCobro=="" || $cuotas=="" || $montoCuota=="") {
				echo "¡Complete los campos obligatorios!";
			}else{
				$montoAbonadoSinComa=str_replace(array(','), '' , $montoAbonado);
				// Si campos obligatorios fueron completados
				$parts = explode('/',$fechaVenta);
				$fechaFormateada = $parts[2] . '-' . $parts[1] . '-' . $parts[0];
				session_start();
				$idUltimaMod=$_SESSION['idUsuarioSisCob'];
				if(empty($idVenta)){
					if ($_SESSION['new_ventas']==0) {
						echo "¡Acción denegada!";
					}else{

						$respuesta=$v->nuevo($fechaFormateada, $idCliente, $idCobrador, $diaCobro, $montoAbonadoSinComa, $cuotas, $montoCuota, $total, $idUltimaMod, $_POST["descripcion"], $_POST["precioVenta"], $_POST["cantidad"], $_POST["subtotal"]);
						// RETORNO
						$r = explode(":", $respuesta);
						$resultadoOperacion = $r[0];
						$resultadoId=$r[1];
						if ($resultadoOperacion==1) {
							echo "¡Registro creado con exito!:".$resultadoId;
						}else{
							echo "¡Ocurrió un problema y no se pudo crear";
						}

					}
					
				}else{

					if ($_SESSION['alt_ventas']==0) {
						echo "¡Acción denegada!";
					}else{

						// editar
						$verifCuotasPagas=$v->verificar_cuota_tiene_pago($idVenta);

						$respuesta=$v->buscar_id($idVenta);
						$totalBd=$respuesta['total_venta'];
						$diaCobroBd=$respuesta['dia_cobro'];
						$montoAbonadoBd=$respuesta['monto_abonado'];
						$cuotasBd=$respuesta['cuotas'];
						$montoCuotaBd=$respuesta['monto_cuota'];


						if ( mysqli_num_rows($verifCuotasPagas)>0) {
							if ($totalBd!=$total || $diaCobroBd!=$diaCobro || $montoAbonadoBd!=$montoAbonadoSinComa || $cuotasBd!=$cuotas || $montoCuotaBd!=$montoCuota) {
								echo "¡No puede editar datos de pago ya que tiene cuotas pagadas!";
							}else{
								$respuesta=$v->editar_menos_cuotas($idVenta, $fechaFormateada, $idCliente, $idCobrador, $idUltimaMod);
								// RETORNO
								$r = explode(":", $respuesta);
								$resultadoOperacion = $r[0];
								$resultadoId=$r[1];
								if ($resultadoOperacion==1) {
									echo "¡Registro editado con exito!:".$resultadoId;
								}else{
									echo "¡Ocurrió un problema y no se pudo editar";
								}
							}	
						}else{
							$respuesta=$v->editar_todo($idVenta, $fechaFormateada, $idCliente, $idCobrador, $diaCobro, $montoAbonadoSinComa, $cuotas, $montoCuota, $total, $idUltimaMod, $_POST["descripcion"], $_POST["precioVenta"], $_POST["cantidad"], $_POST["subtotal"]);
							// RETORNO
							$r = explode(":", $respuesta);
							$resultadoOperacion = $r[0];
							$resultadoId=$r[1];
							if ($resultadoOperacion==1) {
								echo "¡Registro editado con exito!:".$resultadoId;
							}else{
								echo "¡Ocurrió un problema y no se pudo editar";
							}
						}

					}
					

				}
			}	
		}
		

	break;

	case 'mostrar':
		$respuesta = $v->buscar_id($idVenta);
		echo json_encode($respuesta);
	break;

	case 'listar':
		session_start();
		$alteracion=$_SESSION['alt_ventas'];
		$respuesta=$v->listar();
		$data = Array();

		while($reg=$respuesta->fetch_object()){
			if($alteracion==1){
				$opciones='<button class="btn btn-warning" onclick="mostrar('.$reg->id_venta.')"><i class="fas fa-pencil-alt"></i></button> <button class="btn btn-info" onclick="imprimir_comprobante('.$reg->id_venta.')"><i class="fas fa-print"></i></button>'; 
			}else{
				$opciones='<button class="btn btn-info" onclick="imprimir_comprobante('.$reg->id_venta.')"><i class="fas fa-print"></i></button>';
			}

			if($reg->estado=="PENDIENTE"){
				$estado='<span class="badge badge-warning">'.$reg->estado.'</span>';
			}else{
				if($reg->estado=="PAGADA"){
					$estado='<span class="badge badge-success">'.$reg->estado.'</span>';
				}else{
					if($reg->estado=="ANULADA"){
						$estado='<span class="badge badge-danger">'.$reg->estado.'</span>';
					}
				}
			}

			$data[]=array(
				"0"=>$opciones,
				"1"=>str_pad($reg->nro_comprobante, 8 ,"0", STR_PAD_LEFT),
				"2"=>$estado,
				"3"=>$reg->fecha_venta,	
				"4"=>$reg->apellidoNombre,
				"5"=>$reg->dni,
				"6"=>$reg->cobrador,	
				"7"=>"$".number_format($reg->total_venta, 2),
				"8"=>"$".number_format($reg->saldo_pendiente, 2),
					

			);
		}

		$results=array(
			"sEcho"=>1, //informacion para el data table
			"iTotalRecords"=>count($data), //enbviamos total de registros para al datatable
			"iTotalDisplayRecords"=>count($data), //enviamos total de registros a visualizar
			"aaData"=>$data
		);
		echo json_encode($results);
	break;

	case 'listar_por_cliente':
		$idCliente=$_REQUEST['idCliente'];
		session_start();
		$alteracion=$_SESSION['alt_ventas'];
		$respuesta=$v->listar_por_cliente($idCliente);
		$data = Array();

		while($reg=$respuesta->fetch_object()){
			if($alteracion==1){
				$opciones='<button class="btn btn-warning" onclick="mostrar('.$reg->id_venta.')"><i class="fas fa-pencil-alt"></i></button> <button class="btn btn-info" onclick="imprimir_comprobante('.$reg->id_venta.')"><i class="fas fa-print"></i></button>'; 
			}else{
				$opciones='<button class="btn btn-info" onclick="imprimir_comprobante('.$reg->id_venta.')"><i class="fas fa-print"></i></button>';
			}

			if($reg->estado=="PENDIENTE"){
				$estado='<span class="badge badge-warning">'.$reg->estado.'</span>';
			}else{
				if($reg->estado=="PAGADA"){
					$estado='<span class="badge badge-success">'.$reg->estado.'</span>';
				}else{
					if($reg->estado=="ANULADA"){
						$estado='<span class="badge badge-danger">'.$reg->estado.'</span>';
					}
				}
			}

			$data[]=array(
				"0"=>$opciones,
				"1"=>str_pad($reg->nro_comprobante, 8 ,"0", STR_PAD_LEFT),
				"2"=>$estado,
				"3"=>$reg->fecha_venta,	
				"4"=>$reg->apellidoNombre,
				"5"=>$reg->dni,
				"6"=>$reg->cobrador,	
				"7"=>"$".number_format($reg->total_venta, 2),
				"8"=>"$".number_format($reg->saldo_pendiente, 2),
					

			);
		}

		$results=array(
			"sEcho"=>1, //informacion para el data table
			"iTotalRecords"=>count($data), //enbviamos total de registros para al datatable
			"iTotalDisplayRecords"=>count($data), //enviamos total de registros a visualizar
			"aaData"=>$data
		);
		echo json_encode($results);
	break;

	case 'listar_por_fecha':
		$fechaIni=$_REQUEST['fechaIni'];
		$fechaFin=$_REQUEST['fechaFin'];

		$parts = explode('/',$fechaIni);
		$fechaIniFormateada = $parts[2] . '-' . $parts[1] . '-' . $parts[0];

		$parts = explode('/',$fechaFin);
		$fechaFinFormateada = $parts[2] . '-' . $parts[1] . '-' . $parts[0];
		
		session_start();
		$alteracion=$_SESSION['alt_ventas'];
		$respuesta=$v->listar_por_fecha($fechaIniFormateada,$fechaFinFormateada);
		$data = Array();

		while($reg=$respuesta->fetch_object()){
			if($alteracion==1){
				$opciones='<button class="btn btn-warning" onclick="mostrar('.$reg->id_venta.')"><i class="fas fa-pencil-alt"></i></button> <button class="btn btn-info" onclick="imprimir_comprobante('.$reg->id_venta.')"><i class="fas fa-print"></i></button>'; 
			}else{
				$opciones='<button class="btn btn-info" onclick="imprimir_comprobante('.$reg->id_venta.')"><i class="fas fa-print"></i></button>';
			}

			if($reg->estado=="PENDIENTE"){
				$estado='<span class="badge badge-warning">'.$reg->estado.'</span>';
			}else{
				if($reg->estado=="PAGADA"){
					$estado='<span class="badge badge-success">'.$reg->estado.'</span>';
				}else{
					if($reg->estado=="ANULADA"){
						$estado='<span class="badge badge-danger">'.$reg->estado.'</span>';
					}
				}
			}

			$data[]=array(
				"0"=>$opciones,
				"1"=>str_pad($reg->nro_comprobante, 8 ,"0", STR_PAD_LEFT),
				"2"=>$estado,
				"3"=>$reg->fecha_venta,	
				"4"=>$reg->apellidoNombre,
				"5"=>$reg->dni,
				"6"=>$reg->cobrador,	
				"7"=>"$".number_format($reg->total_venta, 2),
				"8"=>"$".number_format($reg->saldo_pendiente, 2),
					

			);
		}

		$results=array(
			"sEcho"=>1, //informacion para el data table
			"iTotalRecords"=>count($data), //enbviamos total de registros para al datatable
			"iTotalDisplayRecords"=>count($data), //enviamos total de registros a visualizar
			"aaData"=>$data
		);
		echo json_encode($results);
	break;

	case 'generar_nro_comprobante':
		$resultado = $v->generar_nro_comprobante();
		echo json_encode($resultado);
	break;

	case 'listar_clientes_para_venta':
		
		$respuesta=$c->listar();
		//declaramos un array
		$data = Array();
		while($reg=$respuesta->fetch_object()){

			$saldoPendienteTotal=$v->saldo_pendiente_total($reg->id_cliente);
			$saldoPendienteTotal=$saldoPendienteTotal['saldoPendienteTotal'];
			if ($saldoPendienteTotal==null) {
				$saldoPendienteTotal=0;
			}

			$data[]=array(
				"0"=>'<button class="btn btn-success" onclick="agregar_cliente('.$reg->id_cliente.',\''.$reg->dni.'\',\''.$reg->apellido_nombre.'\',\''.$reg->domicilio.'\',\''.$reg->telefono.'\')"><i class="fas fa-arrow-circle-right"></i></button>',
				"1"=>$reg->apellido_nombre,
				"2"=>$reg->dni,
				"3"=>$reg->barrio,
				"4"=>$reg->domicilio,
				"5"=>$reg->telefono,
				"6"=>"$".number_format($saldoPendienteTotal, 2),
			);
		}

		$results=array(
			"sEcho"=>1, //informacion para el data table
			"iTotalRecords"=>count($data), //enbviamos total de registros para al datatable
			"iTotalDisplayRecords"=>count($data), //enviamos total de registros a visualizar
			"aaData"=>$data
		);
		echo json_encode($results);
	break;

	case 'listar_clientes_para_filtrar':
		
		$respuesta=$c->listar();
		//declaramos un array
		$data = Array();
		while($reg=$respuesta->fetch_object()){
			$saldoPendienteTotal=$v->saldo_pendiente_total($reg->id_cliente);
			$saldoPendienteTotal=$saldoPendienteTotal['saldoPendienteTotal'];
			if ($saldoPendienteTotal==null) {
				$saldoPendienteTotal=0;
			}

			$data[]=array(
				"0"=>'<button class="btn btn-success" onclick="listar_por_cliente('.$reg->id_cliente.',\''.$reg->apellido_nombre.'\')"><i class="fas fa-arrow-circle-right"></i></button>',
				"1"=>$reg->apellido_nombre,
				"2"=>$reg->dni,
				"3"=>$reg->barrio,	
				"4"=>$reg->domicilio,
				"5"=>$reg->telefono,
				"6"=>"$".number_format($saldoPendienteTotal, 2),
			);
		}

		$results=array(
			"sEcho"=>1, //informacion para el data table
			"iTotalRecords"=>count($data), //enbviamos total de registros para al datatable
			"iTotalDisplayRecords"=>count($data), //enviamos total de registros a visualizar
			"aaData"=>$data
		);
		echo json_encode($results);
	break;

	case 'listar_detalles':
		$id=$_GET['id'];
		$resultado=$v->listar_detalles($id);
		echo '<thead class="thead-dark">
				<th>Opciones</th>
				<th>Cantidad</th>
				<th>Descripción</th>
				<th>Precio&nbsp;V.</th>
				<th>Subtotal</th>
			</thead>';
	    $idFila=0;
	    setlocale(LC_MONETARY, 'en_US');
		while($reg=$resultado->fetch_object()){
			$precioVenta="$".number_format($reg->precio_venta, 2);
			$subtotal="$".number_format($reg->subtotal, 2);
			echo '<tr class="filas" id="fila'.$idFila.'">
					<td style="display: none"><input class="precioVenta" type="hidden" name="precioVenta[] id="precioVenta[]" value="'.$reg->precio_venta.'">'.$reg->precio_venta.'</td>
					<td style="display: none"><input type="hidden" name="subtotal[] id="subtotal[]" value="'.$reg->subtotal.'">'.$reg->subtotal.'</td>
					<td><button type="button" class="btn btn-danger" onclick="borrar_detalle('.$idFila.')"><i class="fas fa-trash-alt"></i></button> <button type="button" class="btn btn-info" onclick="ver_detalle('.$idFila.')"><i class="fas fa-eye"></i></button></td>
					<td><input type="hidden" name="cantidad[] id="cantidad[]" value="'.$reg->cantidad.'">'.$reg->cantidad.'</td>
					<td><input class="descripcion" type="hidden" name="descripcion[] id="descripcion[]" value="'.$reg->descripcion.'">'.$reg->descripcion.'</td>
					<td>'.$precioVenta.'</td>
					<td>'.$subtotal.'</td>
				</tr>';
			$idFila++;
		}
		$idFila--;
		echo '<input type="hidden" id="idFila" value="'.$idFila.'">';
	break;

	case 'cargar_cobradores':
		
		$resultado=$u->cargar_cobradores();
		while($reg=$resultado->fetch_object()){
			echo '<option value='.$reg->id_usuario.'>'.$reg->apellido_nombre.'</option>';
		}
	break;

	case 'listar_por_cobrador_cargar_datos':
		
		$resultado=$u->cargar_cobradores();
		echo '<option value="" selected>Todos</option>';
		while($reg=$resultado->fetch_object()){
			echo '<option value='.$reg->apellido_nombre.'>'.$reg->apellido_nombre.'</option>';
		}
	break;

	case 'traer_id_usuario':
		session_start();
		echo $_SESSION['idUsuarioSisCob'];
	break;

	case 'cant_ventas_dashboard':
		$resultado=$v->cant_ventas_dashboard();
		echo $resultado['cant'];
	break;

}
?>