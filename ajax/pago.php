<?php
require ('../modelos/Pago.php');
require ('../modelos/Venta.php');
require ('../modelos/Cuota.php');

$p = new Pago();
$v=new Venta();
$c=new Cuota();

$idVentaFormPago = isset($_POST['idVentaFormPago']) ? limpiarCadena($_POST['idVentaFormPago']) : "";
$nroCuotaFormPago = isset($_POST['nroCuotaFormPago']) ? limpiarCadena($_POST['nroCuotaFormPago']) : "";
$idPago = isset($_POST['idPago']) ? limpiarCadena($_POST['idPago']) : "";
$montoPagado = isset($_POST['montoPagado']) ? limpiarCadena($_POST['montoPagado']) : "";


switch ($_GET['op']) {

	case 'nuevo':
		$montoPagadoSinComa=str_replace(array(','), '' , $montoPagado);
		if ($montoPagadoSinComa=="" || $montoPagadoSinComa<=0) {
			echo "¡Complete los campos obligatorios!";
		}else{
			if(empty($idPago)){
				$respuesta=$v->buscar_id($idVentaFormPago);
				$saldoPendienteBd=$respuesta['saldo_pendiente'];
				$montoCuotaBd=$respuesta['monto_cuota'];

				$respuesta=$p->total_pagado($idVentaFormPago,$nroCuotaFormPago);
				$totalPagadoBd=$respuesta['totalPagado'];

				$respuesta=$c->buscar_id($idVentaFormPago,$nroCuotaFormPago);
				$montoAdicionalBd=$respuesta['monto_adicional'];
					

				$totalPagado=$totalPagadoBd+$montoPagadoSinComa;
				$montoCuotaConMontoAdicional=$montoCuotaBd+$montoAdicionalBd;
				if($totalPagado>$montoCuotaConMontoAdicional){
					echo "¡Los pagos no pueden superar al monto de cuota pendiente!";
				}else{
					$registrar=true;
					session_start();
					$idRegistradoPor=$_SESSION['idUsuarioSisCob'];
					
					if($nroCuotaFormPago>1){
					// verificar que cuota anterior este pagada
						$verifAnterior = $c->verificacion_cuota_anterior($idVentaFormPago,$nroCuotaFormPago);

						if(mysqli_num_rows($verifAnterior)>0){
							$registrar=true;
						}else{
							$registrar=false;
						}	
					}

					if($registrar){
						if ($_SESSION['new_pagos']==0) {
							echo "¡Acción denegada!";
						}else{
							$respuesta = $p->nuevo($idVentaFormPago,$nroCuotaFormPago,$saldoPendienteBd,$montoPagadoSinComa,$totalPagadoBd,$montoCuotaConMontoAdicional,$idRegistradoPor);
						
						// RETORNO
							$r = explode(":", $respuesta);
							$resultadoOperacion = $r[0];
							$resultadoId=$r[1];
							if ($resultadoOperacion==1) {
								echo "¡Registro creado con exito!:".$resultadoId;
							}else{
								echo "¡Ocurrió un problema y no se pudo crear";
							}
						}
						

					}else{
						echo '¡Debe tener pagada la cuota anterior!';
					}	
				}
			}else{
				echo '¡Los pagos no se pueden editar!';
			}
		}
		
		
	break;

	case 'mostrar':
		$respuesta = $p->buscar_id($idPago);
		echo json_encode($respuesta);
	break;

	case 'eliminar':
		session_start();
		if ($_SESSION['alt_pagos']==0) {
			echo "¡Acción denegada!";
		}else{
			$respuesta=$v->buscar_id($idVentaFormPago);
			$saldoPendienteBd=$respuesta['saldo_pendiente'];

			$respuesta=$p->buscar_id($idPago);
			$montoPagadoBd=$respuesta['monto_pagado'];

			$respuesta=$p->total_pagado($idVentaFormPago,$nroCuotaFormPago);
			$totalPagadoBd=$respuesta['totalPagado'];

			$respuesta=$p->eliminar($idPago,$idVentaFormPago,$nroCuotaFormPago,$saldoPendienteBd,$montoPagadoBd,$totalPagadoBd);
			echo $respuesta ? "¡Registro eliminado con exito!" : "¡Ocurrió un problema y no se pudo eliminar!";
		}
		
	break;

	case 'listar_pagos':
		$idVenta=$_GET['idVenta'];
		$nroCuota=$_GET['nroCuota'];
		$respuesta=$p->listar_pagos($idVenta,$nroCuota);
		while($reg=$respuesta->fetch_object()){
			echo '<label>N° Comprobante: </label> <a style="cursor:pointer; color:#6777ef;" onclick="mostrar_pago('.$reg->id_pago.')">'.str_pad($reg->nro_comprobante, 8 ,"0", STR_PAD_LEFT).'</a><br>';
		}
	break;

	case 'listar':
		$respuesta=$p->listar();
		$data = Array();

		while($reg=$respuesta->fetch_object()){

			$opciones='<button class="btn btn-info" onclick="imprimir_comprobante('.$reg->id_pago.')"><i class="fas fa-print"></i></button>'; 

			$data[]=array(
				"0"=>$opciones,
				"1"=>str_pad($reg->nro_comprobante, 8 ,"0", STR_PAD_LEFT),
				"2"=>$reg->apellidoNombre,
				"3"=>$reg->domicilio,
				"4"=>"$".number_format($reg->monto_pagado, 2),		
			);
		}

		$results=array(
			"sEcho"=>1, //informacion para el data table
			"iTotalRecords"=>count($data), //enbviamos total de registros para al datatable
			"iTotalDisplayRecords"=>count($data), //enviamos total de registros a visualizar
			"aaData"=>$data
		);
		echo json_encode($results);
	break;

	case 'listar_pagos_hoy':
		$respuesta=$p->listar_pagos_hoy();
		$data = Array();

		while($reg=$respuesta->fetch_object()){

			$opciones='<button class="btn btn-info" onclick="imprimir_comprobante('.$reg->id_pago.')"><i class="fas fa-print"></i></button>'; 

			$data[]=array(
				"0"=>$opciones,
				"1"=>str_pad($reg->nro_comprobante, 8 ,"0", STR_PAD_LEFT),
				"2"=>$reg->apellidoNombre,
				"3"=>$reg->domicilio,
				"4"=>"$".number_format($reg->monto_pagado, 2),	
				"5"=>$reg->registradoPor,	
			);
		}

		$results=array(
			"sEcho"=>1, //informacion para el data table
			"iTotalRecords"=>count($data), //enbviamos total de registros para al datatable
			"iTotalDisplayRecords"=>count($data), //enviamos total de registros a visualizar
			"aaData"=>$data
		);
		echo json_encode($results);
	break;

	case 'listar_por_fecha':
		$fechaIni=$_REQUEST['fechaIni'];
		$fechaFin=$_REQUEST['fechaFin'];

		$parts = explode('/',$fechaIni);
		$fechaIniFormateada = $parts[2] . '-' . $parts[1] . '-' . $parts[0];

		$parts = explode('/',$fechaFin);
		$fechaFinFormateada = $parts[2] . '-' . $parts[1] . '-' . $parts[0];

		$respuesta=$p->listar_por_fecha($fechaIniFormateada,$fechaFinFormateada);
		$data = Array();

		while($reg=$respuesta->fetch_object()){

			$opciones='<button class="btn btn-info" onclick="imprimir_comprobante('.$reg->id_pago.')"><i class="fas fa-print"></i></button>'; 

			$data[]=array(
				"0"=>$opciones,
				"1"=>str_pad($reg->nro_comprobante, 8 ,"0", STR_PAD_LEFT),
				"2"=>$reg->apellidoNombre,
				"3"=>$reg->domicilio,
				"4"=>"$".number_format($reg->monto_pagado, 2),
				"5"=>$reg->registradoPor,		
			);
		}

		$results=array(
			"sEcho"=>1, //informacion para el data table
			"iTotalRecords"=>count($data), //enbviamos total de registros para al datatable
			"iTotalDisplayRecords"=>count($data), //enviamos total de registros a visualizar
			"aaData"=>$data
		);
		echo json_encode($results);
	break;

	case 'total_pagado':
		$idVenta=$_GET['idVenta'];
		$nroCuota=$_GET['nroCuota'];
		$respuesta=$p->total_pagado($idVenta,$nroCuota);
		$totalPagadoBd=$respuesta['totalPagado'];

		$respuesta=$v->buscar_id($idVenta);
		$montoCuotaBd=$respuesta['monto_cuota'];

		$respuesta=$c->buscar_id($idVenta,$nroCuota);
		$montoAdicionalBd=$respuesta['monto_adicional'];

		$montoCuotaConMontoAdicional=$montoCuotaBd+$montoAdicionalBd;

		$montoPendiente=$montoCuotaConMontoAdicional-$totalPagadoBd;
		echo $montoPendiente;
	break;

	case 'cant_pagos_hoy_dashboard':
		$respuesta=$p->cant_pagos_hoy_dashboard();
		echo $respuesta['cant'];
	break;
}
?>