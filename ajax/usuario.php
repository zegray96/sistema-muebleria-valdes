<?php
require ('../modelos/Usuario.php');
require ('../modelos/Rol.php');

$u = new Usuario();
$r = new Rol();

$idUsuario = isset($_POST['idUsuario']) ? limpiarCadena($_POST['idUsuario']) : "";
$apellidoNombre = isset($_POST['apellidoNombre']) ? limpiarCadena($_POST['apellidoNombre']) : "";
$usuario = isset($_POST['usuario']) ? limpiarCadena($_POST['usuario']) : "";
$clave = isset($_POST['clave']) ? limpiarCadena($_POST['clave']) : "";
$idRol = isset($_POST['idRol']) ? limpiarCadena($_POST['idRol']) : "";
$estado = isset($_POST['estado']) ? limpiarCadena($_POST['estado']) : "";


switch ($_GET['op']) {
	case 'nuevo_editar':
		if ($idUsuario==1) {
			echo "¡No puede editar usuario Super Admin!";
		}else{
			if ($apellidoNombre=="" || $usuario=="" || $clave=="" || $idRol=="" || $estado=="") {
				echo "¡Complete los campos obligatorios!";
			}else{

				$verificacion=null;
				$usuarioBd="";

				if(!empty($idUsuario)){
					$resultado=$u->buscar_id($idUsuario);
					$usuarioBd=$resultado['usuario'];

				}
				if($usuario!=$usuarioBd){
					$verificacion=$u->verificar_existencia_usuario($usuario);
				}

				if(!empty($verificacion)){
					echo "¡Usuario Ya Existe!";
				}else{
			//CLAVE HASH SHA256
					$claveHash=hash("SHA256",$clave);
					if(empty($idUsuario)){
						session_start();
						if ($_SESSION['new_acceso']==0) {
							echo "¡Acción denegada!";
						}else{
							$resultado=$u->nuevo($apellidoNombre,$usuario,$claveHash,$idRol,$estado);
							echo $resultado ? "¡Registro creado con exito!" : "¡Ocurrió un problema y no se pudo crear!";
						}

					}else{
						session_start();
						if ($_SESSION['alt_acceso']==0) {
							echo "¡Acción denegada!";
						}else{
							$resultado=$u->editar($idUsuario,$apellidoNombre,$usuario,$idRol,$estado);
							echo $resultado ? "¡Registro editado con exito!" : "¡Ocurrió un problema y no se pudo editar!";
						}

					}
				}

			}
			
		}
		
		
		
	break;

	case 'modificar_clave':	
		session_start();
		if ($_SESSION['alt_acceso']==0) {
			echo "¡Acción denegada!";
		}else{
			$idUsuarioModificar = isset($_POST['idUsuarioModificar']) ? limpiarCadena($_POST['idUsuarioModificar']) : "";
			$claveModificar = isset($_POST['claveModificar']) ? limpiarCadena($_POST['claveModificar']) : "";
			$repetirClaveModificar = isset($_POST['repetirClaveModificar']) ? limpiarCadena($_POST['repetirClaveModificar']) : "";

			if ($claveModificar!=$repetirClaveModificar) {
				echo "¡Claves no coinciden!";
			}else{
				$claveHash=hash("SHA256", $claveModificar);
				$resultado=$u->modificar_clave($idUsuarioModificar,$claveHash);
				echo $resultado ? "¡Clave modificada con exito!" : "¡Ocurrió un problema y no se pudo modificar!";
			}
		}
		
	break;

	case 'modificar_clave_mi_cuenta':	
		session_start();
		$idUsuarioModificar = $_SESSION['idUsuarioSisCob'];
		$claveModificar = isset($_POST['claveModificar']) ? limpiarCadena($_POST['claveModificar']) : "";
		$repetirClaveModificar = isset($_POST['repetirClaveModificar']) ? limpiarCadena($_POST['repetirClaveModificar']) : "";

		if ($claveModificar!=$repetirClaveModificar) {
			echo "¡Claves no coinciden!";
		}else{
			$claveHash=hash("SHA256", $claveModificar);
			$resultado=$u->modificar_clave($idUsuarioModificar,$claveHash);
			echo $resultado ? "¡Clave modificada con exito!" : "¡Ocurrió un problema y no se pudo modificar!";
		}
		
	break;

	case 'modificar_cuenta':
		$verificacion=null;
		$usuarioBd="";
		session_start();
		$idUsuario=$_SESSION['idUsuarioSisCob'];

		if ($idUsuario==1) {
			echo "¡No puede editar usuario Super Admin!";
		}else{
			$resultado=$u->buscar_id($idUsuario);
			$usuarioBd=$resultado['usuario'];

			if($usuario!=$usuarioBd){
				$verificacion=$u->verificar_existencia_usuario($usuario);
			}

			if(!empty($verificacion)){
				echo "¡Usuario Ya Existe!";
			}else{
				$resultado=$u->modificar_cuenta($idUsuario,$apellidoNombre,$usuario);
				echo $resultado ? "¡Registro editado con exito! Los cambios se reflejarán luego de cerrar sesión..." : "¡Ocurrió un problema y no se pudo editar!";
			}
		}
				
		
	break;

	case 'mostrar':
		$respuesta=$u->buscar_id($idUsuario);
		echo json_encode($respuesta);
	break;

	case 'listar':
		session_start();
		$alteracion=$_SESSION['alt_acceso'];
		$respuesta=$u->listar();
		$data = Array();

		while($reg=$respuesta->fetch_object()){

			if($alteracion==1 && $reg->id_usuario!=1){
				$opciones='<button class="btn btn-warning" onclick="mostrar('.$reg->id_usuario.')"><i class="fas fa-pencil-alt"></i></button>'; 
			}else{
				$opciones='';
			}

			if($reg->estado=="ACTIVO"){
				$estado='<span class="badge badge-success">'.$reg->estado.'</span>';
			}else{
				if($reg->estado=="INACTIVO"){
					$estado='<span class="badge badge-danger">'.$reg->estado.'</span>';
				}
			}

			$data[]=array(
				"0"=>$opciones,
				"1"=>$reg->apellido_nombre,
				"2"=>$reg->usuario,
				"3"=>$reg->rol,
				"4"=>$estado,
			);
		}

		$results=array(
			"sEcho"=>1, //informacion para el data table
			"iTotalRecords"=>count($data), //enbviamos total de registros para al datatable
			"iTotalDisplayRecords"=>count($data), //enviamos total de registros a visualizar
			"aaData"=>$data
		);
		echo json_encode($results);
	break;

	case 'cargar_roles':
		$resultado=$r->listar();
		while($reg=$resultado->fetch_object()){
			echo '<option value='.$reg->id_rol.'>'.$reg->nombre.'</option>';
		}
	break;
}
?>