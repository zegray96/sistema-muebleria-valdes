<?php
require ('../modelos/Cliente.php');
require ('../modelos/Venta.php');

$c = new Cliente();
$v = new Venta();

$idCliente = isset($_POST['idCliente']) ? limpiarCadena($_POST['idCliente']) : "";
$apellidoNombre = isset($_POST['apellidoNombre']) ? limpiarCadena($_POST['apellidoNombre']) : "";
$dni = isset($_POST['dni']) ? limpiarCadena($_POST['dni']) : "";
$barrio = isset($_POST['barrio']) ? limpiarCadena($_POST['barrio']) : "";
$domicilio = isset($_POST['domicilio']) ? limpiarCadena($_POST['domicilio']) : "";
$telefono = isset($_POST['telefono']) ? limpiarCadena($_POST['telefono']) : "";


switch ($_GET['op']) {
	case 'nuevo_editar':
		if($apellidoNombre=="" || $dni=="" || $barrio=="" || $domicilio=="" || $telefono==""){
			echo "¡Complete los campos obligatorios!";
		}else{
			
			$verificacion=null;
			$dniBd="";
			// Si vamos a editar traemos el dni de la bd
			if(!empty($idCliente)){
				$respuesta=$c->buscar_id($idCliente);
				$dniBd=$respuesta['dni'];
			}
			// Verificar existencia dni
			if($dni!=$dniBd){
				$verificacion=$c->verificar_existencia_dni($dni);
			}


			if(!empty($verificacion)){
				echo "¡Dni ya existe!";
			}else{
				if(empty($idCliente)){
					session_start();
					if ($_SESSION['new_clientes']==0) {
						echo "¡Acción denegada!";
					}else{
						// Nuevo
						$respuesta = $c->nuevo($apellidoNombre,$dni,$barrio,$domicilio,$telefono);
						echo $respuesta ? "¡Registro creado con exito!" : "¡Ocurrió un problema y no se pudo crear!";
					}
					
				}else{
					session_start();
					if ($_SESSION['alt_clientes']==0) {
						echo "¡Acción denegada!";
					}else{
						// Editar
						$respuesta = $c->editar($idCliente,$apellidoNombre,$dni,$barrio,$domicilio,$telefono);
						echo $respuesta ? "¡Registro editado con exito!" : "¡Ocurrió un problema y no se pudo editar!";
					}
					
				}
			}	
		}
	break;

	case 'mostrar':
		$respuesta = $c->buscar_id($idCliente);
		echo json_encode($respuesta);
	break;

	case 'listar':
		session_start();
		$alteracion=$_SESSION['alt_clientes'];
		$respuesta=$c->listar();
		$data = Array();

		while($reg=$respuesta->fetch_object()){
			$saldoPendienteTotal=$v->saldo_pendiente_total($reg->id_cliente);
			$saldoPendienteTotal=$saldoPendienteTotal['saldoPendienteTotal'];
			if ($saldoPendienteTotal==null) {
				$saldoPendienteTotal=0;
			}

			if($alteracion==1){
				$opciones='<button class="btn btn-warning" onclick="mostrar('.$reg->id_cliente.')"><i class="fas fa-pencil-alt"></i></button>'; 
			}else{
				$opciones='';
			}

			$data[]=array(
				"0"=>$opciones,
				"1"=>$reg->apellido_nombre,
				"2"=>$reg->dni,
				"3"=>$reg->barrio,
				"4"=>$reg->domicilio,
				"5"=>$reg->telefono,	
				"6"=>"$".number_format($saldoPendienteTotal, 2),
			);
		}

		$results=array(
			"sEcho"=>1, //informacion para el data table
			"iTotalRecords"=>count($data), //enbviamos total de registros para al datatable
			"iTotalDisplayRecords"=>count($data), //enviamos total de registros a visualizar
			"aaData"=>$data
		);
		echo json_encode($results);
	break;

	case 'cant_clientes_dashboard':
		$resultado=$c->cant_clientes_dashboard();
		echo $resultado['cant'];
	break;
}
?>