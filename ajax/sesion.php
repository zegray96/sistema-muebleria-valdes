<?php
require ('../modelos/Usuario.php');
$u = new Usuario();

session_start();

$usuario=isset($_POST['usuario']) ? limpiarCadena($_POST['usuario']) : "";
$clave=isset($_POST['clave']) ? limpiarCadena($_POST['clave']) : "";

switch ($_GET['op']) {
	case 'iniciar_sesion':
		$claveHash = hash("SHA256",$clave);
		$respuesta = $u->verificar_login($usuario,$claveHash);
		$reg = $respuesta->fetch_object();
		if(isset($reg)){	
			$_SESSION['idUsuarioSisCob']=$reg->id_usuario;
			$_SESSION['apellidoNombre']=$reg->apellido_nombre;
			$_SESSION['usuario'] = $reg->usuario;
			$_SESSION['clave'] = $reg->clave;
			$_SESSION['idRol'] = $reg->id_rol;
			$_SESSION['rol'] = $reg->rol;
			$_SESSION['estado'] = $reg->estado;
			$_SESSION['v_acceso'] = $reg->v_acceso;
			$_SESSION['v_clientes'] = $reg->v_clientes;
			$_SESSION['v_ventas'] = $reg->v_ventas;
			$_SESSION['v_cuotas'] = $reg->v_cuotas;
			$_SESSION['v_pedidos'] = $reg->v_pedidos;
			$_SESSION['v_reclamos'] = $reg->v_reclamos;
			$_SESSION['v_pagos'] = $reg->v_pagos;
			$_SESSION['alt_acceso'] = $reg->alt_acceso;
			$_SESSION['alt_clientes'] = $reg->alt_clientes;
			$_SESSION['alt_ventas'] = $reg->alt_ventas;
			$_SESSION['alt_pagos'] = $reg->alt_pagos;
			$_SESSION['alt_cuotas'] = $reg->alt_cuotas;
			$_SESSION['alt_configuracion'] = $reg->alt_configuracion;
			$_SESSION['alt_pedidos'] = $reg->alt_pedidos;
			$_SESSION['alt_reclamos'] = $reg->alt_reclamos;
			$_SESSION['new_acceso'] = $reg->new_acceso;
			$_SESSION['new_clientes'] = $reg->new_clientes;
			$_SESSION['new_ventas'] = $reg->new_ventas;
			$_SESSION['new_pagos'] = $reg->new_pagos;
			$_SESSION['new_pedidos'] = $reg->new_pedidos;
			$_SESSION['new_reclamos'] = $reg->new_reclamos;
		}
		echo json_encode($reg);
	break;

	case 'cerrar_sesion':
		session_unset();
		session_destroy();
	break;
	
	
}

?>