<?php
require ('../modelos/Reclamo.php');
require ('../modelos/Cliente.php');

$r = new Reclamo();
$c = new Cliente();

$idReclamo = isset($_POST['idReclamo']) ? limpiarCadena($_POST['idReclamo']) : "";
$idCliente = isset($_POST['idCliente']) ? limpiarCadena($_POST['idCliente']) : "";
$fechaReclamo = isset($_POST['fechaReclamo']) ? limpiarCadena($_POST['fechaReclamo']) : "";
$estado = isset($_POST['estado']) ? limpiarCadena($_POST['estado']) : "";
$motivo = isset($_POST['motivo']) ? limpiarCadena($_POST['motivo']) : "";

switch ($_GET['op']) {

	case 'nuevo_editar':
		if ($fechaReclamo=="" || $idCliente=="" || $estado=="" || $motivo=="") {
			echo "¡Complete los campos obligatorios!";
		}else{
			$parts = explode('/',$fechaReclamo);
			$fechaFormateada = $parts[2] . '-' . $parts[1] . '-' . $parts[0];
			session_start();
			$idUltimaMod=$_SESSION['idUsuarioSisCob'];

			if (empty($idReclamo)) {
				if ($_SESSION['new_reclamos']==0) {
					echo "¡Acción denegada!";
				}else{
					$respuesta=$r->nuevo($fechaFormateada, $idCliente, $motivo, $estado, $idUltimaMod);
					echo $respuesta ? "¡Registro creado con exito!" : "¡Ocurrió un problema y no se pudo crear!";
				}			
			}else{
				if ($_SESSION['alt_reclamos']==0) {
					echo "¡Acción denegada!";
				}else{
					$respuesta=$r->editar($idReclamo, $fechaFormateada, $idCliente, $motivo, $estado, $idUltimaMod);
					echo $respuesta ? "¡Registro editado con exito!" : "¡Ocurrió un problema y no se pudo editar!";
				}			
			}
		}

		
	break;

	case 'mostrar':
		$respuesta=$r->mostrar($idReclamo);
		echo json_encode($respuesta);
	break;


	case 'listar':
		session_start();
		$alteracion=$_SESSION['alt_pedidos'];
		$respuesta=$r->listar();
		$data = Array();

		while($reg=$respuesta->fetch_object()){

			if($alteracion==1){
				$opciones='<button class="btn btn-warning" onclick="mostrar('.$reg->id_reclamo.')"><i class="fas fa-pencil-alt"></i></button>'; 
			}else{
				$opciones='';
			}

			if ($reg->estado=="PENDIENTE") {
				$estado='<span class="badge badge-warning">'.$reg->estado.'</span>';
			}else{
				if ($reg->estado=="SOLUCIONADO") {
					$estado='<span class="badge badge-success">'.$reg->estado.'</span>';
				}
			}

			$data[]=array(
				"0"=>$opciones,
				"1"=>$reg->fecha_reclamo,
				"2"=>$estado,
				"3"=>$reg->apellidoNombre,
				"4"=>$reg->dni,
				"5"=>$reg->motivo,
				
			);
		}

		$results=array(
			"sEcho"=>1, //informacion para el data table
			"iTotalRecords"=>count($data), //enbviamos total de registros para al datatable
			"iTotalDisplayRecords"=>count($data), //enviamos total de registros a visualizar
			"aaData"=>$data
		);
		echo json_encode($results);
	break;


	case 'listar_clientes':
		
		$respuesta=$c->listar();
		//declaramos un array
		$data = Array();
		while($reg=$respuesta->fetch_object()){

			$data[]=array(
				"0"=>'<button class="btn btn-success" onclick="agregar_cliente('.$reg->id_cliente.',\''.$reg->dni.'\',\''.$reg->apellido_nombre.'\')"><i class="fas fa-arrow-circle-right"></i></button>',
				"1"=>$reg->apellido_nombre,
				"2"=>$reg->dni,
				"3"=>$reg->barrio,	
				"4"=>$reg->domicilio,
				"5"=>$reg->telefono,
			);
		}

		$results=array(
			"sEcho"=>1, //informacion para el data table
			"iTotalRecords"=>count($data), //enbviamos total de registros para al datatable
			"iTotalDisplayRecords"=>count($data), //enviamos total de registros a visualizar
			"aaData"=>$data
		);
		echo json_encode($results);
	break;

	case 'cant_reclamos_pendientes_dashboard':
		$respuesta=$r->cant_reclamos_pendientes_dashboard();
		echo $respuesta['cant'];
	break;

	

}
?>