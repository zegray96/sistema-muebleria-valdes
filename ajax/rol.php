<?php
require ('../modelos/Rol.php');

$r = new Rol();

$idRol = isset($_POST['idRol']) ? limpiarCadena($_POST['idRol']) : "";
$nombre = isset($_POST['nombre']) ? limpiarCadena($_POST['nombre']) : "";
$vAcceso=isset($_POST['vAcceso'])? 1:0;
$vClientes=isset($_POST['vClientes'])? 1:0;
$vVentas=isset($_POST['vVentas'])? 1:0;
$vCuotas=isset($_POST['vCuotas'])? 1:0;
$vPagos=isset($_POST['vPagos'])? 1:0;
$vPedidos=isset($_POST['vPedidos'])? 1:0;
$vReclamos=isset($_POST['vReclamos'])? 1:0;
$newAcceso=isset($_POST['newAcceso'])? 1:0;
$newClientes=isset($_POST['newClientes'])? 1:0;
$newVentas=isset($_POST['newVentas'])? 1:0;
$newPagos=isset($_POST['newPagos'])? 1:0;
$newPedidos=isset($_POST['newPedidos'])? 1:0;
$newReclamos=isset($_POST['newReclamos'])? 1:0;
$altAcceso=isset($_POST['altAcceso'])? 1:0;
$altClientes=isset($_POST['altClientes'])? 1:0;
$altVentas=isset($_POST['altVentas'])? 1:0;
$altPagos=isset($_POST['altPagos'])? 1:0;
$altCuotas=isset($_POST['altCuotas'])? 1:0;
$altConfiguracion=isset($_POST['altConfiguracion'])? 1:0;
$altPedidos=isset($_POST['altPedidos'])? 1:0;
$altReclamos=isset($_POST['altReclamos'])? 1:0;

switch ($_GET['op']) {

	case 'nuevo_editar':
		if ($idRol==1) {
			echo "¡No puede editar rol Super Admin!";
		}else{

			if($nombre==""){
				echo "¡Complete los campos obligatorios!";
			}else{
				$verificacion=null;
				$nombreBd="";
				
				if(!empty($idRol)){
					$respuesta=$r->buscar_id($idRol);
					$nombreBd=$respuesta['nombre'];
				}

				if($nombre!=$nombreBd){
					$verificacion=$r->verificar_existencia_nombre($nombre);
				}


				if(!empty($verificacion)){
					echo "¡Nombre ya existe!";
				}else{
					if(empty($idRol)){
						session_start();
						if ($_SESSION['new_acceso']==0) {
							echo "¡Acción denegada!";
						}else{
							// Nuevo
							$respuesta = $r->nuevo($nombre,$vAcceso,$vClientes,$vVentas,$vCuotas,$vPagos,$vPedidos,$vReclamos,$altAcceso,$altClientes,$altVentas,$altPagos,$altCuotas,$altConfiguracion,$altPedidos,$altReclamos,$newAcceso,$newClientes,$newVentas,$newPagos,$newPedidos,$newReclamos);
							echo $respuesta ? "¡Registro creado con exito!" : "¡Ocurrió un problema y no se pudo crear!";
						}
						
					}else{
						session_start();
						if ($_SESSION['alt_acceso']==0) {
							echo "¡Acción denegada!";
						}else{
							// Editar
							$respuesta = $r->editar($idRol,$nombre,$vAcceso,$vClientes,$vVentas,$vCuotas,$vPagos,$vPedidos,$vReclamos,$altAcceso,$altClientes,$altVentas,$altPagos,$altCuotas,$altConfiguracion,$altPedidos,$altReclamos,$newAcceso,$newClientes,$newVentas,$newPagos,$newPedidos,$newReclamos);
							echo $respuesta ? "¡Registro editado con exito!" : "¡Ocurrió un problema y no se pudo editar!";
						}
						
					}
				}	

			}
			
		}
			
	break;

	case 'mostrar':
		$respuesta=$r->buscar_id($idRol);
		echo json_encode($respuesta);
	break;

	case 'listar':
		session_start();
		$alteracion=$_SESSION['alt_acceso'];
		$respuesta=$r->listar();
		$data = Array();

		while($reg=$respuesta->fetch_object()){

			if($alteracion==1 && $reg->id_rol!=1){
				$opciones='<button class="btn btn-warning" onclick="mostrar('.$reg->id_rol.')"><i class="fas fa-pencil-alt"></i></button>'; 
			}else{
				$opciones='';
			}

			$data[]=array(
				"0"=>$opciones,
				"1"=>$reg->nombre,
			);
		}

		$results=array(
			"sEcho"=>1, //informacion para el data table
			"iTotalRecords"=>count($data), //enbviamos total de registros para al datatable
			"iTotalDisplayRecords"=>count($data), //enviamos total de registros a visualizar
			"aaData"=>$data
		);
		echo json_encode($results);
	break;

}
?>