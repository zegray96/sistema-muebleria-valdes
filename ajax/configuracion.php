<?php
require ('../modelos/Configuracion.php');

$c = new Configuracion();

$nombreEmpresa = isset($_POST['nombreEmpresa']) ? limpiarCadena($_POST['nombreEmpresa']) : "";
$descripcionCortaEmpresa = isset($_POST['descripcionCortaEmpresa']) ? limpiarCadena($_POST['descripcionCortaEmpresa']) : "";
$telefono = isset($_POST['telefono']) ? limpiarCadena($_POST['telefono']) : "";


switch ($_GET['op']) {
	case 'editar':
		if($nombreEmpresa=="" || $descripcionCortaEmpresa=="" || $telefono==""){
			echo "¡Complete los campos obligatorios!";
		}else{
			
			if(is_uploaded_file($_FILES['logoEmpresa']['tmp_name'])){
				move_uploaded_file($_FILES['logoEmpresa']['tmp_name'], "../assets/img/logo-empresa.png");
			}

			// Editar
			$respuesta = $c->editar($nombreEmpresa,$descripcionCortaEmpresa,$telefono);
			echo $respuesta ? "¡Registro editado con exito!" : "¡Ocurrió un problema y no se pudo editar!";

			
		}
	break;

	case 'mostrar':
		$respuesta = $c->traer_configuracion();
		echo json_encode($respuesta);
	break;

	
}

?>
