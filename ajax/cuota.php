<?php
require ('../modelos/Cuota.php');
require ('../modelos/Venta.php');
require ('../modelos/Cliente.php');
require ('../modelos/Usuario.php');

$c = new Cuota();
$v = new Venta();
$cl = new Cliente();
$u = new Usuario();


$idVenta = isset($_POST['idVenta']) ? limpiarCadena($_POST['idVenta']) : "";
$nroCuota = isset($_POST['nroCuota']) ? limpiarCadena($_POST['nroCuota']) : "";
$fechaVencimiento = isset($_POST['fechaVencimiento']) ? limpiarCadena($_POST['fechaVencimiento']) : "";
$observacion = isset($_POST['observacion']) ? limpiarCadena($_POST['observacion']) : "";
$montoAdicional = isset($_POST['montoAdicional']) ? limpiarCadena($_POST['montoAdicional']) : "";



switch ($_GET['op']) {

	case 'editar':
		$montoAdicionalSinComa=str_replace(array(','), '' , $montoAdicional);
		
		$parts = explode('/',$fechaVencimiento);
		$fechaFormateada = $parts[2] . '-' . $parts[1] . '-' . $parts[0];
		session_start();
		$idUltimaMod=$_SESSION['idUsuarioSisCob'];

		$respuesta=$c->buscar_id($idVenta,$nroCuota);
		$montoAdicionalBd=$respuesta['monto_adicional'];
		$estadoBd=$respuesta['estado'];

		if ($estadoBd=="PAGADA" && $montoAdicionalBd!=$montoAdicionalSinComa) {
			echo "¡No puede modificar el monto adicional si la cuota esta pagada!";
		}else{
			if ($_SESSION['alt_cuotas']==0) {
				echo "¡Acción denegada!";
			}else{
				$respuesta = $c->editar($idVenta,$nroCuota,$fechaFormateada,$observacion,$montoAdicionalSinComa,$montoAdicionalBd,$idUltimaMod);
				echo $respuesta ? "¡Registro editado con exito!" : "¡Ocurrió un problema y no se pudo editar!";
			}
		}	
	break;

	case 'mostrar':
		$respuesta=$c->buscar_id($idVenta,$nroCuota);
		echo json_encode($respuesta);
	break;

	case 'listar_todas':
		session_start();
		$alteracion=$_SESSION['alt_cuotas'];
		$respuesta=$c->listar_todas();
		$data = Array();

		while($reg=$respuesta->fetch_object()){
			$saldoPendienteTotal=$v->saldo_pendiente_total($reg->id_cliente);
			$saldoPendienteTotal=$saldoPendienteTotal['saldoPendienteTotal'];
			if ($saldoPendienteTotal==null) {
				$saldoPendienteTotal=0;
			}

			$saldoPendienteVenta=$v->saldo_pendiente_venta($reg->id_venta);
			$saldoPendienteVenta=$saldoPendienteVenta['saldoPendienteVenta'];
			

			if($alteracion==1){
				$opciones='<button class="btn btn-warning" onclick="mostrar_cuota('.$reg->id_venta.','.$reg->nro_cuota.')"><i class="fas fa-pencil-alt"></i></button>'; 
			}else{
				$opciones='';
			}

			if($reg->estado=="PENDIENTE"){
				$estado='<span class="badge badge-warning">'.$reg->estado.'</span>';
			}else{
				if($reg->estado=="PAGADA"){
					$estado='<span class="badge badge-success">'.$reg->estado.'</span>';
				}else{
					if($reg->estado=="VENCIDA"){
						$estado='<span class="badge badge-danger">'.$reg->estado.'</span>';
					}else{
						if($reg->estado=="EN PROCESO"){
							$estado='<span class="badge badge-info">'.$reg->estado.'</span>';
						}
					}
				}
			}

			$data[]=array(
				"0"=>$opciones,
				"1"=>str_pad($reg->nroComprobanteVentaAsociado, 8 ,"0", STR_PAD_LEFT),
				"2"=>$reg->nro_cuota,
				"3"=>$estado,
				"4"=>$reg->fecha_vencimiento,
				"5"=>$reg->apellidoNombre,
				"6"=>$reg->barrio,
				"7"=>$reg->domicilio,
				"8"=>$reg->cobrador,
				"9"=>"$".number_format($saldoPendienteTotal, 2),
				"10"=>"$".number_format($saldoPendienteVenta, 2),
				
				
						
			);
		}

		$results=array(
			"sEcho"=>1, //informacion para el data table
			"iTotalRecords"=>count($data), //enbviamos total de registros para al datatable
			"iTotalDisplayRecords"=>count($data), //enviamos total de registros a visualizar
			"aaData"=>$data
		);
		echo json_encode($results);
	break;

	case 'listar_vencimientos_hoy':
		session_start();
		$alteracion=$_SESSION['alt_cuotas'];
		$respuesta=$c->listar_vencimientos_hoy();
		$data = Array();

		while($reg=$respuesta->fetch_object()){
			$saldoPendienteTotal=$v->saldo_pendiente_total($reg->id_cliente);
			$saldoPendienteTotal=$saldoPendienteTotal['saldoPendienteTotal'];
			if ($saldoPendienteTotal==null) {
				$saldoPendienteTotal=0;
			}

			$saldoPendienteVenta=$v->saldo_pendiente_venta($reg->id_venta);
			$saldoPendienteVenta=$saldoPendienteVenta['saldoPendienteVenta'];

			if($alteracion==1){
				$opciones='<button class="btn btn-warning" onclick="mostrar_cuota('.$reg->id_venta.','.$reg->nro_cuota.')"><i class="fas fa-pencil-alt"></i></button>';  
			}else{
				$opciones='';
			}

			if($reg->estado=="PENDIENTE"){
				$estado='<span class="badge badge-warning">'.$reg->estado.'</span>';
			}else{
				if($reg->estado=="PAGADA"){
					$estado='<span class="badge badge-success">'.$reg->estado.'</span>';
				}else{
					if($reg->estado=="VENCIDA"){
						$estado='<span class="badge badge-danger">'.$reg->estado.'</span>';
					}else{
						if($reg->estado=="EN PROCESO"){
							$estado='<span class="badge badge-info">'.$reg->estado.'</span>';
						}
					}
				}
			}

			$data[]=array(
				"0"=>$opciones,
				"1"=>str_pad($reg->nroComprobanteVentaAsociado, 8 ,"0", STR_PAD_LEFT),
				"2"=>$reg->nro_cuota,
				"3"=>$estado,
				"4"=>$reg->fecha_vencimiento,
				"5"=>$reg->apellidoNombre,
				"6"=>$reg->barrio,
				"7"=>$reg->domicilio,
				"8"=>$reg->cobrador,
				"9"=>"$".number_format($saldoPendienteTotal, 2),
				"10"=>"$".number_format($saldoPendienteVenta, 2),
			);
		}

		$results=array(
			"sEcho"=>1, //informacion para el data table
			"iTotalRecords"=>count($data), //enbviamos total de registros para al datatable
			"iTotalDisplayRecords"=>count($data), //enviamos total de registros a visualizar
			"aaData"=>$data
		);
		echo json_encode($results);
	break;

	case 'listar_por_cliente':
		session_start();
		$alteracion=$_SESSION['alt_cuotas'];
		$idCliente=$_REQUEST['idCliente'];
		$respuesta=$c->listar_por_cliente($idCliente);
		$data = Array();

		while($reg=$respuesta->fetch_object()){
			$saldoPendienteTotal=$v->saldo_pendiente_total($reg->id_cliente);
			$saldoPendienteTotal=$saldoPendienteTotal['saldoPendienteTotal'];
			if ($saldoPendienteTotal==null) {
				$saldoPendienteTotal=0;
			}

			$saldoPendienteVenta=$v->saldo_pendiente_venta($reg->id_venta);
			$saldoPendienteVenta=$saldoPendienteVenta['saldoPendienteVenta'];

			if($alteracion==1){
				$opciones='<button class="btn btn-warning" onclick="mostrar_cuota('.$reg->id_venta.','.$reg->nro_cuota.')"><i class="fas fa-pencil-alt"></i></button>';  
			}else{
				$opciones='';
			}

			if($reg->estado=="PENDIENTE"){
				$estado='<span class="badge badge-warning">'.$reg->estado.'</span>';
			}else{
				if($reg->estado=="PAGADA"){
					$estado='<span class="badge badge-success">'.$reg->estado.'</span>';
				}else{
					if($reg->estado=="VENCIDA"){
						$estado='<span class="badge badge-danger">'.$reg->estado.'</span>';
					}else{
						if($reg->estado=="EN PROCESO"){
							$estado='<span class="badge badge-info">'.$reg->estado.'</span>';
						}
					}
				}
			}

			$data[]=array(
				"0"=>$opciones,
				"1"=>str_pad($reg->nroComprobanteVentaAsociado, 8 ,"0", STR_PAD_LEFT),
				"2"=>$reg->nro_cuota,
				"3"=>$estado,
				"4"=>$reg->fecha_vencimiento,
				"5"=>$reg->apellidoNombre,
				"6"=>$reg->barrio,
				"7"=>$reg->domicilio,
				"8"=>$reg->cobrador,
				"9"=>"$".number_format($saldoPendienteTotal, 2),
				"10"=>"$".number_format($saldoPendienteVenta, 2),
			);
		}

		$results=array(
			"sEcho"=>1, //informacion para el data table
			"iTotalRecords"=>count($data), //enbviamos total de registros para al datatable
			"iTotalDisplayRecords"=>count($data), //enviamos total de registros a visualizar
			"aaData"=>$data
		);
		echo json_encode($results);
	break;

	case 'listar_por_fecha_vencimiento':
		$fechaIni=$_REQUEST['fechaIni'];
		$fechaFin=$_REQUEST['fechaFin'];

		$parts = explode('/',$fechaIni);
		$fechaIniFormateada = $parts[2] . '-' . $parts[1] . '-' . $parts[0];

		$parts = explode('/',$fechaFin);
		$fechaFinFormateada = $parts[2] . '-' . $parts[1] . '-' . $parts[0];

		session_start();
		$alteracion=$_SESSION['alt_cuotas'];
		$respuesta=$c->listar_por_fecha_vencimiento($fechaIniFormateada,$fechaFinFormateada);
		$data = Array();

		while($reg=$respuesta->fetch_object()){
			$saldoPendienteTotal=$v->saldo_pendiente_total($reg->id_cliente);
			$saldoPendienteTotal=$saldoPendienteTotal['saldoPendienteTotal'];
			if ($saldoPendienteTotal==null) {
				$saldoPendienteTotal=0;
			}

			$saldoPendienteVenta=$v->saldo_pendiente_venta($reg->id_venta);
			$saldoPendienteVenta=$saldoPendienteVenta['saldoPendienteVenta'];

			if($alteracion==1){
				$opciones='<button class="btn btn-warning" onclick="mostrar_cuota('.$reg->id_venta.','.$reg->nro_cuota.')"><i class="fas fa-pencil-alt"></i></button>';  
			}else{
				$opciones='';
			}

			if($reg->estado=="PENDIENTE"){
				$estado='<span class="badge badge-warning">'.$reg->estado.'</span>';
			}else{
				if($reg->estado=="PAGADA"){
					$estado='<span class="badge badge-success">'.$reg->estado.'</span>';
				}else{
					if($reg->estado=="VENCIDA"){
						$estado='<span class="badge badge-danger">'.$reg->estado.'</span>';
					}else{
						if($reg->estado=="EN PROCESO"){
							$estado='<span class="badge badge-info">'.$reg->estado.'</span>';
						}
					}
				}
			}

			$data[]=array(
				"0"=>$opciones,
				"1"=>str_pad($reg->nroComprobanteVentaAsociado, 8 ,"0", STR_PAD_LEFT),
				"2"=>$reg->nro_cuota,
				"3"=>$estado,
				"4"=>$reg->fecha_vencimiento,
				"5"=>$reg->apellidoNombre,
				"6"=>$reg->barrio,
				"7"=>$reg->domicilio,
				"8"=>$reg->cobrador,
				"9"=>"$".number_format($saldoPendienteTotal, 2),
				"10"=>"$".number_format($saldoPendienteVenta, 2),
			);
		}

		$results=array(
			"sEcho"=>1, //informacion para el data table
			"iTotalRecords"=>count($data), //enbviamos total de registros para al datatable
			"iTotalDisplayRecords"=>count($data), //enviamos total de registros a visualizar
			"aaData"=>$data
		);
		echo json_encode($results);
	break;

	case 'listar_clientes':
		
		$respuesta=$cl->listar();
		//declaramos un array
		$data = Array();
		while($reg=$respuesta->fetch_object()){
			$data[]=array(
				"0"=>'<button class="btn btn-success" onclick="listar_por_cliente('.$reg->id_cliente.',\''.$reg->apellido_nombre.'\')"><i class="fas fa-arrow-circle-right"></i></button>',
				"1"=>$reg->apellido_nombre,
				"2"=>$reg->dni,
				"3"=>$reg->barrio,	
				"4"=>$reg->domicilio,
				"5"=>$reg->telefono,
			);
		}

		$results=array(
			"sEcho"=>1, //informacion para el data table
			"iTotalRecords"=>count($data), //enbviamos total de registros para al datatable
			"iTotalDisplayRecords"=>count($data), //enviamos total de registros a visualizar
			"aaData"=>$data
		);
		echo json_encode($results);
	break;

	case 'actualizar_cuotas_vencidas':
		$respuesta=$c->actualizar_cuotas_vencidas();
		echo $respuesta ? "¡Estado de cuotas actualizadas con exito!" : "¡Ocurrió un problema y no se pudo actualizar!";
	break;

	case 'cant_cuotas_venc_hoy_dashboard':
		$resultado=$c->cant_cuotas_venc_hoy_dashboard();
		echo $resultado['cant'];
	break;

	case 'listar_por_cobrador_cargar_datos':
		
		$resultado=$u->cargar_cobradores();
		echo '<option value="" selected>Todos</option>';
		while($reg=$resultado->fetch_object()){
			echo '<option value='.$reg->apellido_nombre.'>'.$reg->apellido_nombre.'</option>';
		}
	break;

}
?>