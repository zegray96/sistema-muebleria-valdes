<?php
require ('../modelos/Pedido.php');

$p = new Pedido();

$idPedido = isset($_POST['idPedido']) ? limpiarCadena($_POST['idPedido']) : "";
$fechaEntrega = isset($_POST['fechaEntrega']) ? limpiarCadena($_POST['fechaEntrega']) : "";
$descripcion = isset($_POST['descripcion']) ? limpiarCadena($_POST['descripcion']) : "";
$apellidoNombre = isset($_POST['apellidoNombre']) ? limpiarCadena($_POST['apellidoNombre']) : "";
$domicilio = isset($_POST['domicilio']) ? limpiarCadena($_POST['domicilio']) : "";
$telefono = isset($_POST['telefono']) ? limpiarCadena($_POST['telefono']) : "";
$estado = isset($_POST['estado']) ? limpiarCadena($_POST['estado']) : "";

switch ($_GET['op']) {

	case 'nuevo_editar':

		$parts = explode('/',$fechaEntrega);
		$fechaFormateada = $parts[2] . '-' . $parts[1] . '-' . $parts[0];
		if($fechaEntrega=="" || $descripcion=="" || $apellidoNombre=="" || $domicilio=="" || $telefono=="" || $estado==""){
			echo "¡Complete los campos obligatorios!";
		}else{
			session_start();
			$idUltimaMod=$_SESSION['idUsuarioSisCob'];

			if(empty($idPedido)){
				if ($_SESSION['new_pedidos']==0) {
					echo "¡Acción denegada!";
				}else{
					// Nuevo
					$respuesta = $p->nuevo($fechaFormateada,$descripcion,$apellidoNombre,$domicilio,$telefono,$estado,$idUltimaMod);
					echo $respuesta ? "¡Registro creado con exito!" : "¡Ocurrió un problema y no se pudo crear!";
				}

			}else{
				if ($_SESSION['alt_pedidos']==0) {
					echo "¡Acción denegada!";
				}else{
					// Editar
					$respuesta = $p->editar($idPedido,$fechaFormateada,$descripcion,$apellidoNombre,$domicilio,$telefono,$estado,$idUltimaMod);
					echo $respuesta ? "¡Registro editado con exito!" : "¡Ocurrió un problema y no se pudo editar!";
				}

			}

		}
	break;

	case 'mostrar':
		$respuesta=$p->buscar_id($idPedido);
		echo json_encode($respuesta);
	break;

	case 'listar_todos':
		session_start();
		$alteracion=$_SESSION['alt_pedidos'];
		$respuesta=$p->listar_todos();
		$data = Array();

		while($reg=$respuesta->fetch_object()){

			if($alteracion==1){
				$opciones='<button class="btn btn-warning" onclick="mostrar('.$reg->id_pedido.')"><i class="fas fa-pencil-alt"></i></button>'; 
			}else{
				$opciones='';
			}

			if ($reg->estado=="PENDIENTE") {
				$estado='<span class="badge badge-warning">'.$reg->estado.'</span>';
			}else{
				if ($reg->estado=="ENTREGADO") {
					$estado='<span class="badge badge-success">'.$reg->estado.'</span>';
				}
			}

			$data[]=array(
				"0"=>$opciones,
				"1"=>$reg->fecha_entrega,
				"2"=>$estado,
				"3"=>$reg->descripcion,
				"4"=>$reg->apellido_nombre,
				"5"=>$reg->domicilio,
				"6"=>$reg->telefono,
			);
		}

		$results=array(
			"sEcho"=>1, //informacion para el data table
			"iTotalRecords"=>count($data), //enbviamos total de registros para al datatable
			"iTotalDisplayRecords"=>count($data), //enviamos total de registros a visualizar
			"aaData"=>$data
		);
		echo json_encode($results);
	break;

	case 'listar_pedidos_para_hoy':
		session_start();
		$alteracion=$_SESSION['alt_pedidos'];
		$respuesta=$p->listar_pedidos_para_hoy();
		$data = Array();

		while($reg=$respuesta->fetch_object()){

			if($alteracion==1){
				$opciones='<button class="btn btn-warning" onclick="mostrar('.$reg->id_pedido.')"><i class="fas fa-pencil-alt"></i></button>'; 
			}else{
				$opciones='';
			}

			if ($reg->estado=="PENDIENTE") {
				$estado='<span class="badge badge-warning">'.$reg->estado.'</span>';
			}else{
				if ($reg->estado=="ENTREGADO") {
					$estado='<span class="badge badge-success">'.$reg->estado.'</span>';
				}
			}

			$data[]=array(
				"0"=>$opciones,
				"1"=>$reg->fecha_entrega,
				"2"=>$estado,
				"3"=>$reg->descripcion,
				"4"=>$reg->apellido_nombre,
				"5"=>$reg->domicilio,
				"6"=>$reg->telefono,
			);
		}

		$results=array(
			"sEcho"=>1, //informacion para el data table
			"iTotalRecords"=>count($data), //enbviamos total de registros para al datatable
			"iTotalDisplayRecords"=>count($data), //enviamos total de registros a visualizar
			"aaData"=>$data
		);
		echo json_encode($results);
	break;

	case 'listar_por_fecha':
		$fechaIni=$_REQUEST['fechaIni'];
		$fechaFin=$_REQUEST['fechaFin'];

		$parts = explode('/',$fechaIni);
		$fechaIniFormateada = $parts[2] . '-' . $parts[1] . '-' . $parts[0];

		$parts = explode('/',$fechaFin);
		$fechaFinFormateada = $parts[2] . '-' . $parts[1] . '-' . $parts[0];
		
		session_start();
		$alteracion=$_SESSION['alt_pedidos'];
		$respuesta=$p->listar_por_fecha($fechaIniFormateada,$fechaFinFormateada);
		$data = Array();

		while($reg=$respuesta->fetch_object()){
			
			if($alteracion==1){
				$opciones='<button class="btn btn-warning" onclick="mostrar('.$reg->id_pedido.')"><i class="fas fa-pencil-alt"></i></button>'; 
			}else{
				$opciones='';
			}

			if ($reg->estado=="PENDIENTE") {
				$estado='<span class="badge badge-warning">'.$reg->estado.'</span>';
			}else{
				if ($reg->estado=="ENTREGADO") {
					$estado='<span class="badge badge-success">'.$reg->estado.'</span>';
				}
			}

			$data[]=array(
				"0"=>$opciones,
				"1"=>$reg->fecha_entrega,
				"2"=>$estado,
				"3"=>$reg->descripcion,
				"4"=>$reg->apellido_nombre,
				"5"=>$reg->domicilio,
				"6"=>$reg->telefono,	
			);
		}

		$results=array(
			"sEcho"=>1, //informacion para el data table
			"iTotalRecords"=>count($data), //enbviamos total de registros para al datatable
			"iTotalDisplayRecords"=>count($data), //enviamos total de registros a visualizar
			"aaData"=>$data
		);
		echo json_encode($results);
	break;

	case 'cant_pedidos_para_hoy_dashboard':
		$resultado=$p->cant_pedidos_para_hoy_dashboard();
		echo $resultado['cant'];
	break;

}
?>