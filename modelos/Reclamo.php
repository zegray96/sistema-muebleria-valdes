<?php
require ('../config/Conexion.php');

Class Reclamo{
	public function __construct(){

	}

	public function nuevo($fechaReclamo, $idCliente, $motivo, $estado, $idUltimaMod){
		date_default_timezone_set('America/Argentina/Buenos_Aires');
		$fechaHoraUltimaMod = date("Y-m-d H:i:s");

		$sql="INSERT INTO reclamos (fecha_reclamo, id_cliente, motivo, estado, id_ultima_mod, fecha_hora_ultima_mod) VALUES ('$fechaReclamo', '$idCliente', '$motivo', '$estado', '$idUltimaMod', '$fechaHoraUltimaMod')";
		return ejecutarConsulta($sql);
	}

	public function editar($idReclamo, $fechaReclamo, $idCliente, $motivo, $estado, $idUltimaMod){
		date_default_timezone_set('America/Argentina/Buenos_Aires');
		$fechaHoraUltimaMod = date("Y-m-d H:i:s"); 

		$sql="UPDATE reclamos SET fecha_reclamo='$fechaReclamo', id_cliente='$idCliente', motivo='$motivo', estado='$estado', id_ultima_mod='$idUltimaMod', fecha_hora_ultima_mod='$fechaHoraUltimaMod' WHERE id_reclamo='$idReclamo'";
		return ejecutarConsulta($sql);
	}

	public function mostrar($idReclamo){
		$sql="SELECT r.id_reclamo, CONCAT(LPAD(DAY(r.fecha_reclamo),2,'0'),'/', LPAD(MONTH(r.fecha_reclamo),2,'0'), '/', YEAR(r.fecha_reclamo)) as fecha_reclamo, r.id_cliente, c.apellido_nombre as apellidoNombre, c.dni as dni, r.estado, r.motivo, u.apellido_nombre as ultimaModNombre, r.fecha_hora_ultima_mod  FROM reclamos r
		INNER JOIN clientes c ON c.id_cliente=r.id_cliente
		INNER JOIN usuarios u ON u.id_usuario=r.id_ultima_mod
		WHERE id_reclamo='$idReclamo'";
		return ejecutarConsultaSimpleFila($sql);
	}

	public function listar(){
		$sql="SELECT r.id_reclamo, r.fecha_reclamo, r.id_cliente, c.apellido_nombre as apellidoNombre, c.dni as dni, r.estado, r.motivo FROM reclamos r
		INNER JOIN clientes c ON c.id_cliente=r.id_cliente";
		return ejecutarConsulta($sql);
	}

	public function cant_reclamos_pendientes_dashboard(){
		$sql="SELECT COUNT(id_reclamo) AS cant FROM reclamos WHERE estado='PENDIENTE'";
		return ejecutarConsultaSimpleFila($sql);
	}

	
}

?>