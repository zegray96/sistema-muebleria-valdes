<?php
require ('../config/Conexion.php');

Class Usuario{
	public function __construct(){

	}

	public function nuevo($apellidoNombre,$usuario,$clave,$idRol,$estado){
		$sql="INSERT INTO usuarios (apellido_nombre, usuario, clave, id_rol, estado) VALUES ('$apellidoNombre', '$usuario', '$clave', '$idRol', '$estado')";
		return ejecutarConsulta($sql);
	}

	public function editar($idUsuario,$apellidoNombre,$usuario,$idRol,$estado){
		$sql="UPDATE usuarios SET apellido_nombre='$apellidoNombre', usuario='$usuario', id_rol='$idRol', estado='$estado' WHERE id_usuario='$idUsuario'";
		return ejecutarConsulta($sql);
	}

	public function modificar_cuenta($idUsuario,$apellidoNombre,$usuario){
		$sql="UPDATE usuarios SET apellido_nombre='$apellidoNombre', usuario='$usuario' WHERE id_usuario='$idUsuario'";
		return ejecutarConsulta($sql);
	}

	public function modificar_clave($idUsuario,$clave){
		$sql="UPDATE usuarios SET clave='$clave' WHERE id_usuario='$idUsuario'";
		return ejecutarConsulta($sql);
	}

	public function listar(){
		$sql="SELECT u.id_usuario, u.apellido_nombre, u.usuario, u.clave, r.nombre as rol, u.estado FROM usuarios u
		INNER JOIN roles r ON r.id_rol = u.id_rol";
		return ejecutarConsulta($sql);
	}

	public function buscar_id($idUsuario){
		$sql="SELECT * FROM usuarios WHERE id_usuario='$idUsuario'";
		return ejecutarConsultaSimpleFila($sql);
	}

	public function verificar_login($usuario,$clave){
		$sql="SELECT u.id_usuario, u.apellido_nombre, u.usuario, u.clave, u.id_rol, r.nombre as rol, u.estado, r.v_acceso as v_acceso,
		r.v_clientes as v_clientes, r.v_ventas as v_ventas, r.v_cuotas as v_cuotas, r.v_pedidos as v_pedidos, r.v_reclamos as v_reclamos, r.v_pagos as v_pagos, r.alt_acceso as alt_acceso, r.alt_clientes as alt_clientes, r.alt_ventas as alt_ventas, r.alt_pagos as alt_pagos, r.alt_cuotas as alt_cuotas, r.alt_configuracion as alt_configuracion, r.alt_pedidos as alt_pedidos, r.alt_reclamos as alt_reclamos, r.new_acceso as new_acceso, r.new_clientes as new_clientes, r.new_ventas as new_ventas, r.new_pagos as new_pagos, r.new_pedidos as new_pedidos, r.new_reclamos as new_reclamos FROM usuarios u
		INNER JOIN roles r ON r.id_rol = u.id_rol
		WHERE u.usuario = '$usuario' AND u.clave = '$clave' AND u.estado='ACTIVO'";
		return ejecutarConsulta($sql);
	}

	public function cargar_cobradores(){
		$sql="SELECT * FROM usuarios ORDER BY apellido_nombre ASC";
		return ejecutarConsulta($sql);
	}

	public function verificar_existencia_usuario($usuario){
		$sql="SELECT * FROM usuarios WHERE usuario='$usuario'";
		return ejecutarConsultaSimpleFila($sql);
	}
}

?>
