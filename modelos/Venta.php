<?php
require ('../config/Conexion.php');

Class Venta{
	public function __construct(){

	}

	public function nuevo($fechaVenta, $idCliente, $idCobrador, $diaCobro, $montoAbonado, $cuotas, $montoCuota, $total, $idUltimaMod, $descripcion, $precioVenta, $cantidad, $subtotal){
		if($montoAbonado==""){
			$montoAbonado=0;
		}

		$estado="PENDIENTE";
		$saldoPendiente=$total-$montoAbonado;


		date_default_timezone_set('America/Argentina/Buenos_Aires');
		$fechaHoraUltimaMod = date("Y-m-d H:i:s");

		$respuesta=self::generar_nro_comprobante();
		if ($respuesta==null) {
			$nroComprobante=1;
		}else{
			$nroComprobante=$respuesta['nro_comprobante'];
		}


		$sql="INSERT INTO ventas (nro_comprobante, fecha_venta, id_cliente, id_cobrador, dia_cobro, monto_abonado, cuotas, monto_cuota, total_venta, id_ultima_mod, fecha_hora_ultima_mod, estado, saldo_pendiente) VALUES ('$nroComprobante','$fechaVenta','$idCliente','$idCobrador','$diaCobro','$montoAbonado','$cuotas','$montoCuota','$total','$idUltimaMod','$fechaHoraUltimaMod','$estado','$saldoPendiente')";
		$idVenta=ejecutarConsulta_retornarID($sql);

		

		//crear detalles
		$i=0;
		$sw=true;
		while($i<count($descripcion)){
			$sql="INSERT INTO detalles_ventas (descripcion, precio_venta, cantidad, subtotal, id_venta) VALUES ('$descripcion[$i]', '$precioVenta[$i]','$cantidad[$i]', '$subtotal[$i]', '$idVenta')";
			ejecutarConsulta($sql) or $sw=false;
			$i++;
		}

		//crear cuotas
		$fechaEntera = strtotime($fechaVenta);
		if ($montoAbonado>0) {
			// si paga algo, comenzara el mes siguiente
			$mes = date('m', $fechaEntera)+1;
		}else{
			// si no pago nada, la primer cuota sera este mes
			$mes = date('m', $fechaEntera);
		}
		
		$anio = date('Y', $fechaEntera);

		$i=1;
		while ($i<=$cuotas) {
			$dia=$diaCobro;
				// si estamos en enero, incrementar el año
			if($mes==13){
				$mes=01;
				$anio++;
			}

			$verif=false;
			while ($verif==false) {
				$verif=checkdate($mes, $dia, $anio);
				if($verif){
					$fechaVencimiento=$anio.'-'.$mes.'-'.$dia;
					$sql="INSERT INTO cuotas (id_venta, fecha_vencimiento, nro_cuota, estado, id_ultima_mod, fecha_hora_ultima_mod) VALUES ('$idVenta','$fechaVencimiento','$i','PENDIENTE','$idUltimaMod','$fechaHoraUltimaMod')";
					ejecutarConsulta($sql) or $sw=false;
					$i++;
					$mes++;
				}else{
					$dia--;
				}
			}
		}

		return $sw.":".$idVenta;

		
	}


	public function editar_todo($idVenta, $fechaVenta, $idCliente, $idCobrador, $diaCobro, $montoAbonado, $cuotas, $montoCuota, $total, $idUltimaMod, $descripcion, $precioVenta, $cantidad, $subtotal){
		if($montoAbonado==""){
			$montoAbonado=0;
		}

		$saldoPendiente=$total-$montoAbonado;


		date_default_timezone_set('America/Argentina/Buenos_Aires');
		$fechaHoraUltimaMod = date("Y-m-d H:i:s");
		

		$sql="UPDATE ventas SET fecha_venta='$fechaVenta', id_cliente='$idCliente', id_cobrador='$idCobrador', dia_cobro='$diaCobro', monto_abonado='$montoAbonado', cuotas='$cuotas', monto_cuota='$montoCuota', saldo_pendiente='$saldoPendiente', total_venta='$total', id_ultima_mod='$idUltimaMod', fecha_hora_ultima_mod='$fechaHoraUltimaMod' WHERE id_venta='$idVenta'";
		ejecutarConsulta($sql);

		
		// eliminar detalles 
		$sql="DELETE FROM detalles_ventas WHERE id_venta='$idVenta'";
		ejecutarConsulta($sql);

		//crear detalles
		$i=0;
		$sw=true;
		while($i<count($descripcion)){
			$sql="INSERT INTO detalles_ventas (descripcion, precio_venta, cantidad, subtotal, id_venta) VALUES ('$descripcion[$i]', '$precioVenta[$i]','$cantidad[$i]', '$subtotal[$i]', '$idVenta')";
			ejecutarConsulta($sql) or $sw=false;
			$i++;
		}

		//eliminar cuotas
		$sql="DELETE from cuotas where id_venta='$idVenta'";
		ejecutarConsulta($sql);

		//crear cuotas
		$fechaEntera = strtotime($fechaVenta);
		if ($montoAbonado>0) {
			// si paga algo, comenzara el mes siguiente
			$mes = date('m', $fechaEntera)+1;
		}else{
			// si no pago nada, la primer cuota sera este mes
			$mes = date('m', $fechaEntera);
		}
		$anio = date('Y', $fechaEntera);

		$i=1;
		while ($i<=$cuotas) {
			$dia=$diaCobro;
				// si estamos en enero, incrementar el año
			if($mes==13){
				$mes=01;
				$anio++;
			}

			$verif=false;
			while ($verif==false) {
				$verif=checkdate($mes, $dia, $anio);
				if($verif){
					$fechaVencimiento=$anio.'-'.$mes.'-'.$dia;
					$sql="INSERT INTO cuotas (id_venta, fecha_vencimiento, nro_cuota, estado, id_ultima_mod, fecha_hora_ultima_mod) VALUES ('$idVenta','$fechaVencimiento','$i','PENDIENTE','$idUltimaMod','$fechaHoraUltimaMod')";
					ejecutarConsulta($sql) or $sw=false;
					$i++;
					$mes++;
				}else{
					$dia--;
				}
			}
		}

		return $sw.":".$idVenta;

		
	}

	public function editar_menos_cuotas($idVenta, $fechaVenta, $idCliente, $idCobrador, $idUltimaMod){

		date_default_timezone_set('America/Argentina/Buenos_Aires');
		$fechaHoraUltimaMod = date("Y-m-d H:i:s");
		

		$sql="UPDATE ventas SET fecha_venta='$fechaVenta', id_cliente='$idCliente', id_cobrador='$idCobrador', id_ultima_mod='$idUltimaMod', fecha_hora_ultima_mod='$fechaHoraUltimaMod' WHERE id_venta='$idVenta'";
		return ejecutarConsulta($sql).":".$idVenta;

	
		
	}




	public function listar(){
		$sql="SELECT v.id_venta, v.nro_comprobante, v.fecha_venta, c.apellido_nombre as apellidoNombre, c.dni as dni, u.apellido_nombre as cobrador, v.dia_cobro, v.monto_abonado, v.cuotas, v.monto_cuota, v.total_venta, v.estado, v.saldo_pendiente FROM ventas v 
		INNER JOIN clientes c ON c.id_cliente = v.id_cliente
		INNER JOIN usuarios u ON u.id_usuario = v.id_cobrador";
		return ejecutarConsulta($sql);
	}

	public function listar_por_cliente($idCliente){
		$sql="SELECT v.id_venta, v.nro_comprobante, v.fecha_venta, c.apellido_nombre as apellidoNombre, c.dni as dni, u.apellido_nombre as cobrador, v.dia_cobro, v.monto_abonado, v.cuotas, v.monto_cuota, v.total_venta, v.estado, v.saldo_pendiente FROM ventas v 
		INNER JOIN clientes c ON c.id_cliente = v.id_cliente
		INNER JOIN usuarios u ON u.id_usuario = v.id_cobrador
		WHERE v.id_cliente='$idCliente'";
		return ejecutarConsulta($sql);
	}

	public function listar_por_fecha($fechaIni,$fechaFin){
		$sql="SELECT v.id_venta, v.nro_comprobante, v.fecha_venta, c.apellido_nombre as apellidoNombre, c.dni as dni, u.apellido_nombre as cobrador, v.dia_cobro, v.monto_abonado, v.cuotas, v.monto_cuota, v.total_venta, v.estado, v.saldo_pendiente FROM ventas v 
		INNER JOIN clientes c ON c.id_cliente = v.id_cliente
		INNER JOIN usuarios u ON u.id_usuario = v.id_cobrador
		WHERE v.fecha_venta BETWEEN '$fechaIni' AND '$fechaFin'";
		return ejecutarConsulta($sql);
	}

	public function buscar_id($idVenta){
        $sql="SELECT v.id_venta, v.nro_comprobante, CONCAT(LPAD(DAY(v.fecha_venta),2,'0'),'/', LPAD(MONTH(v.fecha_venta),2,'0'), '/', YEAR(v.fecha_venta)) as fecha_venta, v.id_cliente, c.apellido_nombre as apellidoNombre, c.dni as dni, c.domicilio as domicilio, c.telefono as telefono, v.id_cobrador, v.dia_cobro, v.monto_abonado, v.cuotas, v.monto_cuota, v.total_venta, v.estado, v.saldo_pendiente, u.apellido_nombre as ultimaModNombre, v.fecha_hora_ultima_mod FROM ventas v 
        INNER JOIN clientes c ON c.id_cliente=v.id_cliente 
        INNER JOIN usuarios u ON u.id_usuario=v.id_ultima_mod
        WHERE v.id_venta ='$idVenta'";
        return ejecutarConsultaSimpleFila($sql);
    }

	public function generar_nro_comprobante(){
		$sql="SELECT nro_comprobante+1 as nro_comprobante FROM ventas ORDER BY nro_comprobante DESC LIMIT 1";
		return ejecutarConsultaSimpleFila($sql);
	}

	public function listar_detalles($idVenta){
        $sql="SELECT dv.descripcion, dv.precio_venta, dv.cantidad, dv.subtotal FROM detalles_ventas dv WHERE dv.id_venta='$idVenta'";
        return ejecutarConsulta($sql);
    }

    public function verificar_cuota_tiene_pago($idVenta){
    	$sql="SELECT * FROM cuotas WHERE id_venta='$idVenta' AND (estado='PAGADA' OR estado='EN PROCESO')";
    	return ejecutarConsulta($sql);
    }

    public function saldo_pendiente_total($idCliente){
        $sql="SELECT SUM(saldo_pendiente) as saldoPendienteTotal FROM ventas WHERE id_cliente='$idCliente'";
        return ejecutarConsultaSimpleFila($sql);
    }

    public function saldo_pendiente_venta($idVenta){
    	$sql="SELECT saldo_pendiente as saldoPendienteVenta FROM ventas WHERE id_venta='$idVenta'";
    	return ejecutarConsultaSimpleFila($sql);
    }

    public function cant_ventas_dashboard(){
		$sql="SELECT COUNT(id_venta) as cant FROM ventas";
		return ejecutarConsultaSimpleFila($sql);
	}
}

?>
