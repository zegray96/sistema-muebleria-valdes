<?php
require ('../config/Conexion.php');

Class Cliente{
	public function __construct(){

	}

	public function listar(){
		$sql="SELECT * FROM clientes";
		return ejecutarConsulta($sql);
	}

	public function nuevo($apellidoNombre,$dni,$barrio,$domicilio,$telefono){
		$sql="INSERT INTO clientes (apellido_nombre, dni, barrio, domicilio, telefono) VALUES ('$apellidoNombre', '$dni', '$barrio', '$domicilio', '$telefono')";
		return ejecutarConsulta($sql);
	}

	public function editar($idCliente,$apellidoNombre,$dni,$barrio,$domicilio,$telefono){
		$sql="UPDATE clientes SET apellido_nombre = '$apellidoNombre', dni = '$dni', barrio = '$barrio', domicilio='$domicilio', telefono = '$telefono' WHERE id_cliente = '$idCliente'";
		return ejecutarConsulta($sql);
	}


	public function buscar_id($idCliente){
		$sql="SELECT * FROM clientes WHERE id_cliente='$idCliente'";
		return ejecutarConsultaSimpleFila($sql);
	}

	public function verificar_existencia_dni($dni){
		$sql="SELECT * FROM clientes WHERE dni='$dni'";
		return ejecutarConsultaSimpleFila($sql);
	}

	public function cant_clientes_dashboard(){
		$sql="SELECT COUNT(id_cliente) as cant FROM clientes";
		return ejecutarConsultaSimpleFila($sql);
	}

	
}

?>