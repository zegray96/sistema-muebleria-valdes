<?php
require ('../config/Conexion.php');

Class Pedido{
	public function __construct(){

	}

	public function nuevo($fechaEntrega,$descripcion,$apellidoNombre,$domicilio,$telefono,$estado,$idUltimaMod){
		date_default_timezone_set('America/Argentina/Buenos_Aires');
		$fechaHoraUltimaMod = date("Y-m-d H:i:s");

		$sql="INSERT INTO pedidos (fecha_entrega, descripcion, apellido_nombre, domicilio, telefono, estado, id_ultima_mod, fecha_hora_ultima_mod) VALUES ('$fechaEntrega','$descripcion','$apellidoNombre','$domicilio','$telefono','$estado','$idUltimaMod','$fechaHoraUltimaMod')";
		return ejecutarConsulta($sql);
	}

	public function editar($idPedido,$fechaEntrega,$descripcion,$apellidoNombre,$domicilio,$telefono,$estado,$idUltimaMod){
		date_default_timezone_set('America/Argentina/Buenos_Aires');
		$fechaHoraUltimaMod = date("Y-m-d H:i:s");
		
		$sql="UPDATE pedidos SET fecha_entrega='$fechaEntrega', descripcion='$descripcion', apellido_nombre='$apellidoNombre', domicilio='$domicilio', telefono='$telefono', estado='$estado', id_ultima_mod='$idUltimaMod', fecha_hora_ultima_mod='$fechaHoraUltimaMod' WHERE id_pedido='$idPedido'";
		return ejecutarConsulta($sql);
	}

	public function buscar_id($idPedido){
		$sql="SELECT p.id_pedido, CONCAT(LPAD(DAY(p.fecha_entrega),2,'0'),'/', LPAD(MONTH(p.fecha_entrega),2,'0'), '/', YEAR(p.fecha_entrega)) as fecha_entrega, p.descripcion, p.apellido_nombre, p.domicilio, p.telefono, p.estado, u.apellido_nombre as ultimaModNombre, p.fecha_hora_ultima_mod FROM pedidos p
		INNER JOIN usuarios u ON u.id_usuario=p.id_ultima_mod 
		WHERE p.id_pedido='$idPedido'";
		return ejecutarConsultaSimpleFila($sql);
	}

	public function listar_todos(){
		$sql="SELECT * FROM pedidos";
		return ejecutarConsulta($sql);
	}

	public function listar_por_fecha($fechaIni,$fechaFin){
		$sql="SELECT * FROM pedidos WHERE fecha_entrega BETWEEN '$fechaIni' AND '$fechaFin'";
		return ejecutarConsulta($sql);
	}

	public function listar_pedidos_para_hoy(){
		date_default_timezone_set('America/Argentina/Buenos_Aires'); 
		$hoy = date("Y-m-d");
		$sql="SELECT * FROM pedidos WHERE fecha_entrega='$hoy'";
		return ejecutarConsulta($sql);
	}

	public function cant_pedidos_para_hoy_dashboard(){
		date_default_timezone_set('America/Argentina/Buenos_Aires');
		$hoy = date("Y-m-d");
		$sql="SELECT COUNT(id_pedido) as cant FROM pedidos WHERE fecha_entrega='$hoy'";
		return ejecutarConsultaSimpleFila($sql);
	}

	
}

?>