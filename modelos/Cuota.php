<?php
require ('../config/Conexion.php');

Class Cuota{
	public function __construct(){

	}

	public function actualizar_cuotas_vencidas(){
		date_default_timezone_set('America/Argentina/Buenos_Aires');
		$hoy = date("Y-m-d");
		$sql="UPDATE cuotas SET estado='VENCIDA' WHERE estado='PENDIENTE' AND '$hoy'>fecha_vencimiento";
		return ejecutarConsulta($sql);
	}

	public function editar($idVenta,$nroCuota,$fechaVencimiento,$observacion,$montoAdicional,$montoAdicionalBd,$idUltimaMod){
		date_default_timezone_set('America/Argentina/Buenos_Aires');
		$fechaHoraUltimaMod = date("Y-m-d H:i:s");

		// restamos del saldo el monto adicional que ya teniamos
		$sql="UPDATE ventas SET saldo_pendiente=saldo_pendiente-'$montoAdicionalBd' WHERE id_venta='$idVenta'";
		ejecutarConsulta($sql);

		// actualizamos datos de cuota
		$sql="UPDATE cuotas SET fecha_vencimiento='$fechaVencimiento', observacion='$observacion', monto_adicional='$montoAdicional' ,id_ultima_mod='$idUltimaMod', fecha_hora_ultima_mod='$fechaHoraUltimaMod' WHERE id_venta='$idVenta' AND nro_cuota='$nroCuota'";
		ejecutarConsulta($sql);

		// sumamos el monto adicional nuevo al saldo
		$sql="UPDATE ventas SET saldo_pendiente=saldo_pendiente+'$montoAdicional' WHERE id_venta='$idVenta'";
		return ejecutarConsulta($sql);
	}

	public function verificacion_cuota_paga($idVenta,$nroCuota){
    	$sql="SELECT * FROM cuotas WHERE id_venta='$idVenta' AND nro_cuota='$nroCuota' AND estado='PAGADA'";
    	return ejecutarConsulta($sql);
    }

    public function verificacion_cuota_anterior($idVenta,$nroCuota){
	 	$nroCuota--;
    	$sql="SELECT * FROM cuotas WHERE id_venta='$idVenta' AND nro_cuota='$nroCuota' AND estado='PAGADA'";
    	return ejecutarConsulta($sql);
    }

	public function buscar_id($idVenta,$nroCuota){
		$sql="SELECT c.id_venta, v.nro_comprobante as nroComprobanteVentaAsociado, CONCAT(LPAD(DAY(c.fecha_vencimiento),2,'0'),'/', LPAD(MONTH(c.fecha_vencimiento),2,'0'), '/', YEAR(v.fecha_venta)) as fecha_vencimiento, cl.apellido_nombre as apellidoNombre, cl.dni as dni, c.nro_cuota, v.monto_cuota as montoCuota, c.estado, u.apellido_nombre as ultimaModNombre, c.fecha_hora_ultima_mod, c.observacion, c.monto_adicional FROM cuotas c
		INNER JOIN ventas v ON v.id_venta=c.id_venta
		INNER JOIN clientes cl ON cl.id_cliente=v.id_cliente
		LEFT JOIN pagos p ON p.id_venta=c.id_venta 
		LEFT JOIN usuarios u ON u.id_usuario=c.id_ultima_mod  
		WHERE c.id_venta='$idVenta' AND c.nro_cuota='$nroCuota'";
		return ejecutarConsultaSimpleFila($sql);
	}

	public function listar_todas(){
		$sql="SELECT v.nro_comprobante as nroComprobanteVentaAsociado, c.id_venta, cl.id_cliente, cl.apellido_nombre as apellidoNombre, cl.domicilio as domicilio, cl.barrio as barrio, u.apellido_nombre as cobrador, c.fecha_vencimiento, c.nro_cuota, c.estado FROM cuotas c 
		INNER JOIN ventas v ON v.id_venta=c.id_venta
		INNER JOIN clientes cl ON cl.id_cliente=v.id_cliente
		INNER JOIN usuarios u ON u.id_usuario=v.id_cobrador";
		return ejecutarConsulta($sql);
	}

	public function listar_vencimientos_hoy(){
		date_default_timezone_set('America/Argentina/Buenos_Aires'); 
		$hoy = date("Y-m-d");
		$sql="SELECT v.nro_comprobante as nroComprobanteVentaAsociado, c.id_venta, cl.id_cliente, cl.apellido_nombre as apellidoNombre, cl.domicilio as domicilio, cl.barrio as barrio, u.apellido_nombre as cobrador, c.fecha_vencimiento, c.nro_cuota, c.estado FROM cuotas c 
		INNER JOIN ventas v ON v.id_venta=c.id_venta
		INNER JOIN clientes cl ON cl.id_cliente=v.id_cliente
		INNER JOIN usuarios u ON u.id_usuario=v.id_cobrador
		WHERE c.fecha_vencimiento='$hoy'";
		return ejecutarConsulta($sql);
	}

	public function listar_por_cliente($idCliente){
		$sql="SELECT v.nro_comprobante as nroComprobanteVentaAsociado, c.id_venta, cl.id_cliente, cl.apellido_nombre as apellidoNombre, cl.domicilio as domicilio, cl.barrio as barrio, u.apellido_nombre as cobrador, c.fecha_vencimiento, c.nro_cuota, c.estado FROM cuotas c 
		INNER JOIN ventas v ON v.id_venta=c.id_venta
		INNER JOIN clientes cl ON cl.id_cliente=v.id_cliente
		INNER JOIN usuarios u ON u.id_usuario=v.id_cobrador
		WHERE v.id_cliente='$idCliente'";
		return ejecutarConsulta($sql);
	}

	public function listar_por_fecha_vencimiento($fechaIni,$fechaFin){
		$sql="SELECT v.nro_comprobante as nroComprobanteVentaAsociado, c.id_venta, cl.id_cliente, cl.apellido_nombre as apellidoNombre, cl.domicilio as domicilio, cl.barrio as barrio, u.apellido_nombre as cobrador, c.fecha_vencimiento, c.nro_cuota, c.estado FROM cuotas c 
		INNER JOIN ventas v ON v.id_venta=c.id_venta 
		INNER JOIN clientes cl ON cl.id_cliente=v.id_cliente 
		INNER JOIN usuarios u ON u.id_usuario=v.id_cobrador 
		WHERE c.fecha_vencimiento BETWEEN '$fechaIni' AND '$fechaFin'";
		return ejecutarConsulta($sql);
	}

	

	public function listar_pagos($idVenta,$nroCuota){
		$sql="SELECT * FROM pagos WHERE id_venta='$idVenta' AND nro_cuota='$nroCuota'";
		return ejecutarConsulta($sql);
	}

	public function cant_cuotas_venc_hoy_dashboard(){
		date_default_timezone_set('America/Argentina/Buenos_Aires');
		$hoy = date("Y-m-d");
		$sql="SELECT COUNT(id_venta) as cant FROM cuotas WHERE fecha_vencimiento='$hoy'";
		return ejecutarConsultaSimpleFila($sql);
	}

}

?>