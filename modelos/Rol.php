<?php
require ('../config/Conexion.php');
Class Rol{
	public function __construct(){

	}

	
	public function nuevo($nombre,$vAcceso,$vClientes,$vVentas,$vCuotas,$vPagos,$vPedidos,$vReclamos,$altAcceso,$altClientes,$altVentas,$altPagos,$altCuotas,$altConfiguracion,$altPedidos,$altReclamos,$newAcceso,$newClientes,$newVentas,$newPagos,$newPedidos,$newReclamos){
		$sql="INSERT INTO roles (nombre, v_acceso, v_clientes, v_ventas, v_cuotas, v_pagos, v_pedidos, v_reclamos, alt_acceso, alt_clientes, alt_ventas, alt_pagos, alt_cuotas, alt_configuracion, alt_pedidos, alt_reclamos, new_acceso, new_clientes, new_ventas, new_pagos, new_pedidos, new_reclamos) VALUES ('$nombre','$vAcceso','$vClientes','$vVentas','$vCuotas','$vPagos','$vPedidos','$vReclamos','$altAcceso','$altClientes','$altVentas','$altPagos','$altCuotas','$altConfiguracion','$altPedidos','$altReclamos','$newAcceso','$newClientes','$newVentas','$newPagos','$newPedidos','$newReclamos')";
		return ejecutarConsulta($sql);
	}

	public function editar($idRol,$nombre,$vAcceso,$vClientes,$vVentas,$vCuotas,$vPagos,$vPedidos,$vReclamos,$altAcceso,$altClientes,$altVentas,$altPagos,$altCuotas,$altConfiguracion,$altPedidos,$altReclamos,$newAcceso,$newClientes,$newVentas,$newPagos,$newPedidos,$newReclamos){
		$sql="UPDATE roles SET nombre = '$nombre', v_acceso = '$vAcceso', v_clientes = '$vClientes', v_ventas='$vVentas', v_cuotas = '$vCuotas', v_pagos = '$vPagos', v_pedidos='$vPedidos', v_reclamos='$vReclamos', alt_acceso='$altAcceso', alt_clientes='$altClientes', alt_ventas='$altVentas', alt_pagos='$altPagos', alt_cuotas='$altCuotas', alt_configuracion='$altConfiguracion', alt_pedidos='$altPedidos', alt_reclamos='$altReclamos', new_acceso='$newAcceso', new_clientes='$newClientes', new_ventas='$newVentas', new_pagos='$newPagos', new_pedidos='$newPedidos', new_reclamos='$newReclamos' WHERE id_rol = '$idRol'";
		return ejecutarConsulta($sql);
	}

	public function listar(){
		$sql="SELECT * FROM roles";
		return ejecutarConsulta($sql);
	}

	public function buscar_id($idRol){
		$sql="SELECT * FROM roles WHERE id_rol='$idRol'";
		return ejecutarConsultaSimpleFila($sql);
	}

	public function verificar_existencia_nombre($nombre){
		$sql="SELECT * FROM roles WHERE nombre='$nombre'";
		return ejecutarConsultaSimpleFila($sql);
	}
}
?>