<?php
require ('../config/Conexion.php');

Class Pago{
	public function __construct(){

	}

	public function nuevo($idVenta,$nroCuota,$saldoPendienteBd,$montoPagado,$totalPagadoBd,$montoCuotaConMontoAdicional,$idRegistradoPor){
		date_default_timezone_set('America/Argentina/Buenos_Aires');
		$fechaHoraPago = date("Y-m-d H:i:s");

		$saldoPendiente=$saldoPendienteBd-$montoPagado;
		$totalPagado=$totalPagadoBd+$montoPagado;

		$respuesta=self::generar_nro_comprobante();
		if ($respuesta==null) {
			$nroComprobante=1;
		}else{
			$nroComprobante=$respuesta['nro_comprobante'];
		}

		$sql="INSERT INTO pagos (nro_comprobante,monto_pagado,fecha_hora,id_registrado_por,id_venta,nro_cuota) VALUES ('$nroComprobante','$montoPagado','$fechaHoraPago','$idRegistradoPor','$idVenta','$nroCuota')";
		$idPago=ejecutarConsulta_retornarID($sql);

		if ($totalPagado==$montoCuotaConMontoAdicional) {
			$sql="UPDATE cuotas SET estado='PAGADA' WHERE id_venta='$idVenta' AND nro_cuota='$nroCuota'";
			ejecutarConsulta($sql);
		}else{
			$sql="UPDATE cuotas SET estado='EN PROCESO' WHERE id_venta='$idVenta' AND nro_cuota='$nroCuota'";
			ejecutarConsulta($sql);
		}

		

		if($saldoPendiente<=0){
			$saldoPendiente=0;
			$sql="UPDATE ventas SET saldo_pendiente='$saldoPendiente', estado='PAGADA' WHERE id_venta='$idVenta'";
			return ejecutarConsulta($sql).":".$idPago;
		}else{
			$sql="UPDATE ventas SET saldo_pendiente='$saldoPendiente' WHERE id_venta='$idVenta'";
			return ejecutarConsulta($sql).":".$idPago;
		}
	}

	public function buscar_id($idPago){
		$sql="SELECT p.id_pago, p.nro_comprobante, p.monto_pagado, CONCAT(LPAD(DAY(p.fecha_hora),2,'0'),'/', LPAD(MONTH(p.fecha_hora),2,'0'), '/', YEAR(p.fecha_hora)) as fechaPago, p.fecha_hora, u.apellido_nombre as registradoPor, p.id_venta, p.nro_cuota 
		FROM pagos p
		INNER JOIN usuarios u ON u.id_usuario=p.id_registrado_por 
		WHERE id_pago='$idPago'";
		return ejecutarConsultaSimpleFila($sql);
	}

	public function buscar_id_para_comprobante($idPago){
		$sql="SELECT p.id_pago, p.nro_comprobante, p.monto_pagado, CONCAT(LPAD(DAY(p.fecha_hora),2,'0'),'/', LPAD(MONTH(p.fecha_hora),2,'0'), '/', YEAR(p.fecha_hora)) as fechaPago, p.fecha_hora, p.id_venta, p.nro_cuota, v.nro_comprobante as nroComprobanteVenta, v.monto_cuota as montoCuota, c.apellido_nombre as apellidoNombre, c.id_cliente as idCliente 
		FROM pagos p
		INNER JOIN ventas v ON v.id_venta=p.id_venta 
		INNER JOIN clientes c ON c.id_cliente=v.id_cliente
		WHERE id_pago='$idPago'";
		return ejecutarConsultaSimpleFila($sql);
	}

	public function listar_pagos($idVenta,$nroCuota){
		$sql="SELECT * FROM pagos WHERE id_venta='$idVenta' AND nro_cuota='$nroCuota'";
		return ejecutarConsulta($sql);
	}

	public function listar(){
		$sql="SELECT p.id_pago, p.nro_comprobante, c.apellido_nombre as apellidoNombre, c.domicilio as domicilio, p.monto_pagado FROM pagos p
		INNER JOIN ventas v ON v.id_venta=p.id_venta
		INNER JOIN clientes c ON c.id_cliente=v.id_cliente";
		return ejecutarConsulta($sql);
	}

	public function listar_pagos_hoy(){
		date_default_timezone_set('America/Argentina/Buenos_Aires'); 
		$hoy = date("Y-m-d");
		$sql="SELECT p.id_pago, p.nro_comprobante, c.apellido_nombre as apellidoNombre, c.domicilio as domicilio, p.monto_pagado, u.apellido_nombre as registradoPor FROM pagos p
		INNER JOIN ventas v ON v.id_venta=p.id_venta
		INNER JOIN clientes c ON c.id_cliente=v.id_cliente
		INNER JOIN usuarios u ON u.id_usuario=p.id_registrado_por
		WHERE DATE(p.fecha_hora)='$hoy'";
		return ejecutarConsulta($sql);
	}

	public function listar_por_fecha($fechaIni,$fechaFin){
		$sql="SELECT p.id_pago, p.nro_comprobante, c.apellido_nombre as apellidoNombre, c.domicilio as domicilio, p.monto_pagado, u.apellido_nombre as registradoPor FROM pagos p
		INNER JOIN ventas v ON v.id_venta=p.id_venta
		INNER JOIN clientes c ON c.id_cliente=v.id_cliente
		INNER JOIN usuarios u ON u.id_usuario=p.id_registrado_por
		WHERE DATE(p.fecha_hora) BETWEEN '$fechaIni' AND '$fechaFin'";
		return ejecutarConsulta($sql);
	}

	public function total_pagado($idVenta,$nroCuota){
		$sql="SELECT SUM(monto_pagado) as totalPagado FROM pagos WHERE id_venta='$idVenta' AND nro_cuota='$nroCuota'";
		return ejecutarConsultaSimpleFila($sql);
	}

	public function eliminar($idPago,$idVenta,$nroCuota,$saldoPendienteBd,$montoPagadoBd,$totalPagadoBd){
		$saldoPendiente=$saldoPendienteBd+$montoPagadoBd;
		$totalPagado=$totalPagadoBd-$montoPagadoBd;

		$sql="UPDATE ventas SET saldo_pendiente='$saldoPendiente', estado='PENDIENTE' WHERE id_venta='$idVenta'";
		ejecutarConsulta($sql);

		if ($totalPagado==0) {
			$sql="UPDATE cuotas SET estado='PENDIENTE' WHERE id_venta='$idVenta' AND nro_cuota='$nroCuota'";
			ejecutarConsulta($sql);
		}else{
			$sql="UPDATE cuotas SET estado='EN PROCESO' WHERE id_venta='$idVenta' AND nro_cuota='$nroCuota'";
			ejecutarConsulta($sql);
		}

		$sql="DELETE FROM pagos WHERE id_pago='$idPago'";
		return ejecutarConsulta($sql);
	}

	public function generar_nro_comprobante(){
		$sql="SELECT nro_comprobante+1 as nro_comprobante FROM pagos ORDER BY nro_comprobante DESC LIMIT 1";
		return ejecutarConsultaSimpleFila($sql);
	}

	public function saldo_pendiente_total($idCliente){
        $sql="SELECT SUM(saldo_pendiente) as saldoPendienteTotal FROM ventas WHERE id_cliente='$idCliente'";
        return ejecutarConsultaSimpleFila($sql);
    }

    public function cant_pagos_hoy_dashboard(){
    	date_default_timezone_set('America/Argentina/Buenos_Aires'); 
		$hoy = date("Y-m-d");
		$sql="SELECT COUNT(id_pago) as cant FROM pagos
		WHERE DATE(fecha_hora)='$hoy'";
		return ejecutarConsultaSimpleFila($sql);
    }

}

?>