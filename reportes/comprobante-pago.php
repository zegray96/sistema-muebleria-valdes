<?php
//activamos el almacenmiento de la sesion
ob_start();
session_start();

if(!isset($_SESSION['idUsuarioSisCob'])){
  header('Location: ../vistas/login');
}else{

require('../assets/TCPDF/tcpdf.php');
require('../modelos/Pago.php');
require('../modelos/Venta.php');
require('../modelos/Configuracion.php');

// Extend the TCPDF class to create custom Header and Footer
class MYPDF extends TCPDF {

	//Page header
	public function Header() {

		$p = new Pago();
		$respuesta = $p->buscar_id_para_comprobante($_GET['id']);	
		$apellidoNombre = mb_strtoupper($respuesta['apellidoNombre'],'UTF-8');
		$fechaPago = $respuesta['fechaPago'];
		$nroComprobantePago = str_pad($respuesta['nro_comprobante'], 8 ,"0", STR_PAD_LEFT);
		$fechaHora = $respuesta['fecha_hora'];
		$horaPago = explode(" ", $fechaHora);

		$conf = new Configuracion();
		$respuesta=$conf->traer_configuracion();
		$nombreEmpresa = mb_strtoupper($respuesta['nombre_empresa'],'UTF-8');
		$telefonoEmpresa = $respuesta['telefono'];

	
		$this->SetFont('helvetica','B',28);    //Letra Arial, negrita (Bold), tam. 20
		$this->setY(8);
		$this->setX(10);
	    $this->MultiCell(190,5,$nombreEmpresa,0,'C',false);
	    $this->Ln(5);
	    $this->setX(55);
	   	$this->Cell(100, 5, 'TEL: '.$telefonoEmpresa,0,0,'C');
	   	$this->Ln(7);
	    $this->setX(10);
		$this->Cell(192, 5, '----------------------------------------------------------',0,0,'L');

		$this->Ln(10);
		$this->setX(55);
		$this->Cell(100, 5, 'COMPROBANTE DE PAGO',0,0,'C');

		$this->Ln(12);
		$this->setX(55);
	    $this->Cell(100, 5, 'X',0,0,'C');

	    $this->Ln(14);
	    $this->setX(55);
	    $this->SetFont('helvetica','B',23);
	    $this->Cell(100, 5, '(DOCUMENTO NO VÁLIDO COMO FACTURA)',0,0,'C');

	    
	    $this->SetFont('helvetica','B',23); 
		$this->Ln(24);
		$this->setX(10);
		$this->Cell(11, 5, 'N°',0,0,'L');
		$this->Cell(90);
		$this->Cell(10, 5, 'FECHA',0,0,'C');
		$this->Cell(50);
		$this->Cell(10, 5, 'HORA',0,0,'C');

		$this->Ln(10);
		$this->setX(10);
		$this->Cell(11, 5, $nroComprobantePago,0,0,'L');
		$this->Cell(90);
		$this->Cell(10, 5, $fechaPago,0,0,'C');
		$this->Cell(60);
		$this->Cell(10, 5, $horaPago[1],0,0,'R');

		$this->SetFont('helvetica','B',23);  
		$this->Ln(20);
		$this->setX(10);
		$this->Cell(10, 5, 'CLIENTE:',0,0,'L');
		$this->Ln(14);
		$this->setX(10);
		$this->MultiCell(190,5,$apellidoNombre,0,'L',false);

		// Espacio para que vaya el contenido de cliente - detalles - totales
		$this->SetTopMargin($this->y);


	}


	// Page footer
	public function Footer() {

		$p = new Pago();
		$v = new Venta();
		$respuesta = $p->buscar_id_para_comprobante($_GET['id']);
		$montoPagado= number_format($respuesta['monto_pagado'], 2);
		$idCliente= number_format($respuesta['idCliente']);
		$idVenta= number_format($respuesta['id_venta']);

		$respuesta = $p->saldo_pendiente_total($idCliente);
		$saldoPendienteTotal= number_format($respuesta['saldoPendienteTotal'], 2);

		$respuesta=$v->saldo_pendiente_venta($idVenta);
		$saldoPendienteVenta=number_format($respuesta['saldoPendienteVenta'], 2);


		// Posición: a 1,5 cm del final
	    $this->SetY(-60);
	    $this->SetFont('helvetica','B',25);    //Letra Arial, negrita (Bold), tam. 20
		

		$this->Ln(10);
		$this->setX(10);
		$this->Cell(80, 5, 'MONTO PAGADO: ',0,0,'L');
		$this->Cell(5);
		$this->Cell(10, 5, $montoPagado,0,0,'L');

		$this->Ln(15);
		$this->setX(10);
		$this->Cell(135, 5, 'SALDO DE VENTA ADEUDADO: ',0,0,'L');
		$this->Cell(5);
		$this->Cell(10, 5, $saldoPendienteVenta,0,0,'L');

		$this->Ln(15);
		$this->setX(10);
		$this->Cell(120, 5, 'SALDO TOTAL ADEUDADO: ',0,0,'L');
		$this->Cell(5);
		$this->Cell(10, 5, $saldoPendienteTotal,0,0,'L');
	}
}

// create new PDF document
$pdf = new MYPDF(PDF_PAGE_ORIENTATION, PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);

$p = new Pago();

$respuesta = $p->buscar_id_para_comprobante($_GET['id']);
$nroComprobanteVenta = str_pad($respuesta['nroComprobanteVenta'], 8 ,"0", STR_PAD_LEFT);
$nroComprobantePago = $respuesta['nro_comprobante'];
$nroCuota = $respuesta['nro_cuota'];
$montoCuota= number_format($respuesta['montoCuota'], 2);

$pdf->AddPage('P', 'A4');
$pdf->SetTitle('Comprobante de Pago');

$pdf->SetFont('helvetica','B',25);    

$pdf->Ln(10);
$pdf->setX(10);
$pdf->Cell(10, 5, '-----------------------------------------------------------------',0,0,'L');
$pdf->Ln(8);
$pdf->setX(10);
$pdf->Cell(10, 5, 'VENTA ASOC.',0,0,'L');
$pdf->Cell(70);
$pdf->Cell(10,5,'N° CUO.',0,0,'L');
$pdf->Cell(35);
$pdf->Cell(10,5,'MONTO CUOTA',0,0,'L');
$pdf->Ln(6);
$pdf->setX(10);
$pdf->Cell(10, 5, '-----------------------------------------------------------------',0,0,'L');

$pdf->SetFont('helvetica','B',25); 
$pdf->Ln(10);
$pdf->setX(10);
$pdf->Cell(10, 5, $nroComprobanteVenta,0,0,'L');
$pdf->Cell(80);
$pdf->Cell(10,5,$nroCuota,0,0,'C');
$pdf->Cell(40);
$pdf->Cell(18,5,$montoCuota,0,0,'C');
 


$pdf->Output('comprobante-pago-'.$nroComprobantePago.'.pdf', 'I');

}
//Fin llave
ob_end_flush(); //libera el espacio del buffer

?>