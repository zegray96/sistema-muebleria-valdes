<?php
//activamos el almacenmiento de la sesion
ob_start();
session_start();

if(!isset($_SESSION['idUsuarioSisCob'])){
  header('Location: ../vistas/login');
}else{

require('../assets/TCPDF/tcpdf.php');
require('../modelos/Venta.php');
require('../modelos/Configuracion.php');

// Extend the TCPDF class to create custom Header and Footer
class MYPDF extends TCPDF {
	protected $ultimaPagina = false;
	//Page header
	public function Header() {

		$v = new Venta();
		$respuesta = $v->buscar_id($_GET['id']);
		$nroComprobanteVenta = str_pad($respuesta['nro_comprobante'], 8 ,"0", STR_PAD_LEFT);
		$fechaVenta = $respuesta["fecha_venta"];
		$apellidoNombre = mb_strtoupper($respuesta['apellidoNombre'],'UTF-8');
		$dni = $respuesta['dni'];
		$domicilio = mb_strtoupper($respuesta['domicilio'],'UTF-8');
		$telefono = mb_strtoupper($respuesta['telefono'],'UTF-8');

		$conf = new Configuracion();
		$respuesta=$conf->traer_configuracion();
		$nombreEmpresa = mb_strtoupper($respuesta['nombre_empresa'],'UTF-8');
		$descripcionCortaEmpresa = mb_strtoupper($respuesta['descripcion_corta_empresa'],'UTF-8');
		$telefonoEmpresa = mb_strtoupper($respuesta['telefono'],'UTF-8');
		$logoEmpresa = '../assets/img/logo-empresa.png';

		$this->setY(8);
		// Logo
		$this->Image($logoEmpresa, 83, 10, 45, '', '', '', 'T', false, 300, '', false, false, 0, false, false, false);
		$this->SetFont('helvetica','B',30);    //Letra Arial, negrita (Bold), tam. 20
		$this->Ln(48);
		$this->setX(10);
	    $this->MultiCell(190,5,$nombreEmpresa,0,'C',false);
	    $this->Ln(5);
	    $this->SetFont('helvetica','',23);
	   	$this->MultiCell(190,5,$descripcionCortaEmpresa,0,'C',false);
	    $this->Ln(5);
	    $this->setX(55);
	   	$this->Cell(100, 5, 'TEL: '.$telefonoEmpresa,0,0,'C');
	   	$this->Ln(7);
	    $this->setX(10);
		$this->Cell(192, 5, '----------------------------------------------------------------------',0,0,'L');

		$this->setX(10);
		$this->Ln(10);
		$this->SetFont('helvetica','B',25);
	    // Nro de comprobante 
	    $this->Cell(100, 10, 'N°: '.'0001'.' - '.$nroComprobanteVenta,0,0,'L');
	    $this->Cell(12);
	    $this->Cell(75, 5, 'FECHA: '.$fechaVenta,0,0,'C');

		$this->Ln(20);
		$this->setX(10);
		$this->SetFont('helvetica','',25);
		$this->Cell(40, 5, 'CLIENTE:',0,0,'L');
		$this->Cell(5);
		$this->SetFont('helvetica','B',25);
		$this->MultiCell(150,5,$apellidoNombre,0,'L',false);

		$this->Ln(5);
		$this->setX(10);
		$this->SetFont('helvetica','',25);
		$this->Cell(50, 5, 'DOMICILIO:',0,0,'L');
		$this->Cell(5);
		$this->SetFont('helvetica','B',25);
		$this->MultiCell(140,5,$domicilio,0,'L',false);

		// Creamos las celdas para los titulos de cada columna y le asignamos un fondo gris y el tipo de letra
		$this->Ln(10);
		$this->SetFont('helvetica','',23);
		$this->Cell(192, 5, '----------------------------------------------------------------------',0,0,'L');
		$this->Ln(7);
		$this->SetFont('helvetica','B',18);	
		$this->Cell(25,6,'CANT.',0,0,'L');
		$this->Cell(85,6,'DESCRIPCIÓN',0,0,'L');
		$this->Cell(40,6,'P. V.',0,0,'C');
		$this->Cell(40,6,'SUBTOTAL',0,0,'C');
		$this->SetFont('helvetica','',23);
		$this->Ln(5);
		$this->Cell(192, 5, '----------------------------------------------------------------------',0,0,'L');

		// Espacio para que vaya el contenido de cliente - detalles - totales
		$this->SetTopMargin($this->y+6);

	}

	public function Close() {
		$this->ultimaPagina = true;
		parent::Close();
	}


	// Page footer
	public function Footer() {

		$v = new Venta();
		$respuesta = $v->buscar_id($_GET['id']);
		$cuotas = $respuesta['cuotas'];
		$montoCuota = "$".number_format($respuesta['monto_cuota'], 2);
		$totalVenta = "$".number_format($respuesta['total_venta'], 2);

		
		if ($this->ultimaPagina) {
	      	// Position at 15 mm from bottom
			$this->SetY(-50);

			$this->Cell(130);
			$this->SetFont('helvetica','B',25);
			$this->Cell(60, 6, 'CUOTAS: '.$cuotas, 0,0,'R');

			$this->Ln(12);
			$this->Cell(130);
			$this->Cell(60, 6, 'MONTO CUOTA: '.$montoCuota, 0,0,'R');

			$this->SetFont('helvetica','B',30);
			$this->Ln(14);
			$this->Cell(130);
			$this->Cell(60, 6, 'TOTAL: '.$totalVenta, 0,0,'R');

		} 
	}
}

// create new PDF document
$pdf = new MYPDF(PDF_PAGE_ORIENTATION, PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);

$v = new Venta();
$respuesta = $v->buscar_id($_GET['id']);
$nroComprobanteVenta = $respuesta['nro_comprobante'];

$pdf->AddPage('P', 'A4');
$pdf->SetTitle('Comprobante de Venta');



$pdf->SetFont('helvetica', 'B', 18);

// Comenzamos a crear las filas de los registros segun la consulta mysql
$respuesta = $v->listar_detalles($_GET['id']);

while ($reg=$respuesta->fetch_object()){

	$cantidad = $reg->cantidad;

	$descripcion= substr($reg->descripcion, 0, 25);
	$descripcion=mb_strtoupper($descripcion,'UTF-8');
	$precioVenta=number_format($reg->precio_venta, 2);
	$subtotal= number_format($reg->subtotal, 2);


	$pdf->Cell(25,10,$cantidad,0,0,'C');
	$pdf->Cell(85,10,$descripcion,0,0,'L');
	$pdf->Cell(40,10,$precioVenta,0,0,'C');
	$pdf->Cell(40, 10, $subtotal, 0,0,'C');

	$pdf->Ln(10);

}


$pdf->Output('comprobante-venta-'.$nroComprobanteVenta.'.pdf', 'I');

}
//Fin llave
ob_end_flush(); //libera el espacio del buffer

?>