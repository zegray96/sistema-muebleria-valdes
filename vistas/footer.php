      <!--Modal Cargando -->
      <div class="modal" id="cargandoModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" data-backdrop="static" data-keyboard="false"> 
        <div class="modal-dialog modal-sm">
          <div class="modal-content">
            <div class="modal-header">
              <div class="spinner-border cargando-modal"></div>
            </div>
          </div>
        </div> 
      </div>
      <!--Fin Modal Cargando -->

      <!-- Footer -->
      <footer class="sticky-footer bg-white">
        <div class="container my-auto">
          <div class="copyright text-center my-auto">
            <span>Version <?php echo $version?>
              
            </span>
          </div>
        </div>
      </footer>
      <!-- Footer -->
    </div>
    <!-- End Content wrapper -->
  </div>
  <!-- End wrapper -->

  <!-- Scroll to top -->
  <a class="scroll-to-top rounded" href="#page-top">
    <i class="fas fa-angle-up"></i>
  </a>

  <script src="../assets/js/jquery/jquery.min.js"></script>
  <script src="../assets/js/jquery-easing/jquery.easing.min.js"></script>
  <script src="../assets/js/bootstrap/bootstrap.bundle.min.js"></script>
  <script src="../assets/js/ruang-admin.min.js"></script>

  <!-- Datatables -->
  <script src="../assets/js/datatables/jquery.dataTables.min.js"></script>
  <script src="../assets/js/datatables/dataTables.bootstrap4.min.js"></script>
  <script src="../assets/js/datatables/buttons.print.min.js"></script>
  <script src="../assets/js/datatables/dataTables.buttons.min.js"></script>
  <script src="../assets/js/datatables/jszip.min.js"></script>
  <script src="../assets/js/datatables/pdfmake.min.js"></script>
  <script src="../assets/js/datatables/vfs_fonts.js"></script>
  <script src="../assets/js/datatables/buttons.html5.min.js"></script>

  <!-- Sweetalert2 -->
  <script src="../assets/js/sweetalert2/sweetalert2.all.min.js"></script>

  <!-- Maxlength -->
  <script src="../assets/js/maxlength/maxlength.js"></script>

  <!-- Bootstrap Datepicker -->
  <script src="../assets/js/bootstrap-datepicker/bootstrap-datepicker.min.js"></script>
  <script src="../assets/js/bootstrap-datepicker/bootstrap-datepicker.es.min.js"></script>

  <!-- Bootstrap select -->
  <script src="../assets/js/bootstrap-select/bootstrap-select.min.js"></script>
  <!-- Idioma bootstrap-select -->
  <script src="../assets/js/bootstrap-select/defaults-es_ES.js"></script>

  <script src="../vistas/scripts/sesion.js?ver=<?php echo $version?>"></script>
  
</body>

</html>