<?php
//activamos el almacenmiento de la sesion
ob_start();
session_start();

if(!isset($_SESSION['idUsuarioSisCob'])){
  header('Location: login');
}else{
  //Llave
  require ('header.php');
?>


	<!-- Container fluid -->
	<div class="container-fluid" id="container-wrapper">

		<?php
		if($_SESSION['v_ventas']==0){
			echo '<div class="d-sm-flex align-items-center justify-content-between mb-4">
					<h1 class="h3 mb-0 text-gray-800">Acceso denegado</h1>
				</div>';
		}else{
		// Contenido autorizado
		?>

		<div class="d-sm-flex align-items-center justify-content-between mb-4">
			<h1 class="h3 mb-0 text-gray-800"><i class="fas fa-shopping-cart"></i> Ventas</h1>
		</div>

		<div class="row mb-3">
			<div class="col-lg-12">
				<div class="card mb-4">
					<div id="contenedor-cabecera" class="card-header py-3 d-flex flex-row align-items-center justify-content-between">
						<p id="textFiltro" class="card bg-primary text-white font-weight-bold mb-3 p-1" style=""></p>

						

						<div class="input-width-50-100 float-left pr-lg-4 mb-3">
							<select id="listarPor" class="selectpicker form-control" title="<i class='fas fa-filter'></i> Listar Por">
								<option value="Fecha">Fecha</option>
								<option value="Cliente">Cliente</option>
								<option value="Todas">Todas</option>
							</select>
						</div>

						<div class="input-width-50-100 float-left pl-lg-4 mb-3">
							<select id="listarPorCobrador" class="selectpicker form-control" data-live-search="true">
								
							</select>
						</div>

						

						<?php
						if ($_SESSION['new_ventas']==1) {
							echo "<button id='btnNuevo' class='btn btn-success'><i class='fas fa-plus-circle'></i> Nuevo</button>";
						}
						?>
						
					</div>
					<!-- Tabla -->
					<div id="listado" class="table-responsive p-3">
						<table id="tblListado" class="table align-items-center table-hover table-bordered" style="width: 100%;">
							<thead class="thead-light">
								<th>&nbsp;Opciones&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</th>
								<th>&nbsp;N°&nbsp;Comprobante&nbsp;</th>
								<th>&nbsp;Estado&nbsp;</th>
								<th>&nbsp;Fecha&nbsp;Venta&nbsp;</th>
								<th>&nbsp;Apellido&nbsp;y&nbsp;Nombre&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</th>
								<th>&nbsp;Dni&nbsp;</th>
								<th>&nbsp;Cobrador&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</th>
								<th>&nbsp;Total&nbsp;</th>
								<th>&nbsp;Saldo&nbsp;Pendiente&nbsp;</th>		
							</thead>
						</table>
					</div>
					<!-- End tabla -->

					<!-- Formulario -->
					<div id="formulario">
						<div class="card-body">

							<div id="div-ultima-mod" class="card bg-primary text-white mb-2">
								<div class="card-body">
									<label class="font-weight-bold">Ultima Modificación: </label>
									<span id="ultimaModNombre"></span>
									<br>
									<label class="font-weight-bold">Fecha: </label>
									<span id="ultimaModFecha"></span>
									<br>
									<label class="font-weight-bold">Hora: </label>
									<span id="ultimaModHora"></span>
								</div>
							</div>

							<form id="form">

								<input type="hidden" id="idVenta" name="idVenta">
								

								<!-- Datos venta -->
								<div id="datos-venta" class="float-left">
									<h4 class="font-weight-bold">Datos Venta</h4>
									

									<div id="div-nroc" class="form-group float-left">
										<label>N° Comprobante</label>
										<input type="hidden" id="nroComprobante" name="nroComprobante">
										<input type="text" class="form-control" id="nroComprobanteString" name="nroComprobanteString" readonly>
									</div>

									<div id="div-fechav" class="form-group float-left" id="simple-date1">
					                    <label><span class="text-danger">(*)</span> Fecha Venta</label>
					                      <div class="input-group date">
					                        <div class="input-group-prepend">
					                          <span class="input-group-text"><i class="fas fa-calendar"></i></span>
					                        </div>
					                        <input type="text" class="form-control" id="fechaVenta" name="fechaVenta" required>
					                      </div>
									</div>

									
								</div>
								<!-- Fin datos venta -->

								<!-- Datos cliente -->
								<div id="datos-cliente" class="float-left mt-3 mt-lg-0">
									<h4 class="font-weight-bold">Datos Cliente</h4>
									<input type="hidden" id="idCliente" name="idCliente">
									<div id="div-dni" class="form-group float-left">
										<label><span class="text-danger">(*)</span> Dni</label>
										<div class="input-group">
										  <input type="text" class="form-control" id="dni" name="dni" readonly required>
										  <div class="input-group-append">
										    <button type="button" class="btn btn-primary" id="btnBuscarCliente"><i class="fas fa-search"></i></button>
										  </div>
										</div>
									</div>

									<div id="div-ayn" class="form-group float-left">
										<label>Apellido y Nombre</label>
										<input type="text" class="form-control" id="apellidoNombre" name="apellidoNombre" readonly>
									</div>

									<div id="div-direc" class="form-group float-left">
										<label>Dirección</label>
										<input type="text" class="form-control" id="domicilio" name="domicilio" readonly>
									</div>
								</div>
								<!-- Fin datos cliente  -->


								<!-- Datos articulo -->
								<div id="datos-articulo" class="mt-3 float-left">
									<h4 class="font-weight-bold">Datos Articulo</h4>
									<div id="div-descrip" class="form-group float-left">
										<label><span class="text-danger">(*)</span> Descripción</label>
										<input type="text" class="form-control" id="descripcion" name="descripcion" data-maxsize="70" style="text-transform: capitalize;">
									</div>

									<div id="div-preciov" class="form-group float-left">
										<label><span class="text-danger">(*)</span> Precio V.</label>
										<input type="text" class="form-control" id="precioVenta" name="precioVenta" placeholder="00.0">
									</div>

									<div id="div-cantart" class="form-group float-left">
										<label><span class="text-danger">(*)</span> Cantidad</label>
										<input type="number" min="1" class="form-control" id="cantidad" name="cantidad">
									</div>

									<div id="div-agregar">
										<button id="btnAgregarDetalle" type="button" class="btn btn-success btn-agregar"><i class="fas fa-cart-plus"></i> Agregar</button>
									</div>
									

								</div>
								<!-- Fin datos articulos -->

								<!-- Tabla -->
								<div style="height: 48vh !important;" class="mt-4 float-left panel-body table-responsive">
									<table id="tblDetalles" class="table table-striped table-bordered table-condensed table-hover" style="width:100%; ">
										<thead class="thead-dark">
											<th>Opciones</th>
											<th>Cantidad</th>
											<th>Descripción</th>
											<th>Precio&nbsp;V.</th>
											<th>Subtotal</th>
										</thead>

										<tbody>

										</tbody>
									</table>
								</div>
								<!-- Fin tabla -->

								<!-- Datos pago -->
								<div id="datos-pago" class="mt-4 float-left">
									<h4 class="font-weight-bold">Datos Pago</h4>

									<div id="div-cobrador" class="form-group float-left">
										<label><span class="text-danger">(*)</span> Cobrador</label>
										<select id="idCobrador" name="idCobrador" data-lang="es_ES" title="Seleccione Cobrador" class="selectpicker form-control" data-live-search="true" required>
											
										</select>
									</div>
									
									<div id="datos-cuotas">
										<div id="div-diac" class="form-group float-left">
											<label><span class="text-danger">(*)</span> Dia Cobro</label>
											<select id="diaCobro" name="diaCobro" class="selectpicker form-control" title="Seleccione Dia" data-live-search="true" required>
												<?php
												for ($i=1; $i<=31; $i++) { 
													echo "<option value=".$i." >".$i."</option>";
												}
												?>
											</select>
										</div>

										<div id="div-montoabo" class="form-group float-left">
											<label>Monto Abonado</label>
											<input type="text" class="form-control" id="montoAbonado" name="montoAbonado">
										</div>

										<div id="div-cuotas" class="form-group float-left">
											<label><span class="text-danger">(*)</span> Cuotas</label>
											<select id="cuotas" name="cuotas" class="selectpicker form-control" required>
												<?php
												for ($i=1; $i<=8 ; $i++) { 
													echo "<option value=".$i.">".$i."</option>";
												}
												?>
											</select>
										</div>

										<div id="div-montocuo" class="form-group float-left">
											<label><span class="text-danger">(*)</span> Monto Cuota</label>
											<input type="hidden" id="montoCuota" name="montoCuota">
											<input type="text" class="font-weight-bold form-control" id="montoCuotaString" name="montoCuotaString" readonly>
										</div>
									</div>
									

									

								</div>
								<!-- Fin datos pago -->

								<!-- Total -->
								<div id="datos-total" class="mt-4 float-right">
									<div id="div-total" class="form-group float-right">
										<h3 class="font-weight-bold"> Total</h3>
										<div class="input-group mb-3">
											<div class="input-group-prepend">
												<span class="input-group-text"><i class="fas fa-dollar-sign"></i></span>
											</div>
											<input type="hidden" name="total" id="total">
											<input type="text" class="font-weight-bold form-control" id="totalString" name="totalString" readonly>
										</div>
									</div>
								</div>
								<!-- Fin total -->
								
								<div class="float-right mt-4 mb-4">
									<button type="button" id="btnCancelar" class="btn btn-danger">Cancelar</button>
									<button type="submit" id="btnGuardar" class="btn btn-primary">Guardar</button>
								</div>
									
								
								
							</form>
						</div>
					</div>
					<!-- End formulario -->
				</div>
			</div>
		</div>

		<!-- Modales -->

		<!-- Buscar cliente  para venta-->
		<div class="modal fade" id="buscarClienteParaVenta" tabindex="-1" role="dialog" aria-hidden="true" data-backdrop="static" data-keyboard="false">
			<div class="modal-dialog modal-lg">
				<div class="modal-content">
					<div class="modal-header">
						<h5 class="modal-title">Buscar Cliente</h5>
						<button type="button" class="close" data-dismiss="modal" aria-label="Close">
							<span aria-hidden="true">&times;</span>
						</button>
					</div>
					<div class="modal-body table-responsive">
						<table id="tblClientesVenta" class="table table-striped table-bordered table-condensed table-hover" style="width: 100%;">
							<thead>
								<th>&nbsp;Opciones&nbsp;</th>
								<th>&nbsp;Apellido&nbsp;y&nbsp;Nombre</th>
								<th>&nbsp;Dni</th>
								<th>&nbsp;Barrio&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</th>
								<th>&nbsp;Domicilio&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</th>
								<th>&nbsp;Teléfono&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</th>
								<th>&nbsp;Saldo&nbsp;Pendiente&nbsp;Total&nbsp;</th>
							</thead>
						</table>  
					</div>
				</div>
			</div>
		</div>
		<!-- Fin buscar cliente para venta -->

		<!-- Buscar cliente  para filtrar-->
          <div class="modal fade" id="buscarClienteParaFiltrar" tabindex="-1" role="dialog" aria-hidden="true" data-backdrop="static" data-keyboard="false">
            <div class="modal-dialog modal-lg">
              <div class="modal-content">
                <div class="modal-header">
                  <h5 class="modal-title">Buscar Cliente</h5>
                  <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                  </button>
                </div>
                <div class="modal-body table-responsive">
                	<table id="tblClientesFiltrar" class="table table-striped table-bordered table-condensed table-hover" style="width: 100%;">
                		<thead>
                			<th>&nbsp;Opciones&nbsp;</th>
                			<th>&nbsp;Apellido&nbsp;y&nbsp;Nombre</th>
                			<th>&nbsp;Dni</th>
                			<th>&nbsp;Barrio&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</th>
                			<th>&nbsp;Domicilio&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</th>
                			<th>&nbsp;Teléfono&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</th>
                			<th>&nbsp;Saldo&nbsp;Pendiente&nbsp;Total&nbsp;</th>
                		</thead>
                	</table>  
                </div>
              </div>
            </div>
          </div>
          <!-- Fin buscar cliente para filtrar -->

          <!-- Seleccionar fecha -->
          <div class="modal fade" id="seleccionarFechas" tabindex="-1" role="dialog" aria-hidden="true" data-backdrop="static" data-keyboard="false">
            <div class="modal-dialog modal-sm">
              <div class="modal-content">
                <div class="modal-header">
                  <h5 class="modal-title">Seleccionar Rango Fechas</h5>
                  <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                  </button>
                </div>
                <div class="modal-body">

                  <div class="form-group" id="div-rango-fechas">
                    <div class="input-daterange input-group">
                      <input type="text" class="input-sm form-control" name="fechaIniListar" id="fechaIniListar">
                      <div class="input-group-prepend">
                        <span class="input-group-text">-</span>
                      </div>
                      <input type="text" class="input-sm form-control" name="fechaFinListar" id="fechaFinListar">
                    </div>
                  </div>

                  <div class="mt-4 mb-4 float-right">
                    <button type="button" id="btnCancelarFecha" class="btn btn-danger">Cancelar</button>
                    <button type="button" id="btnListarPorFecha" class="btn btn-primary">Listar</button>
                  </div>

                </div>
              </div>
            </div>
          </div>
          <!-- Fin seleccionar fecha -->
          
		<!-- Fin modales -->
		<?php
		} 	
		// Fin contenido autorizado
		?>
	</div>	
	<!-- End Container fluid -->
</div>
<!-- End Content -->
<?php
require ('footer.php');
?>

<script src="scripts/venta.js?ver=<?php echo $version?>"></script>

<?php
} //Fin llave
ob_end_flush(); 
?>