<?php
//activamos el almacenmiento de la sesion
ob_start();
session_start();

if(!isset($_SESSION['idUsuarioSisCob'])){
  header('Location: login');
}else{
  //Llave
  require ('header.php');
?>


	<!-- Container fluid -->
	<div class="container-fluid" id="container-wrapper">

		<?php
		if($_SESSION['v_pedidos']==0){
			echo '<div class="d-sm-flex align-items-center justify-content-between mb-4">
					<h1 class="h3 mb-0 text-gray-800">Acceso denegado</h1>
				</div>';
		}else{
		// Contenido autorizado
		?>

		<div class="d-sm-flex align-items-center justify-content-between mb-4">
			<h1 class="h3 mb-0 text-gray-800"><i class="far fa-clipboard"></i> Reclamos</h1>
		</div>

		<div class="row mb-3">
			<div class="col-lg-12">
				<div class="card mb-4">
					<div id="contenedor-cabecera" class="card-header py-3 d-flex flex-row align-items-center justify-content-between">

						<?php
						if ($_SESSION['new_reclamos']==1) {
							echo "<button id='btnNuevo' class='btn btn-success'><i class='fas fa-plus-circle'></i> Nuevo</button>";
						}
						?>
						
					</div>
					<!-- Tabla -->
					<div id="listado" class="table-responsive p-3">
						<table id="tblListado" class="table align-items-center table-hover table-bordered" style="width: 100%;">
							<thead class="thead-light">
								<th>&nbsp;Opciones&nbsp;</th>
								<th>&nbsp;Fecha&nbsp;Reclamo&nbsp;</th>
								<th>&nbsp;Estado&nbsp;</th>
								<th>&nbsp;Apellido&nbsp;Nombre&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</th>
								<th>&nbsp;Dni&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</th>	
								<th>&nbsp;Motivo&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</th>
							</thead>
						</table>
					</div>
					<!-- End tabla -->

					<!-- Formulario -->
					<div id="formulario">
						<div class="card-body">

							<div id="div-ultima-mod" class="card bg-primary text-white mb-2">
								<div class="card-body">
									<label class="font-weight-bold">Ultima Modificación: </label>
									<span id="ultimaModNombre"></span>
									<br>
									<label class="font-weight-bold">Fecha: </label>
									<span id="ultimaModFecha"></span>
									<br>
									<label class="font-weight-bold">Hora: </label>
									<span id="ultimaModHora"></span>
								</div>
							</div>
							
							<form id="form">

								<input type="hidden" id="idReclamo" name="idReclamo">

								<div class="form-group float-left input-width-50-100 pr-lg-4" id="simple-date1">
									<label><span class="text-danger">(*)</span> Fecha Reclamo</label>
									<div class="input-group date">
										<div class="input-group-prepend">
											<span class="input-group-text"><i class="fas fa-calendar"></i></span>
										</div>
										<input type="text" autocomplete="off" class="form-control" id="fechaReclamo" name="fechaReclamo" required>
									</div>
								</div>

								<div class="form-group float-lg-left pl-lg-4 input-width-50-100">
									<label><span class="text-danger">(*)</span> Estado</label>
									<select id="estado" name="estado" data-lang="es_ES" title="Seleccione estado" class="selectpicker form-control" required>
										<option value="PENDIENTE">PENDIENTE</option>
										<option value="SOLUCIONADO">SOLUCIONADO</option>
									</select>
								</div>

								<input type="hidden" id="idCliente" name="idCliente">
								<div class="form-group float-left pr-lg-4 input-width-50-100">
									<label><span class="text-danger">(*)</span> Dni</label>
									<div class="input-group">
										<input type="text" class="form-control" id="dni" name="dni" readonly required>
										<div class="input-group-append">
											<button type="button" class="btn btn-primary" id="btnBuscarCliente"><i class="fas fa-search"></i></button>
										</div>
									</div>
								</div>

								<div class="form-group float-left pl-lg-4 input-width-50-100">
									<label>Apellido y Nombre</label>
									<input type="text" class="form-control" id="apellidoNombre" name="apellidoNombre" readonly>
								</div>

								

								<div class="form-group float-lg-left w-100">
									<label><span class="text-danger">(*)</span> Motivo</label>
									<span id="cantMot" class="font-weight-bold"></span><span class="font-weight-bold">/200</span>
									<textarea type="textarea" class="form-control" id="motivo" name="motivo" rows="3" data-maxsize="200" data-output="cantMot" required></textarea>
								</div>

								
								<div class="mt-4 mb-4 float-right">
									<button type="button" id="btnCancelar" class="btn btn-danger">Cancelar</button>
									<button type="submit" id="btnGuardar" class="btn btn-primary">Guardar</button>
								</div>
								
							</form>
						</div>
					</div>
					<!-- End formulario -->
				</div>
			</div>
		</div>

		<!-- Modales -->

			<!-- Buscar cliente -->
			<div class="modal fade" id="buscarCliente" tabindex="-1" role="dialog" aria-hidden="true" data-backdrop="static" data-keyboard="false">
				<div class="modal-dialog modal-lg">
					<div class="modal-content">
						<div class="modal-header">
							<h5 class="modal-title">Buscar Cliente</h5>
							<button type="button" class="close" data-dismiss="modal" aria-label="Close">
								<span aria-hidden="true">&times;</span>
							</button>
						</div>
						<div class="modal-body table-responsive">
							<table id="tblClientes" class="table table-striped table-bordered table-condensed table-hover" style="width: 100%;">
								<thead>
									<th>&nbsp;Opciones&nbsp;</th>
									<th>&nbsp;Apellido&nbsp;y&nbsp;Nombre</th>
									<th>&nbsp;Dni</th>
									<th>&nbsp;Barrio&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</th>
									<th>&nbsp;Domicilio&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</th>
									<th>&nbsp;Teléfono&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</th>
								</thead>
							</table>  
						</div>
					</div>
				</div>
			</div>
			<!-- Fin buscar cliente -->

		<!-- Fin modales -->

		<?php
		} 	
		// Fin contenido autorizado
		?>
	</div>	
	<!-- End Container fluid -->
</div>
<!-- End Content -->
<?php
require ('footer.php');
?>

<script src="scripts/reclamo.js?ver=<?php echo $version?>"></script>

<?php
} //Fin llave
ob_end_flush(); 
?>