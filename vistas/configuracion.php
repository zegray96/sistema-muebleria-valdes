<?php
//activamos el almacenmiento de la sesion
ob_start();
session_start();

if(!isset($_SESSION['idUsuarioSisCob'])){
  header('Location: login');
}else{
  //Llave
  require ('header.php');
?>


	<!-- Container fluid -->
	<div class="container-fluid" id="container-wrapper">
		<?php
		if($_SESSION['alt_configuracion']==0){
			echo '<div class="d-sm-flex align-items-center justify-content-between mb-4">
					<h1 class="h3 mb-0 text-gray-800">Acceso denegado</h1>
				</div>';
		}else{
		// Contenido autorizado
		?>

		<div class="d-sm-flex align-items-center justify-content-between mb-4">
			<h1 class="h3 mb-0 text-gray-800"><i class="fas fa-cog"></i> Configuración</h1>
		</div>

		<div class="row mb-3">
			<div class="col-lg-12">
				<div class="card mb-4">
					<!-- Formulario -->
					<div id="formulario">
						<div class="card-body">
							<form id="form">

								<div class="form-group float-lg-left pr-lg-4 input-width-50-100">
									<label><span class="text-danger">(*)</span> Nombre Empresa</label>
									<input type="text" class="form-control" id="nombreEmpresa" name="nombreEmpresa" 
									placeholder="Ingrese nombre de empresa" data-maxsize="40" required>
								</div>

								<div class="form-group float-lg-left pl-lg-4 input-width-50-100">
									<label><span class="text-danger">(*)</span> Descripción Corta Empresa</label>
									<input type="text" class="form-control" id="descripcionCortaEmpresa" name="descripcionCortaEmpresa" 
									placeholder="Ingrese descripcion corta de empresa" data-maxsize="70" required>
								</div>

								<div class="form-group float-lg-left pr-lg-4 input-width-50-100">
									<label><span class="text-danger">(*)</span> Telefono</label>
									<input type="text" class="form-control" id="telefono" name="telefono" 
									placeholder="Ingrese telefono" data-maxsize="30" required>
								</div>

								<div class="form-group float-lg-left pl-lg-4 input-width-50-100">
									<label>Logo Empresa <span class="text-danger">(.png)</span></label>
									<input type="file" class="form-control" name="logoEmpresa" id="logoEmpresa">


									<div class="mt-3 div-img-logo-icono">
										<img id="logoEmpresaImg" src="../assets/img/logo-empresa.png" width="150px" height="150px">
									</div>
									

								</div>
									
								
								
								<div class="mb-4 float-right botones-con-margen" style="">
									<button type="button" id="btnCancelar" class="btn btn-danger">Cancelar</button>
									<button type="submit" id="btnGuardar" class="btn btn-primary">Guardar</button>
								</div>
								
							</form>
						</div>
					</div>
					<!-- End formulario -->
				</div>
			</div>
		</div>

		<?php
		} 	
		// Fin contenido autorizado
		?>
	</div>	
	<!-- End Container fluid -->
</div>
<!-- End Content -->
<?php
require ('footer.php');
?>

<script src="scripts/configuracion.js?ver=<?php echo $version?>"></script>

<?php
} //Fin llave
ob_end_flush(); 
?>