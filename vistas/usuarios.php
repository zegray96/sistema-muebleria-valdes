<?php
//activamos el almacenmiento de la sesion
ob_start();
session_start();

if(!isset($_SESSION['idUsuarioSisCob'])){
  header('Location: login');
}else{
  //Llave
  require ('header.php');
?>


	<!-- Container fluid -->
	<div class="container-fluid" id="container-wrapper">

		<?php
		if($_SESSION['v_acceso']==0){
			echo '<div class="d-sm-flex align-items-center justify-content-between mb-4">
					<h1 class="h3 mb-0 text-gray-800">Acceso denegado</h1>
				</div>';
		}else{
		// Contenido autorizado
		?>

		<div class="d-sm-flex align-items-center justify-content-between mb-4">
			<h1 class="h3 mb-0 text-gray-800"><i class="fas fa-user"></i> Usuarios</h1>
		</div>

		<div class="row mb-3">
			<div class="col-lg-12">
				<div class="card mb-4">
					<div id="contenedor-cabecera" class="card-header py-3 d-flex flex-row align-items-center justify-content-between">
						<?php
						if ($_SESSION['new_acceso']==1) {
							echo "<button id='btnNuevo' class='btn btn-success'><i class='fas fa-plus-circle'></i> Nuevo</button>";
						}
						?>
						
					</div>
					<!-- Tabla -->
					<div id="listado" class="table-responsive p-3">
						<table id="tblListado" class="table align-items-center table-hover table-bordered" style="width: 100%;">
							<thead class="thead-light">
								<th>&nbsp;Opciones&nbsp;</th>
								<th>&nbsp;Apellido&nbsp;y&nbsp;Nombre&nbsp;</th>
								<th>&nbsp;Usuario&nbsp;</th>
								<th>&nbsp;Rol&nbsp;</th>
								<th>&nbsp;Estado&nbsp;</th>
							</thead>
						</table>
					</div>
					<!-- End tabla -->

					<!-- Formulario -->
					<div id="formulario">
						<div class="card-body">
							<form id="form">

								<input type="hidden" id="idUsuario" name="idUsuario">

								<div class="form-group float-lg-left pr-lg-4 input-width-50-100">
									<label><span class="text-danger">(*)</span> Apellido y Nombre</label>
									<input type="text" class="form-control" id="apellidoNombre" name="apellidoNombre" 
									placeholder="Ingrese apellido y nombre" data-maxsize="50" required style="text-transform: capitalize;">
								</div>

								<div class="form-group float-lg-left pl-lg-4 input-width-50-100">
									<label><span class="text-danger">(*)</span> Usuario</label>
									<input type="text" class="form-control" id="usuario" name="usuario" placeholder="Ingrese usuario" data-maxsize="20" required>
								</div>

								<div class="form-group float-lg-left pr-lg-4 input-width-50-100">
									<label class="mr-1"><span class="text-danger">(*)</span> Clave</label> 
									<a id="linkModificarClave" data-toggle="modal" href="#modificarClave" class="badge badge-info" style="font-size: 15px"> Modificar</a>
									<input type="password" class="form-control" id="clave" name="clave" data-maxsize="100" required placeholder="Ingrese clave">

								</div>

								<div class="form-group float-lg-left pl-lg-4 input-width-50-100">
									<label><span class="text-danger">(*)</span> Rol</label>
									<select id="idRol" name="idRol" data-lang="es_ES" title="Seleccione rol" class="selectpicker form-control" data-live-search="true" required>
										
									</select>
								</div>
									
								<div class="form-group float-lg-left pr-lg-4 input-width-50-100">
									<label><span class="text-danger">(*)</span> Estado</label>
									<select id="estado" name="estado" data-lang="es_ES" title="Seleccione estado" class="selectpicker form-control" required>
										<option value="ACTIVO">ACTIVO</option>
										<option value="INACTIVO">INACTIVO</option>	
									</select>
								</div>

								<div class="form-group float-lg-left pl-lg-4 input-width-50-100">
									<!-- div vacio -->
								</div>
								
								
								<div class="mt-4 mb-4 float-right">
									<button type="button" id="btnCancelar" class="btn btn-danger">Cancelar</button>
									<button type="submit" id="btnGuardar" class="btn btn-primary">Guardar</button>
								</div>
								
							</form>
						</div>
					</div>
					<!-- End formulario -->
				</div>
			</div>
		</div>

		<!-- Modales -->

          <!-- Modificar clave -->
          <div class="modal fade" id="modificarClave" tabindex="-1" role="dialog" aria-hidden="true" data-backdrop="static" data-keyboard="false">
            <div class="modal-dialog modal-sm">
              <div class="modal-content">
                <div class="modal-header">
                  <h5 class="modal-title">Modificar clave</h5>
                  <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                  </button>
                </div>
                <div class="modal-body">

                	<form id="formModificarClave">

                		<input type="hidden" id="idUsuarioModificar" name="idUsuarioModificar">

                		<div class="form-group">
                			<input type="password" class="form-control" id="claveModificar" name="claveModificar" data-maxsize="100" required placeholder="Ingrese nueva clave">
                		</div>

                		<div class="form-group">
                			<input type="password" class="form-control" id="repetirClaveModificar" name="repetirClaveModificar" data-maxsize="100" required placeholder="Repita nueva clave">
                		</div>

                		<div class="mb-4 float-right">
                			<button type="button" id="btnCancelarModificarClave" class="btn btn-danger">Cancelar</button>
                			<button type="submit" id="btnGuardarModificarClave" class="btn btn-primary">Guardar</button>
                		</div>

                	</form>

                </div>
              </div>
            </div>
          </div>
          <!-- Fin seleccionar fecha -->
          
		<!-- Fin modales -->


		<?php
		} 	
		// Fin contenido autorizado
		?>
	</div>	
	<!-- End Container fluid -->
</div>
<!-- End Content -->
<?php
require ('footer.php');
?>

<script src="scripts/usuario.js?ver=<?php echo $version?>"></script>

<?php
} //Fin llave
ob_end_flush(); 
?>