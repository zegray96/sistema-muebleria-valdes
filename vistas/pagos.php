<?php
//activamos el almacenmiento de la sesion
ob_start();
session_start();

if(!isset($_SESSION['idUsuarioSisCob'])){
  header('Location: login');
}else{
  //Llave
  require ('header.php');
?>


	<!-- Container fluid -->
	<div class="container-fluid" id="container-wrapper">

		<?php
		if($_SESSION['v_pagos']==0){
			echo '<div class="d-sm-flex align-items-center justify-content-between mb-4">
					<h1 class="h3 mb-0 text-gray-800">Acceso denegado</h1>
				</div>';
		}else{
		// Contenido autorizado
		?>

		<div class="d-sm-flex align-items-center justify-content-between mb-4">
			<h1 class="h3 mb-0 text-gray-800"><i class="fas fa-receipt"></i> Pagos</h1>
		</div>

		<div class="row mb-3">
			<div class="col-lg-12">
				<div class="card mb-4">
					<div id="contenedor-cabecera" class="card-header py-3 d-flex flex-row align-items-center justify-content-between">

						<p id="textFiltro" class="card bg-primary text-white font-weight-bold mb-3 p-1" style=""></p>

						<div class="input-width-50-100 float-left pr-lg-4 mb-3">
							<select id="listarPor" class="selectpicker form-control" title="<i class='fas fa-filter'></i> Listar Por">
								<option value="Pagos de hoy">Pagos de hoy</option>
								<option value="Fechas">Fechas</option>
							</select>
						</div>

						<div class="input-width-50-100 float-left pl-lg-4 mb-3">
							<select id="listarPorCobrador" class="selectpicker form-control" data-live-search="true">

							</select>
						</div>
						
					</div>
					<!-- Tabla -->
					<div id="listado" class="table-responsive p-3">
						<table id="tblListado" class="table align-items-center table-hover table-bordered" style="width: 100%;">
							<thead class="thead-light">
								<th>&nbsp;Opciones&nbsp;</th>
								<th>&nbsp;N°&nbsp;Pago&nbsp;</th>
								<th>&nbsp;Apellido&nbsp;y&nbsp;Nombre&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</th>
								<th>&nbsp;Domicilio&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</th>
								<th>&nbsp;Monto&nbsp;Pagado&nbsp;</th>
								<th>&nbsp;Registrado&nbsp;Por&nbsp;</th>
							</thead>
						</table>
					</div>
					<!-- End tabla -->

				</div>
			</div>
		</div>
		<!-- Modales -->
			<!-- Seleccionar rango fecha -->
	          <div class="modal fade" id="seleccionarRangoFechas" tabindex="-1" role="dialog" aria-hidden="true" data-backdrop="static" data-keyboard="false">
	            <div class="modal-dialog modal-sm">
	              <div class="modal-content">
	                <div class="modal-header">
	                  <h5 class="modal-title">Seleccionar Rango Fechas</h5>
	                  <button type="button" class="close" data-dismiss="modal" aria-label="Close">
	                    <span aria-hidden="true">&times;</span>
	                  </button>
	                </div>
	                <div class="modal-body">

	                  <div class="form-group" id="div-rango-fechas">
	                    <div class="input-daterange input-group">
	                      <input type="text" class="input-sm form-control" name="fechaIniListar" id="fechaIniListar">
	                      <div class="input-group-prepend">
	                        <span class="input-group-text">-</span>
	                      </div>
	                      <input type="text" class="input-sm form-control" name="fechaFinListar" id="fechaFinListar">
	                    </div>
	                  </div>

	                  <div class="mt-4 mb-4 float-right">
	                    <button type="button" id="btnCancelarFecha" class="btn btn-danger">Cancelar</button>
	                    <button type="button" id="btnListarPorFecha" class="btn btn-primary">Listar</button>
	                  </div>

	                </div>
	              </div>
	            </div>
	          </div>
	          <!-- Fin seleccionar rango fecha venc -->
          <!-- Fin modales -->
		<?php
		} 	
		// Fin contenido autorizado
		?>
	</div>	
	<!-- End Container fluid -->
</div>
<!-- End Content -->
<?php
require ('footer.php');
?>

<script src="scripts/pago.js?ver=<?php echo $version?>"></script>

<?php
} //Fin llave
ob_end_flush(); 
?>