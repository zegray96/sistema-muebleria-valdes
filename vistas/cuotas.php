<?php
//activamos el almacenmiento de la sesion
ob_start();
session_start();

if(!isset($_SESSION['idUsuarioSisCob'])){
  header('Location: login');
}else{
  //Llave
  require ('header.php');
?>


	<!-- Container fluid -->
	<div class="container-fluid" id="container-wrapper">

		<?php
		if($_SESSION['v_cuotas']==0){
			echo '<div class="d-sm-flex align-items-center justify-content-between mb-4">
					<h1 class="h3 mb-0 text-gray-800">Acceso denegado</h1>
				</div>';
		}else{
		// Contenido autorizado
		?>

		<div class="d-sm-flex align-items-center justify-content-between mb-4">
			<h1 class="h3 mb-0 text-gray-800"><i class="fas fa-hand-holding-usd"></i> Cuotas</h1>
		</div>

		<div class="row mb-3">
			<div class="col-lg-12">
				<div class="card mb-4">
					<div id="contenedor-cabecera" class="card-header py-3 d-flex flex-row align-items-center justify-content-between">

            <p id="textFiltro" class="card bg-primary text-white font-weight-bold mb-3 p-1" style=""></p>

            <div class="input-width-50-100 float-left pr-lg-4 mb-3">
              <select id="listarPor" class="selectpicker form-control" title="<i class='fas fa-filter'></i> Listar Por">
                <option value="Vencimientos de hoy">Vencimientos de hoy</option>
                <option value="Fecha vencimiento">Fecha vencimiento</option>
                <option value="Cliente">Cliente</option>
                <option value="Todas">Todas</option>
              </select>
            </div>

            <div class="input-width-50-100 float-left pl-lg-4 mb-3">
              <select id="listarPorCobrador" class="selectpicker form-control" data-live-search="true">
                
              </select>
            </div>
            
					</div>
					<!-- Tabla -->
					<div id="listado" class="table-responsive p-3">
						<table id="tblListado" class="table align-items-center table-hover table-bordered" style="width: 100%;">
							<thead class="thead-light">
								<th>&nbsp;Opciones&nbsp;</th>
                <th>&nbsp;N°&nbsp;Comp.&nbsp;V.&nbsp;</th>
                <th>&nbsp;N°&nbsp;Cuota&nbsp;</th>
                <th>&nbsp;Estado&nbsp;</th>
                <th>&nbsp;Fecha&nbsp;Vencimiento&nbsp;</th>
                <th>&nbsp;Apellido&nbsp;y&nbsp;Nombre&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</th>
                <th>&nbsp;Barrio&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</th>
                <th>&nbsp;Domicilio&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</th>
                <th>&nbsp;Cobrador&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</th>
                <th>&nbsp;Saldo&nbsp;Pendiente&nbsp;Total&nbsp;</th>
								<th>&nbsp;Saldo&nbsp;Pendiente&nbsp;Venta&nbsp;</th>
								
							</thead>
						</table>
					</div>
					<!-- End tabla -->

          <!-- Formulario -->
          <div id="formularioCuota">
            <div class="card-body">
              <form id="formCuota">
                <input type="hidden" id="nroCuota" name="nroCuota">
                <input type="hidden" id="idVenta" name="idVenta">

                <div id="div-ultima-mod" class="card bg-primary text-white mb-2">
                  <div class="card-body">
                    <label class="font-weight-bold">Ultima Modificación: </label>
                    <span id="ultimaModNombre"></span>
                    <br>
                    <label class="font-weight-bold">Fecha: </label>
                    <span id="ultimaModFecha"></span>
                    <br>
                    <label class="font-weight-bold">Hora: </label>
                    <span id="ultimaModHora"></span>
                  </div>
                </div>

                <h1><span id="estado" class="badge badge-warning"></span></h1>

                <h4 class="font-weight-bold text-danger mb-3 mt-3">Monto Pendiente: $<span id="montoPendiente"></span></h4>

                <div class="form-group float-lg-left pr-lg-4 input-width-50-100">
                  <label>Nro Cuota</label>
                  <input type="text" class="form-control" id="nroCuotaString" readonly>
                </div>

                <div class="form-group float-lg-left pl-lg-4 input-width-50-100">
                  <label>Fecha Vencimiento</label>
                  <div class="input-group date">
                    <div class="input-group-prepend">
                      <span class="input-group-text"><i class="fas fa-calendar"></i></span>
                    </div>
                    <input maxlength="10" type="text" class="form-control" id="fechaVencimiento" name="fechaVencimiento" required>
                  </div>
                </div>

                <div class="form-group float-lg-left pr-lg-4 input-width-50-100">
                  <label>N° Comprobante V. Asociado</label>
                  <input type="text" class="form-control" id="nroComprobanteVentaAsoc" readonly>
                </div>
                

                <div class="form-group float-lg-left pl-lg-4 input-width-50-100">
                  <label>Monto Cuota</label>
                  <input type="text" class="form-control" id="montoCuota" readonly>
                </div>

                <div class="form-group float-lg-left pr-lg-4 input-width-50-100">
                  <label>Dni</label>
                  <input type="text" class="form-control" id="dni" readonly>
                </div>

                <div class="form-group float-lg-left pl-lg-4 input-width-50-100">
                  <label>Apellido y Nombre</label>
                  <input type="text" class="form-control" id="apellidoNombre" readonly>
                </div>

                <div class="form-group float-lg-left pr-lg-4 input-width-50-100">
                  <label>Monto Adicional</label>
                  <input type="text" class="form-control" id="montoAdicional" name="montoAdicional" placeholder="0.00">
                </div>

                

                <div class="form-group float-lg-left w-100">
                  <label>Observación</label>
                  <span id="cantObs" class="font-weight-bold"></span><span class="font-weight-bold">/100</span>
                  <textarea type="textarea" class="form-control" id="observacion" name="observacion" rows="3" data-maxsize="100" data-output="cantObs"></textarea>
                </div>

                <!-- Datos pago -->
                <div id="div-datos-pago" style="width: 100%;" class="float-left mt-3">
                  <h4 class="font-weight-bold">Pagos Asociados</h4>
                  <?php
                  if ($_SESSION['new_pagos']==1) {
                    echo "<button type='button' id='btnNuevoPago' class='btn btn-success mt-2 mb-3'>Nuevo Pago</button>";
                  }
                  ?>
                  
                                    
                  <div id="listPagos">

                  </div>
                </div>
                <!-- Fin datos pago  -->

                <div class="mt-4 mb-4 float-right">
                  <button type="button" id="btnCancelarCuota" class="btn btn-danger">Cancelar</button>
                  <button type="submit" id="btnGuardarCuota" class="btn btn-primary">Guardar</button>
                </div>

              </form>
            </div>
          </div>
          <!-- End formulario -->
					
				</div>
			</div>
		</div>

		<!-- Modales -->
    	  <!-- Datos pago -->
          <div class="modal fade" id="pago" tabindex="-1" role="dialog" aria-hidden="true" data-backdrop="static" data-keyboard="false">
            <div class="modal-dialog modal-lg">
              <div class="modal-content">
                <div class="modal-header">
                  <h5 class="modal-title">Datos de Pago</h5>
                  <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                  </button>
                </div>
                <div class="card-body">
                	<form id="formPago">

                    <input type="hidden" id="idPago" name="idPago">
                    <input type="hidden" id="nroCuotaFormPago" name="nroCuotaFormPago">
                    <input type="hidden" id="idVentaFormPago" name="idVentaFormPago">

                    <div class="form-group float-lg-left pr-lg-4 input-width-50-100">
                      <label>N° Comprobante</label>
                      <input type="text" class="form-control" id="nroComprobante" name="nroComprobante" readonly>
                    </div>

                    <div class="form-group float-lg-left input-width-50-100">
                      <label><span class="text-danger">(*)</span> Monto Pagado</label>
                      <input type="text" class="form-control" id="montoPagado" name="montoPagado" placeholder="0.00" required>
                    </div>

                    <div class="form-group float-lg-left pr-lg-4 input-width-50-100">
                      <label>Fecha y Hora Pago</label>
                      <input type="text" class="form-control" id="fechaHoraPago" name="fechaHoraPago" readonly>
                    </div>

                    <div class="form-group float-lg-left input-width-50-100">
                      <label>Registrado Por</label>
                      <input type="text" class="form-control" id="registradoPor" name="registradoPor" readonly>
                    </div>


                    <div class="mt-4 mb-4 float-right">
                      <button type="button" id="btnCancelarPago" class="btn btn-danger">Cancelar</button>
                      
                      <?php
                      if ($_SESSION['alt_pagos']==1) {
                        echo "<button type='button' id='btnEliminarPago' class='btn btn-warning'>Eliminar</button>";
                      }
                      ?>
                      <button type="button" id="btnImprimirPago" class="btn btn-info">Imprimir</button>

                      <button type="submit" id="btnGuardarPago" class="btn btn-primary">Guardar</button>
                    </div>

                  </form>
                </div>
              </div>
            </div>
          </div>
          <!-- Fin datos pago -->

          <!-- Buscar cliente -->
          <div class="modal fade" id="buscarClienteParaFiltrar" tabindex="-1" role="dialog" aria-hidden="true" data-backdrop="static" data-keyboard="false">
            <div class="modal-dialog modal-lg">
              <div class="modal-content">
                <div class="modal-header">
                  <h5 class="modal-title">Buscar Cliente</h5>
                  <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                  </button>
                </div>
                <div class="modal-body table-responsive">
                    <table id="tblClientes" class="table table-striped table-bordered table-condensed table-hover" style="width: 100%;">
                        <thead>
                            <th>&nbsp;Opciones&nbsp;</th>
                            <th>&nbsp;Apellido&nbsp;y&nbsp;Nombre</th>
                            <th>&nbsp;Dni</th>
                            <th>&nbsp;Barrio&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</th>
                            <th>&nbsp;Domicilio&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</th>
                            <th>&nbsp;Teléfono&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</th>
                        </thead>
                    </table>  
                </div>
              </div>
            </div>
          </div>
          <!-- Fin buscar cliente -->

          <!-- Seleccionar rango fecha venc -->
          <div class="modal fade" id="seleccionarRangoFechas" tabindex="-1" role="dialog" aria-hidden="true" data-backdrop="static" data-keyboard="false">
            <div class="modal-dialog modal-sm">
              <div class="modal-content">
                <div class="modal-header">
                  <h5 class="modal-title">Seleccionar Rango Fechas</h5>
                  <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                  </button>
                </div>
                <div class="modal-body">

                  <div class="form-group" id="div-rango-fechas">
                    <div class="input-daterange input-group">
                      <input type="text" class="input-sm form-control" name="fechaIniVencListar" id="fechaIniVencListar">
                      <div class="input-group-prepend">
                        <span class="input-group-text">-</span>
                      </div>
                      <input type="text" class="input-sm form-control" name="fechaFinVencListar" id="fechaFinVencListar">
                    </div>
                  </div>

                  <div class="mt-4 mb-4 float-right">
                    <button type="button" id="btnCancelarFechaVenc" class="btn btn-danger">Cancelar</button>
                    <button type="button" id="btnListarPorFechaVenc" class="btn btn-primary">Listar</button>
                  </div>

                </div>
              </div>
            </div>
          </div>
          <!-- Fin seleccionar rango fecha venc -->

          <!--Modal Actualizando estado -->
          <div class="modal" id="actualizandoCuotasVencidasModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" data-backdrop="static" data-keyboard="false"> 
            <div class="modal-dialog modal-sm">
              <div class="modal-content">
                <div class="modal-header">
                  <h4 class="font-weight-bold">Actualizando cuotas vencidas...</h4>
                </div>
              </div>
            </div> 
          </div>
          <!--Fin Modal  Actualizando estado -->
          
		<!-- Fin modales -->
		<?php
		} 	
		// Fin contenido autorizado
		?>
	</div>	
	<!-- End Container fluid -->
</div>
<!-- End Content -->
<?php
require ('footer.php');
?>

<script src="scripts/cuota-pago.js?ver=<?php echo $version?>"></script>

<?php
} //Fin llave
ob_end_flush(); 
?>