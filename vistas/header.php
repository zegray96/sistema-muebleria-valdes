<!DOCTYPE html>
<html lang="es">

<head>
  <?php 
    include ('../config/version.php');
    include ('../modelos/Configuracion.php');
    $conf=new Configuracion();
    $respuesta=$conf->traer_configuracion();
    $nombreEmpresa=$respuesta['nombre_empresa'];
  ?>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <meta name="description" content="">
  <meta name="author" content="">
  <link id="iconoPagina" href="../assets/img/icono.ico?<?php $aleatorio=rand(); echo $aleatorio ?>" rel="icon">
  <title id="tituloPagina"></title>

  <link rel="stylesheet" href="../assets/css/fontawesome/all.min.css">
  <link rel="stylesheet" href="../assets/css/bootstrap/bootstrap.min.css">
  <link rel="stylesheet" href="../assets/css/ruang-admin.min.css" >
  <link rel="stylesheet" href="../assets/css/datatables/dataTables.bootstrap4.min.css">
  <!-- Bootstrap DatePicker -->  
  <link rel="stylesheet" href="../assets/css/bootstrap-datepicker/bootstrap-datepicker.min.css">
   <!-- Bootstrap select -->
  <link rel="stylesheet" href="../assets/css/bootstrap-select/bootstrap-select.min.css">

  <link rel="stylesheet" href="../assets/css/style.css?ver=<?php echo $version?>">

  
 </head>

<body id="page-top">
  <!-- Preload -->
  <div id="preload">    
    <div class="spinner-border"></div>
  </div>
  <!-- Fin preload -->

  <!-- Wrapper -->
  <div id="wrapper" style="display: none;">
    <!-- Sidebar -->
    <ul class="navbar-nav sidebar sidebar-light accordion" id="accordionSidebar">
      <a class="sidebar-brand d-flex align-items-center justify-content-center" href="dashboard">
      	<div class="sidebar-brand-icon">
          <img id="logoHeader" src="../assets/img/logo-claro.png?<?php $aleatorio=rand(); echo $aleatorio ?>">
        </div>
        <div id="nombreEmpresaHeader" class="sidebar-brand-text mx-2"><?php echo $nombreEmpresa; ?></div>
      </a>
      <hr class="sidebar-divider my-0">
      <li class="nav-item active">
        <a class="nav-link" href="dashboard">
          <i class="fas fa-home"></i>
          <span>Dashboard</span></a>
      </li>
      <hr class="sidebar-divider">

      <div class="sidebar-heading">
        Secciones
      </div>

      <!-- Acceso -->
      <?php
      if($_SESSION['v_acceso']==1){
      ?> 

      <li class="nav-item">
        <a class="nav-link collapsed" href="#" data-toggle="collapse" data-target="#collapseTable" aria-expanded="true"
        aria-controls="collapseTable">
          <i class="fas fa-user-lock"></i>
          <span>Acceso</span>
        </a>
        <div id="collapseTable" class="collapse" aria-labelledby="headingTable" data-parent="#accordionSidebar">
          <div class="bg-white py-2 collapse-inner rounded">
            <a class="collapse-item" href="usuarios"><i class="fas fa-user"></i> Usuarios</a>
            <a class="collapse-item" href="roles"><i class="fas fa-user-cog"></i> Roles</a>
          </div>
        </div>
      </li>

      <?php
      }
      ?>
      
      <!-- Clientes -->
      <?php
      if($_SESSION['v_clientes']==1){
      ?> 

      <li class="nav-item">
        <a class="nav-link" href="clientes">
          <i class="fas fa-users"></i>
          <span>Clientes</span>
        </a>
      </li>

      <?php
      }
      ?>

      <!-- Ventas -->
      <?php
      if($_SESSION['v_ventas']==1){
      ?> 

      <li class="nav-item">
        <a class="nav-link" href="ventas">
          <i class="fas fa-shopping-cart"></i>
          <span>Ventas</span>
        </a>
      </li>

      <?php
      }
      ?>

      <!-- Cuotas -->
      <?php
      if($_SESSION['v_cuotas']==1){
      ?> 

      <li class="nav-item">
        <a class="nav-link" href="cuotas">
          <i class="fas fa-hand-holding-usd"></i>
          <span>Cuotas</span>
        </a>
      </li>

      <?php
      }
      ?>

      <!-- Pagos -->
      <?php
      if($_SESSION['v_pagos']==1){
      ?> 

      <li class="nav-item">
        <a class="nav-link" href="pagos">
          <i class="fas fa-receipt"></i>
          <span>Pagos</span>
        </a>
      </li>

      <?php
      }
      ?>

      <!-- Pedidos -->
      <?php
      if($_SESSION['v_pedidos']==1){
      ?> 

      <li class="nav-item">
        <a class="nav-link" href="pedidos">
          <i class="fas fa-shipping-fast"></i>
          <span>Pedidos</span>
        </a>
      </li>

      <?php
      }
      ?>

       <!-- Reclamos -->
      <?php
      if($_SESSION['v_reclamos']==1){
      ?> 

      <li class="nav-item">
        <a class="nav-link" href="reclamos">
          <i class="far fa-clipboard"></i>
          <span>Reclamos</span>
        </a>
      </li>

      <?php
      }
      ?>

     
     
      <hr class="sidebar-divider">
      <div class="version">Version <?php echo $version?></div>

    </ul>
    <!-- Sidebar -->

    <!-- Content-wrapper -->
    <div id="content-wrapper" class="d-flex flex-column">
      <!-- Content -->
      <div id="content">
        <!-- TopBar -->
        <nav class="navbar navbar-expand navbar-light bg-navbar topbar mb-4 static-top">
          <button id="sidebarToggleTop" class="btn btn-link rounded-circle mr-3">
            <i class="fa fa-bars"></i>
          </button>
          <ul class="navbar-nav ml-auto">
            <li class="nav-item dropdown no-arrow">
              <a class="nav-link dropdown-toggle" href="#" id="userDropdown" role="button" data-toggle="dropdown"
                aria-haspopup="true" aria-expanded="false">
                <img class="img-user" src="../assets/img/user.png">
                <span class="ml-2 d-none d-lg-inline text-white name-user">
                  <?php echo $_SESSION['apellidoNombre'];?>                  
                </span>
              </a>
              <div class="dropdown-menu dropdown-menu-right shadow animated--grow-in" aria-labelledby="userDropdown">
                <a class="dropdown-item" href="mi-cuenta">
                  <i class="fas fa-user fa-sm fa-fw mr-2 text-gray-400"></i>
                  Mi Cuenta
                </a>

                <?php
                if ($_SESSION['alt_configuracion']==1) {
                ?>
                <a class="dropdown-item" href="configuracion">
                  <i class="fas fa-cogs fa-sm fa-fw mr-2 text-gray-400"></i>
                  Configuración
                </a>

                <?php
                }
                ?>
               
                <div class="dropdown-divider"></div>
                <a id="btnCerrarSesion" class="dropdown-item cerrar-sesion">
                  <i class="fas fa-sign-out-alt fa-sm fa-fw mr-2 text-gray-400"></i>
                  Cerrar Sesión
                </a>
              </div>
            </li>
          </ul>
        </nav>
        <!-- Topbar -->

