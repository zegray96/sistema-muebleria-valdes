<?php
// Los header en php siempre se deben hacer antes de mostrar una pantalla
session_start();
if(isset($_SESSION['idUsuarioSisCob'])){
  // si tenemos una sesion inicada mostramos el dashboard 
  header('Location: dashboard');
}else{  
  // si no tenemos, mostramos login
?>
<!DOCTYPE html>
<html lang="es">
<head>
  <?php
    include ('../config/version.php');
  ?>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <link id="iconoPagina" href="../assets/img/icono.ico?<?php $aleatorio=rand(); echo $aleatorio ?>" rel="icon">
  <title id="tituloPagina"></title>
  <link rel="stylesheet" href="../assets/css/fontawesome/all.min.css">
  <link rel="stylesheet" href="../assets/css/bootstrap/bootstrap.min.css">
  <link rel="stylesheet" href="../assets/css/ruang-admin.min.css">
  <link rel="stylesheet" href="../assets/css/datatables/dataTables.bootstrap4.min.css">
  <link rel="stylesheet" href="../assets/css/style.css?ver=<?php echo $version?>">

</head>

<body class="bg-gradient-login">
   <!-- Preload -->
  <div id="preload">    
    <div class="spinner-border"></div>
  </div>
  <!-- Fin preload -->

	<!-- Login Content -->
	<div id="container" class="container" style="display: none;">
		<div class="row">
			<div class="col-sm-9 col-md-7 col-lg-6 col-xl-5 mx-auto">
				<div class="card card-signin my-5">
					<div class="card-body">
						<div class="text-center mb-2">
							<img src="../assets/img/logo-oscuro.png?<?php $aleatorio=rand(); echo $aleatorio ?>" class="logo-login">
							<h1 class="h4 text-gray-900 mb-4 mt-4">Ingrese sus datos</h1>
						</div>

						<form id="frmLogin" class="user">

              <div class="input-group mb-3">
                <div class="input-group-prepend">
                  <span class="input-group-text"><i class="fas fa-user"></i></span>
                </div>
                <input type="usuario" class="form-control" id="usuario" name="usuario" placeholder="Ingrese Usuario" required>
              </div>

              <div class="input-group">
                <div class="input-group-prepend">
                  <span class="input-group-text"><i class="fas fa-lock"></i></span>
                </div>

                <input type="password" class="form-control" id="clave" name="clave" placeholder="Ingrese Clave" required>
                <div class="input-group-append">
                  <button id="btnVerClave" class="btn btn-primary" type="button"><i id="verClave" class="fas fa-eye"></i> </button>
                </div>
              </div>

							<hr>

							<div class="form-group">
								<button type="submit" class="btn btn-primary btn-block">Ingresar</button>
							</div>

						</form>

					</div>
				</div>
			</div>
		</div>
	</div>
  	<!-- Login Content -->

  <script src="../assets/js/jquery/jquery.min.js"></script>
  <script src="../assets/js/jquery-easing/jquery.easing.min.js"></script>
  <script src="../assets/js/bootstrap/bootstrap.bundle.min.js"></script>
  <script src="../assets/js/ruang-admin.min.js"></script>

  <!-- Datatables -->
  <script src="../assets/js/datatables/jquery.dataTables.min.js"></script>
  <script src="../assets/js/datatables/dataTables.bootstrap4.min.js"></script>
  <script src="../assets/js/datatables/buttons.print.min.js"></script>
  <script src="../assets/js/datatables/dataTables.buttons.min.js"></script>
  <script src="../assets/js/datatables/jszip.min.js"></script>
  <script src="../assets/js/datatables/pdfmake.min.js"></script>
  <script src="../assets/js/datatables/vfs_fonts.js"></script>
  <script src="../assets/js/datatables/buttons.html5.min.js"></script>

  <!-- Sweetalert2 -->
  <script src="../assets/js/sweetalert2/sweetalert2.all.min.js"></script>

  <script src="scripts/login.js?ver=<?php echo $version?>"></script>  
  <script src="scripts/sesion.js?ver=<?php echo $version?>"></script>
</body>

</html>

<?php

  }

?>