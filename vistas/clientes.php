<?php
//activamos el almacenmiento de la sesion
ob_start();
session_start();

if(!isset($_SESSION['idUsuarioSisCob'])){
  header('Location: login');
}else{
  //Llave
  require ('header.php');
?>


	<!-- Container fluid -->
	<div class="container-fluid" id="container-wrapper">

		<?php
		if($_SESSION['v_clientes']==0){
			echo '<div class="d-sm-flex align-items-center justify-content-between mb-4">
					<h1 class="h3 mb-0 text-gray-800">Acceso denegado</h1>
				</div>';
		}else{
		// Contenido autorizado
		?>

		<div class="d-sm-flex align-items-center justify-content-between mb-4">
			<h1 class="h3 mb-0 text-gray-800"><i class="fas fa-users"></i> Clientes</h1>
		</div>

		<div class="row mb-3">
			<div class="col-lg-12">
				<div class="card mb-4">
					<div id="contenedor-cabecera" class="card-header py-3 d-flex flex-row align-items-center justify-content-between">
						<?php
						if ($_SESSION['new_clientes']==1) {
							echo "<button id='btnNuevo' class='btn btn-success'><i class='fas fa-plus-circle'></i> Nuevo</button>";
						}
						?>
						
					</div>
					<!-- Tabla -->
					<div id="listado" class="table-responsive p-3">
						<table id="tblListado" class="table align-items-center table-hover table-bordered" style="width: 100%;">
							<thead class="thead-light">
								<th>&nbsp;Opciones&nbsp;</th>
								<th>&nbsp;Apellido&nbsp;y&nbsp;Nombre&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</th>
								<th>&nbsp;Dni&nbsp;</th>
								<th>&nbsp;Barrio&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</th>
								<th>&nbsp;Domicilio&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</th>
								<th>&nbsp;Teléfono&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</th>
								<th>&nbsp;Saldo&nbsp;Pendiente&nbsp;Total&nbsp;</th>
							</thead>
						</table>
					</div>
					<!-- End tabla -->

					<!-- Formulario -->
					<div id="formulario">
						<div class="card-body">
							<form id="form">

								<input type="hidden" id="idCliente" name="idCliente">

								<div class="form-group float-lg-left pr-lg-4 input-width-50-100">
									<label><span class="text-danger">(*)</span> Apellido y Nombre</label>
									<input type="text" class="form-control" id="apellidoNombre" name="apellidoNombre" 
									placeholder="Ingrese apellido y nombre" data-maxsize="50" required style="text-transform: capitalize;">
								</div>

								<div class="form-group float-lg-left pl-lg-4 input-width-50-100">
									<label><span class="text-danger">(*)</span> Dni</label>
									<input type="text" class="form-control" id="dni" name="dni" placeholder="Ingrese dni" data-maxsize="10" onkeyup="format(this)" onchange="format(this)" required>
								</div>

								<div class="form-group float-lg-left pr-lg-4 input-width-50-100">
									<label><span class="text-danger">(*)</span> Barrio</label>
									<input type="text" class="form-control" id="barrio" name="barrio" placeholder="Ingrese dirección" data-maxsize="50" required style="text-transform: capitalize;">
								</div>

								<div class="form-group float-lg-left pl-lg-4 input-width-50-100">
									<label><span class="text-danger">(*)</span> Domicilio</label>
									<input type="text" class="form-control" id="domicilio" name="domicilio" placeholder="Ingrese dirección" data-maxsize="60" required style="text-transform: capitalize;">
								</div>

								<div class="form-group float-lg-left pr-lg-4 input-width-50-100">
									<label><span class="text-danger">(*)</span> Teléfono</label>
									<input type="text" class="form-control" id="telefono" name="telefono" placeholder="Ingrese telefono" data-maxsize="60" required>
								</div>

								<div class="form-group float-lg-left pl-lg-4 input-width-50-100">
									<!-- div vacio -->
								</div>
								
								<div class="mt-4 mb-4 float-right">
									<button type="button" id="btnCancelar" class="btn btn-danger">Cancelar</button>
									<button type="submit" id="btnGuardar" class="btn btn-primary">Guardar</button>
								</div>
								
							</form>
						</div>
					</div>
					<!-- End formulario -->
				</div>
			</div>
		</div>
		<?php
		} 	
		// Fin contenido autorizado
		?>
	</div>	
	<!-- End Container fluid -->
</div>
<!-- End Content -->
<?php
require ('footer.php');
?>

<script src="scripts/cliente.js?ver=<?php echo $version?>"></script>

<?php
} //Fin llave
ob_end_flush(); 
?>