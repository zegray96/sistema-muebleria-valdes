<?php
//activamos el almacenmiento de la sesion
ob_start();
session_start();

if(!isset($_SESSION['idUsuarioSisCob'])){
  header('Location: login');
}else{
  //Llave
  require ('header.php');
?>
	<!-- Container fluid -->
	<div class="container-fluid" id="container-wrapper">
		<div class="d-sm-flex align-items-center justify-content-between mb-4">
			<h1 class="h3 mb-0 text-gray-800"><i class="fas fa-home"></i> Dashboard</h1>
		</div>

		<div class="row mb-3">

			<?php 
			if($_SESSION['v_clientes']==1){
			?>

			<div class="col-xl-3 col-md-6 mb-4">
				<a class="a-dashboard" href="clientes">
					<div class="card h-100">
						<div class="card-body">
							<div class="row align-items-center">
								<div class="col mr-2">
									<div class="h5 mb-0 font-weight-bold text-gray-800">Clientes</div>
									<div class="mt-2 mb-0 text-muted h5">
										<span id="cantClientesDashboard" class="text-success mr-2"></span>
									</div>
								</div>
								<div class="col-auto">
			                      <i class="fas fa-users fa-2x text-info"></i>
			                    </div>
							</div>
						</div>
					</div>
				</a>
			</div>

			<?php	
			}
			?>
			
			<?php 
			if($_SESSION['v_ventas']==1){
			?>

			<div class="col-xl-3 col-md-6 mb-4">
				<a class="a-dashboard" href="ventas">
					<div class="card h-100">
						<div class="card-body">
							<div class="row align-items-center">
								<div class="col mr-2">
									<div class="h5 mb-0 font-weight-bold text-gray-800">Ventas</div>
									<div class="mt-2 mb-0 text-muted h5">
										<span id="cantVentasDashboard" class="text-success mr-2"></span>
									</div>
								</div>
								<div class="col-auto">
			                      <i class="fas fa-shopping-cart fa-2x text-success"></i>
			                    </div>
							</div>
						</div>
					</div>
				</a>
			</div>

			<?php	
			}
			?>

			<?php 
			if($_SESSION['v_cuotas']==1){
			?>

			<div class="col-xl-3 col-md-6 mb-4">
				<a class="a-dashboard" href="cuotas">
					<div class="card h-100">
						<div class="card-body">
							<div class="row align-items-center">
								<div class="col mr-2">
									<div class="h5 mb-0 font-weight-bold text-gray-800">Cuotas</div>
									<div class="mt-2 mb-0 text-muted">
										<span>Venc. Hoy</span>
										<span id="cantCuotasVencHoyDashboard" class="text-danger mr-2"></span>
									</div>
								</div>
								<div class="col-auto">
			                      <i class="fas fa-hand-holding-usd fa-2x text-warning"></i>
			                    </div>
							</div>
						</div>
					</div>
				</a>
			</div>

			<?php	
			}
			?>

			<?php
			if ($_SESSION['v_pagos']==1) {
			?>

			<div class="col-xl-3 col-md-6 mb-4">
				<a class="a-dashboard" href="pagos">
					<div class="card h-100">
						<div class="card-body">
							<div class="row align-items-center">
								<div class="col mr-2">
									<div class="h5 mb-0 font-weight-bold text-gray-800">Pagos</div>
									<div class="mt-2 mb-0 text-muted">
										<span>Hoy</span>
										<span id="cantPagosHoyDashboard" class="text-danger mr-2"></span>
									</div>
								</div>
								<div class="col-auto">
			                      <i class="fas fa-receipt fa-2x text-danger"></i>
			                    </div>
							</div>
						</div>
					</div>
				</a>
			</div>

			<?php
			}
			?>

			<?php
			if ($_SESSION['v_pedidos']==1) {
			?>

			<div class="col-xl-3 col-md-6 mb-4">
				<a class="a-dashboard" href="pedidos">
					<div class="card h-100">
						<div class="card-body">
							<div class="row align-items-center">
								<div class="col mr-2">
									<div class="h5 mb-0 font-weight-bold text-gray-800">Pedidos</div>
									<div class="mt-2 mb-0 text-muted">
										<span>Para Hoy</span>
										<span id="cantPedidosParaHoyDashboard" class="text-danger mr-2"></span>
									</div>
								</div>
								<div class="col-auto">
			                      <i class="fas fa-shipping-fast fa-2x text-primary"></i>
			                    </div>
							</div>
						</div>
					</div>
				</a>
			</div>

			<?php
			}
			?>

			<?php
			if ($_SESSION['v_reclamos']==1) {
			?>

			<div class="col-xl-3 col-md-6 mb-4">
				<a class="a-dashboard" href="reclamos">
					<div class="card h-100">
						<div class="card-body">
							<div class="row align-items-center">
								<div class="col mr-2">
									<div class="h5 mb-0 font-weight-bold text-gray-800">Reclamos</div>
									<div class="mt-2 mb-0 text-muted">
										<span>Pendientes</span>
										<span id="cantReclamosPendientesDashboard" class="text-danger mr-2"></span>
									</div>
								</div>
								<div class="col-auto">
			                      <i class="far fa-clipboard fa-2x text-danger"></i>
			                    </div>
							</div>
						</div>
					</div>
				</a>
			</div>

			<?php
			}
			?>

			


		</div>

		

	</div>
	<!-- End container fluid -->
</div>
<!-- End content -->

<?php
require ('footer.php');
?>

<script src="scripts/dashboard.js?ver=<?php echo $version?>"></script>

<?php
} //Fin llave
ob_end_flush(); 
?>