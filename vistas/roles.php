<?php
//activamos el almacenmiento de la sesion
ob_start();
session_start();

if(!isset($_SESSION['idUsuarioSisCob'])){
  header('Location: login');
}else{
  //Llave
  require ('header.php');
?>


	<!-- Container fluid -->
	<div class="container-fluid" id="container-wrapper">

		<?php
		if($_SESSION['v_acceso']==0){
			echo '<div class="d-sm-flex align-items-center justify-content-between mb-4">
					<h1 class="h3 mb-0 text-gray-800">Acceso denegado</h1>
				</div>';
		}else{
		// Contenido autorizado
		?>

		<div class="d-sm-flex align-items-center justify-content-between mb-4">
			<h1 class="h3 mb-0 text-gray-800"><i class="fas fa-user-cog"></i> Roles</h1>
		</div>

		<div class="row mb-3">
			<div class="col-lg-12">
				<div class="card mb-4">
					<div id="contenedor-cabecera" class="card-header py-3 d-flex flex-row align-items-center justify-content-between">
						<?php
						if ($_SESSION['new_acceso']==1) {
							echo "<button id='btnNuevo' class='btn btn-success'><i class='fas fa-plus-circle'></i> Nuevo</button>";
						}
						?>
						
					</div>
					<!-- Tabla -->
					<div id="listado" class="table-responsive p-3">
						<table id="tblListado" class="table align-items-center table-hover table-bordered" style="width: 100%;">
							<thead class="thead-light">
								<th style="width: 70px;">&nbsp;Opciones&nbsp;</th>
								<th>&nbsp;Nombre&nbsp;</th>
							</thead>
						</table>
					</div>
					<!-- End tabla -->

					<!-- Formulario -->
					<div id="formulario">
						<div class="card-body">
							<form id="form">

								<input type="hidden" id="idRol" name="idRol">

								<div class="form-group">
									<label><span class="text-danger">(*)</span> Nombre</label>
									<input type="text" class="form-control" id="nombre" name="nombre" 
									placeholder="Ingrese nombre" data-maxsize="60" required style="text-transform: capitalize;">
								</div>

								<div class="form-group">
									<label>Vistas</label>

									<div class="custom-control custom-switch">
										<input type="checkbox" class="custom-control-input" id="vAcceso" name="vAcceso">
										<label class="custom-control-label" for="vAcceso">Acceso</label>
									</div>

									<div class="custom-control custom-switch">
										<input type="checkbox" class="custom-control-input" id="vClientes" name="vClientes">
										<label class="custom-control-label" for="vClientes">Clientes</label>
									</div>

									<div class="custom-control custom-switch">
										<input type="checkbox" class="custom-control-input" id="vVentas" name="vVentas">
										<label class="custom-control-label" for="vVentas">Ventas</label>
									</div>

									<div class="custom-control custom-switch">
										<input type="checkbox" class="custom-control-input" id="vCuotas" name="vCuotas">
										<label class="custom-control-label" for="vCuotas">Cuotas</label>
									</div>

									<div class="custom-control custom-switch">
										<input type="checkbox" class="custom-control-input" id="vPagos" name="vPagos">
										<label class="custom-control-label" for="vPagos">Pagos</label>
									</div>

									<div class="custom-control custom-switch">
										<input type="checkbox" class="custom-control-input" id="vPedidos" name="vPedidos">
										<label class="custom-control-label" for="vPedidos">Pedidos</label>
									</div>


									<div class="custom-control custom-switch">
										<input type="checkbox" class="custom-control-input" id="vReclamos" name="vReclamos">
										<label class="custom-control-label" for="vReclamos">Reclamos</label>
									</div>

								</div>

								<div class="form-group">
									<label>Nuevos registros</label>

									<div class="custom-control custom-switch">
										<input type="checkbox" class="custom-control-input" id="newAcceso" name="newAcceso">
										<label class="custom-control-label" for="newAcceso">Acceso</label>
									</div>

									<div class="custom-control custom-switch">
										<input type="checkbox" class="custom-control-input" id="newClientes" name="newClientes">
										<label class="custom-control-label" for="newClientes">Clientes</label>
									</div>

									<div class="custom-control custom-switch">
										<input type="checkbox" class="custom-control-input" id="newVentas" name="newVentas">
										<label class="custom-control-label" for="newVentas">Ventas</label>
									</div>

									<div class="custom-control custom-switch">
										<input type="checkbox" class="custom-control-input" id="newPagos" name="newPagos">
										<label class="custom-control-label" for="newPagos">Pagos</label>
									</div>

									<div class="custom-control custom-switch">
										<input type="checkbox" class="custom-control-input" id="newPedidos" name="newPedidos">
										<label class="custom-control-label" for="newPedidos">Pedidos</label>
									</div>

									<div class="custom-control custom-switch">
										<input type="checkbox" class="custom-control-input" id="newReclamos" name="newReclamos">
										<label class="custom-control-label" for="newReclamos">Reclamos</label>
									</div>

								</div>

								<div class="form-group">
									<label>Alteraciones (Editar - Eliminar)</label>

									<div class="custom-control custom-switch">
										<input type="checkbox" class="custom-control-input" id="altAcceso" name="altAcceso">
										<label class="custom-control-label" for="altAcceso">Acceso</label>
									</div>

									<div class="custom-control custom-switch">
										<input type="checkbox" class="custom-control-input" id="altClientes" name="altClientes">
										<label class="custom-control-label" for="altClientes">Clientes</label>
									</div>

									<div class="custom-control custom-switch">
										<input type="checkbox" class="custom-control-input" id="altVentas" name="altVentas">
										<label class="custom-control-label" for="altVentas">Ventas</label>
									</div>

									<div class="custom-control custom-switch">
										<input type="checkbox" class="custom-control-input" id="altPagos" name="altPagos">
										<label class="custom-control-label" for="altPagos">Pagos</label>
									</div>

									<div class="custom-control custom-switch">
										<input type="checkbox" class="custom-control-input" id="altCuotas" name="altCuotas">
										<label class="custom-control-label" for="altCuotas">Cuotas</label>
									</div>

									<div class="custom-control custom-switch">
										<input type="checkbox" class="custom-control-input" id="altConfiguracion" name="altConfiguracion">
										<label class="custom-control-label" for="altConfiguracion">Configuración</label>
									</div>

									<div class="custom-control custom-switch">
										<input type="checkbox" class="custom-control-input" id="altPedidos" name="altPedidos">
										<label class="custom-control-label" for="altPedidos">Pedidos</label>
									</div>

									<div class="custom-control custom-switch">
										<input type="checkbox" class="custom-control-input" id="altReclamos" name="altReclamos">
										<label class="custom-control-label" for="altReclamos">Reclamos</label>
									</div>

								</div>
								
								<div class="mt-4 mb-4 float-right">
									<button type="button" id="btnCancelar" class="btn btn-danger">Cancelar</button>
									<button type="submit" id="btnGuardar" class="btn btn-primary">Guardar</button>
								</div>
								
							</form>
						</div>
					</div>
					<!-- End formulario -->
				</div>
			</div>
		</div>
		<?php
		} 	
		// Fin contenido autorizado
		?>
	</div>	
	<!-- End Container fluid -->
</div>
<!-- End Content -->
<?php
require ('footer.php');
?>

<script src="scripts/rol.js?ver=<?php echo $version?>"></script>

<?php
} //Fin llave
ob_end_flush(); 
?>