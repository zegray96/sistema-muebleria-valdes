var tabla;
function init(){
	$('#tituloPagina').text('Sistema de Cobros - Pagos');
	$("#wrapper").show(); 
	$("#preload").hide(); 
	listar_pagos_hoy();
	$('#textFiltro').text('Pagos de hoy');
	$('#contenedor-cabecera').css("cssText", "display:block !important");
	listar_por_cobrador_cargar_datos();
}

$('#listarPor').on('change', function() {
	if ($('#listarPor').val()=="Pagos de hoy"){
		listar_pagos_hoy();
		$('#listarPor').val("");
		$('#listarPor').selectpicker('refresh');
		$('#textFiltro').text('Pagos de hoy');
		$('#listarPorCobrador option[value=""]').prop("selected", true);
		$('#listarPorCobrador').selectpicker('refresh');
	}else{
		if ($('#listarPor').val()=="Fechas") {
			$('#seleccionarRangoFechas').modal('show');
			$('#listarPor').val("");
			$('#listarPor').selectpicker('refresh');
			$('#fechaIniListar').datepicker("setDate", '');
			$('#fechaFinListar').datepicker("setDate", '');
		}
	}
});

// Rango fechas
$('#div-rango-fechas .input-daterange').datepicker({        
	format: 'dd/mm/yyyy',        
	autoclose: true,     
	todayHighlight: true,   
	todayBtn: 'linked',
	language: 'es',
});  

$('#btnListarPorFecha').click(function() {
	listar_por_fecha();
});

$('#btnCancelarFecha').click(function() {
	$('#seleccionarRangoFechas').modal('hide');
});



function listar(){
	tabla=$('#tblListado').dataTable({
    	//utilizar en caso de que no haya server side
		"aProccessing": true, //Activamos el procesamiento de datatables
		"aServerSide": true, //Paginacion y filtrado relizados por el servidor
		//
		"searching": true, //no me muestra el boton buscar
        "language":{
        		"url": "../assets/json/Spanish.json"    
        },
        "scrollY": '48vh', //tamaño de barra de desplazamiento
		"scrollX": true, //muestra el scroll x
        "lengthMenu": [[25, 100, 1000], [25, 100, 1000]],
		dom: 'Bflrtip', //definimos los elementos del control de la tabla
		buttons:[
			{
				extend: 'excelHtml5',
				exportOptions: {
					columns: [1,2,3,4,5]
				},
				text: '<i class="fas fa-file-excel"></i>',
				className: 'btn btn-success',
				titleAttr: 'Exportar a Excel',
				
			},
			

			{
				extend: 'csvHtml5',
				exportOptions: {
					columns: [1,2,3,4,5]
				},
				text: '<i class="fa fa-file-csv"></i>',
				className: 'btn btn-warning',
				titleAttr: 'Exportar a CSV',
				
			},


			{
				extend: 'print',
				exportOptions: {
					columns: [1,2,3,4,5]
				},
				text: '<i class="fa fa-print"></i>',
				className: 'btn btn-info',
				titleAttr: 'Imprimir',
				
			},

			

		], 

		"ajax":{
			url:'../ajax/pago.php?op=listar',
			type: 'get',
			dataType: 'json', 
			error: function(e){
				console.log(e.responseText);
			}
		},

		"bDestroy": true,
		"iDisplayLength":25, //cada cuantos registros realizamos la paginacion
		"order":[[1,"desc"]] //para ordenar los registros
    }).DataTable();
}

function listar_pagos_hoy(){
	tabla=$('#tblListado').dataTable({
    	//utilizar en caso de que no haya server side
		"aProccessing": true, //Activamos el procesamiento de datatables
		"aServerSide": true, //Paginacion y filtrado relizados por el servidor
		//
		"searching": true, //no me muestra el boton buscar
        "language":{
        		"url": "../assets/json/Spanish.json"    
        },
        "scrollY": '48vh', //tamaño de barra de desplazamiento
		"scrollX": true, //muestra el scroll x
        "lengthMenu": [[25, 100, 1000], [25, 100, 1000]],
		dom: 'Bflrtip', //definimos los elementos del control de la tabla
		buttons:[
			{
					extend: 'excelHtml5',
					exportOptions: {
						columns: [1,2,3,4,5]
					},
					text: '<i class="fas fa-file-excel"></i>',
					className: 'btn btn-success',
					titleAttr: 'Exportar a Excel',
					
				},
				

				{
					extend: 'csvHtml5',
					exportOptions: {
						columns: [1,2,3,4,5]
					},
					text: '<i class="fa fa-file-csv"></i>',
					className: 'btn btn-warning',
					titleAttr: 'Exportar a CSV',
					
				},


				{
					extend: 'print',
					exportOptions: {
						columns: [1,2,3,4,5]
					},
					text: '<i class="fa fa-print"></i>',
					className: 'btn btn-info',
					titleAttr: 'Imprimir',
					
				},

			

		], 

		"ajax":{
			url:'../ajax/pago.php?op=listar_pagos_hoy',
			type: 'get',
			dataType: 'json', 
			error: function(e){
				console.log(e.responseText);
			}
		},

		"bDestroy": true,
		"iDisplayLength":25, //cada cuantos registros realizamos la paginacion
		"order":[[1,"desc"]] //para ordenar los registros
    }).DataTable();
}

function listar_por_fecha(){
	$('#listarPorCobrador option[value=""]').prop("selected", true);
	$('#listarPorCobrador').selectpicker('refresh');
	
	var fechaIni=$('#fechaIniListar').val();
	var fechaFin=$('#fechaFinListar').val();

	if (fechaIni=="" || fechaFin=="") {
			Swal.fire({
				icon:'error',
				title:'¡Seleccione fechas!',
				allowOutsideClick: false
			});

	}else{
		tabla=$('#tblListado').dataTable({
	    	//utilizar en caso de que no haya server side
			"aProccessing": true, //Activamos el procesamiento de datatables
			"aServerSide": true, //Paginacion y filtrado relizados por el servidor
			//
			"searching": true, //no me muestra el boton buscar
	        "language":{
	        		"url": "../assets/json/Spanish.json"    
	        },
	        "scrollY": '48vh', //tamaño de barra de desplazamiento
			"scrollX": true, //muestra el scroll x
	        "lengthMenu": [[25, 100, 1000], [25, 100, 1000]],
			dom: 'Bflrtip', //definimos los elementos del control de la tabla
			buttons:[
				{
					extend: 'excelHtml5',
					exportOptions: {
						columns: [1,2,3,4,5]
					},
					text: '<i class="fas fa-file-excel"></i>',
					className: 'btn btn-success',
					titleAttr: 'Exportar a Excel',
					
				},
				

				{
					extend: 'csvHtml5',
					exportOptions: {
						columns: [1,2,3,4,5]
					},
					text: '<i class="fa fa-file-csv"></i>',
					className: 'btn btn-warning',
					titleAttr: 'Exportar a CSV',
					
				},


				{
					extend: 'print',
					exportOptions: {
						columns: [1,2,3,4,5]
					},
					text: '<i class="fa fa-print"></i>',
					className: 'btn btn-info',
					titleAttr: 'Imprimir',
					
				},

				

			], 

			"ajax":{
				url:'../ajax/pago.php?op=listar_por_fecha',
				data: {'fechaIni': fechaIni, 'fechaFin': fechaFin},
				type: 'get',
				dataType: 'json', 
				error: function(e){
					console.log(e.responseText);
				}
			},

			"bDestroy": true,
			"iDisplayLength":25, //cada cuantos registros realizamos la paginacion
			"order":[[1,"desc"]] //para ordenar los registros
	    }).DataTable();
	    $('#seleccionarRangoFechas').modal('hide');
	    $('#textFiltro').text(fechaIni+' - '+fechaFin);
	}	

	
}

function listar_por_cobrador_cargar_datos(){
	//cargamos los items al select
	$.post("../ajax/venta.php?op=listar_por_cobrador_cargar_datos", function(r){ //parametro r son las opciones que nos devuelve el selectUsuarios
		$("#listarPorCobrador").html(r); //aca va el id del elemento del form donde vamos a almacenar
		$('#listarPorCobrador').selectpicker('refresh'); //refresco el select
	});		
}

$('#listarPorCobrador').on('change', function () {
    tabla.columns(5).search(this.value).draw();
});

function imprimir_comprobante(id){
	if (id=="") {
		Swal.fire({
			title: '¡Ocurrió un error!',
			icon: 'error',
		});
	}else{
		window.open('../reportes/comprobante-pago?id='+id, '_blank');
	}
}


init();