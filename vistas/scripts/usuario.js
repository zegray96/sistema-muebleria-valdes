var tabla;
function init(){
	$('#tituloPagina').text('Sistema de Cobros - Usuarios');
	listar();
	mostrar_form(false);
	$("#wrapper").show(); 
	$("#preload").hide(); 
	cargar_roles();
}

$('#btnNuevo').click(function() {
	mostrar_form(true);
});

$('#btnCancelar').click(function() {
	mostrar_form(false);
	cargar_roles();
});

$('#btnCancelarModificarClave').click(function() {
	$('#modificarClave').modal('hide');
	$('#claveModificar').val("");
	$('#repetirClaveModificar').val("");
});

$("#form").on("submit", function(e){ 
	nuevo_editar(e);
});

$("#formModificarClave").on("submit", function(e){ 
	modificar_clave(e);
});

//colocar la primer letra de palabra en mayusuclas 
function capitalizar(str) {
    return str.replace(/\w\S*/g, function(txt){
        return txt.charAt(0).toUpperCase() + txt.substr(1).toLowerCase();
    });
}

$('#apellidoNombre').blur(function() {
	$('#apellidoNombre').val(capitalizar($('#apellidoNombre').val()));
});

function listar(){
	tabla=$('#tblListado').dataTable({
    	//utilizar en caso de que no haya server side
		"aProccessing": true, //Activamos el procesamiento de datatables
		"aServerSide": true, //Paginacion y filtrado relizados por el servidor
		//
		"searching": true, //no me muestra el boton buscar
        "language":{
        		"url": "../assets/json/Spanish.json"    
        },
        "lengthMenu": [[5, 100, 1000], [5, 100, 1000]],
		dom: 'Bflrtip', //definimos los elementos del control de la tabla
		buttons:[
			{
				extend: 'excelHtml5',
				exportOptions: {
					columns: [1,2,3,4]
				},
				text: '<i class="fas fa-file-excel"></i>',
				className: 'btn btn-success',
				titleAttr: 'Exportar a Excel',
				
			},
			

			{
				extend: 'csvHtml5',
				exportOptions: {
					columns: [1,2,3,4]
				},
				text: '<i class="fa fa-file-csv"></i>',
				className: 'btn btn-warning',
				titleAttr: 'Exportar a CSV',
				
			},


			{
				extend: 'print',
				exportOptions: {
					columns: [1,2,3,4]
				},
				text: '<i class="fa fa-print"></i>',
				className: 'btn btn-info',
				titleAttr: 'Imprimir',
				
			},

			

		], 

		"ajax":{
			url:'../ajax/usuario.php?op=listar',
			type: 'get',
			dataType: 'json', 
			error: function(e){
				console.log(e.responseText);
			}
		},

		"bDestroy": true,
		"iDisplayLength":5, //cada cuantos registros realizamos la paginacion
		"order":[[1,"asc"]] //para ordenar los registros
    }).DataTable();
}


function mostrar_form(mostrar){
	limpiar();
	if(mostrar){
		$('#listado').hide();
		$('#formulario').show();
		$('#contenedor-cabecera').css("cssText", "display:none !important");
		$('#btnGuardar').prop("disabled",false);
		$('#clave').prop('readonly',false);
		$('#linkModificarClave').hide();
	}else{
		$('#listado').show();
		$('#formulario').hide();
		$('#contenedor-cabecera').css("cssText", "display:block !important");

	}
}

function nuevo_editar(e){
	$('#cargandoModal').modal('show');
	
	e.preventDefault(); //no se activara la accion predeterminada del evento, osea del submit, se va hacer lo que yo le digo
	var formData = new FormData($('#form')[0]); //guardo todos los datos del form en formData
	$.ajax({
		url:'../ajax/usuario.php?op=nuevo_editar',//lugar a donde se envia los datos obtenidos del formulario
		type: "POST",
		data: formData, //estos son los datos que envio
		contentType:false,
		processData:false,

		success: function(datos){ 
			 $('#cargandoModal').fadeOut(300,function(){
                $('#cargandoModal').modal('hide');
             });

			if(datos=="¡Registro creado con exito!" || datos=="¡Registro editado con exito!"){
				Swal.fire({
					icon: 'success',
					title: datos,
					allowOutsideClick: false
				});
				mostrar_form(false);
				tabla.ajax.reload(); 
			}else{
				Swal.fire({
					icon: 'error',
					title: datos,
					allowOutsideClick: false
				});
			}
		}		
	});
}


function modificar_clave(e){
	$('#cargandoModal').modal('show');
	
	e.preventDefault(); //no se activara la accion predeterminada del evento, osea del submit, se va hacer lo que yo le digo
	var formData = new FormData($('#formModificarClave')[0]); //guardo todos los datos del form en formData
	$.ajax({
		url:'../ajax/usuario.php?op=modificar_clave',//lugar a donde se envia los datos obtenidos del formulario
		type: "POST",
		data: formData, //estos son los datos que envio
		contentType:false,
		processData:false,

		success: function(datos){ 
			 $('#cargandoModal').fadeOut(300,function(){
                $('#cargandoModal').modal('hide');
             });

			if(datos=="¡Clave modificada con exito!"){
				Swal.fire({
					icon: 'success',
					title: datos,
					allowOutsideClick: false
				});
				$('#modificarClave').modal('hide');
				$('#claveModificar').val("");
				$('#repetirClaveModificar').val("");
			}else{
				Swal.fire({
					icon: 'error',
					title: datos,
					allowOutsideClick: false
				});
			}
		}		
	});
}

function mostrar(id){
	$('#cargandoModal').modal('show');
	$.post("../ajax/usuario.php?op=mostrar", {idUsuario: id}, function(data, status){
		try {
        /* Si el JSON está mal formado se generará una excepción */
        data = JSON.parse(data);
        if (data.error == true) {
          /* Si hemos enviado por JSON un error, lo notificamos */
          console.log('ERROR detectado:', data);
          return;
        }
        /* Trabajamos habitualmente con la respuesta */
        $('#cargandoModal').fadeOut(300,function(){
        	$('#cargandoModal').modal('hide');
        });
		mostrar_form(true);
		$('#idUsuario').val(data.id_usuario);
		$('#apellidoNombre').val(data.apellido_nombre);
		$('#usuario').val(data.usuario);
		$('#linkModificarClave').show();
		$('#clave').val("*******");
		$('#clave').prop('readonly',true);
		$('#idRol').val(data.id_rol);
		$('#idRol').selectpicker('refresh');
		$('#estado').val(data.estado);
		$('#estado').selectpicker('refresh');

		$('#idUsuarioModificar').val(data.id_usuario);
      } catch (error) {
        /* Si el JSON está mal, notificamos su contenido */
        console.log('ERROR. Recibido:', data);
      }
	})
}

function limpiar(){
	$('#idUsuario').val("");
	$('#apellidoNombre').val("");
	$('#usuario').val("");
	$('#clave').val("");
	$('#idRol').val("");
	$('#estado').val("");
	$('#estado').selectpicker('refresh');
	$('#idUsuarioModificar').val("");
	$('#claveModificar').val("");
	$('#repetirClaveModificar').val("");
}

function cargar_roles(){
	$.post("../ajax/usuario.php?op=cargar_roles", function(r){ 
		$("#idRol").html(r);
		$('#idRol').selectpicker('refresh'); 
	});	
}




init();