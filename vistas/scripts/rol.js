var tabla;
function init(){
	$('#tituloPagina').text('Sistema de Corbos - Roles');
	listar();
	mostrar_form(false);
	$("#wrapper").show(); 
	$("#preload").hide(); 

}

$('#btnNuevo').click(function() {
	mostrar_form(true);
});

$('#btnCancelar').click(function() {
	mostrar_form(false);
});

$("#form").on("submit", function(e){ 
	nuevo_editar(e);
});

//colocar la primer letra de palabra en mayusuclas 
function capitalizar(str) {
    return str.replace(/\w\S*/g, function(txt){
        return txt.charAt(0).toUpperCase() + txt.substr(1).toLowerCase();
    });
}

$('#nombre').blur(function() {
	$('#nombre').val(capitalizar($('#nombre').val()));
});

function listar(){
	tabla=$('#tblListado').dataTable({
    	//utilizar en caso de que no haya server side
		"aProccessing": true, //Activamos el procesamiento de datatables
		"aServerSide": true, //Paginacion y filtrado relizados por el servidor
		//
		"searching": true, //no me muestra el boton buscar
        "language":{
        		"url": "../assets/json/Spanish.json"    
        },
        "lengthMenu": [[5, 100, 1000], [5, 100, 1000]],
		dom: 'Bflrtip', //definimos los elementos del control de la tabla
		buttons:[
			{
				extend: 'excelHtml5',
				exportOptions: {
					columns: [1]
				},
				text: '<i class="fas fa-file-excel"></i>',
				className: 'btn btn-success',
				titleAttr: 'Exportar a Excel',
				
			},
			

			{
				extend: 'csvHtml5',
				exportOptions: {
					columns: [1]
				},
				text: '<i class="fa fa-file-csv"></i>',
				className: 'btn btn-warning',
				titleAttr: 'Exportar a CSV',
				
			},


			{
				extend: 'print',
				exportOptions: {
					columns: [1]
				},
				text: '<i class="fa fa-print"></i>',
				className: 'btn btn-info',
				titleAttr: 'Imprimir',
				
			},

			

		], 

		"ajax":{
			url:'../ajax/rol.php?op=listar',
			type: 'get',
			dataType: 'json', 
			error: function(e){
				console.log(e.responseText);
			}
		},

		"bDestroy": true,
		"iDisplayLength":5, //cada cuantos registros realizamos la paginacion
		"order":[[1,"asc"]] //para ordenar los registros
    }).DataTable();
}


function mostrar_form(mostrar){
	limpiar();
	if(mostrar){
		$('#listado').hide();
		$('#formulario').show();
		$('#contenedor-cabecera').css("cssText", "display:none !important");
		$('#btnGuardar').prop("disabled",false);
	}else{
		$('#listado').show();
		$('#formulario').hide();
		$('#contenedor-cabecera').css("cssText", "display:block !important");

	}
}

function nuevo_editar(e){
	$('#cargandoModal').modal('show');
	
	e.preventDefault(); //no se activara la accion predeterminada del evento, osea del submit, se va hacer lo que yo le digo
	var formData = new FormData($('#form')[0]); //guardo todos los datos del form en formData
	$.ajax({
		url:'../ajax/rol.php?op=nuevo_editar',//lugar a donde se envia los datos obtenidos del formulario
		type: "POST",
		data: formData, //estos son los datos que envio
		contentType:false,
		processData:false,

		success: function(datos){ 
			 $('#cargandoModal').fadeOut(300,function(){
                $('#cargandoModal').modal('hide');
             });

			if(datos=="¡Registro creado con exito!" || datos=="¡Registro editado con exito!"){
				Swal.fire({
					icon: 'success',
					title: datos,
					allowOutsideClick: false
				});
				mostrar_form(false);
				tabla.ajax.reload(); 
			}else{
				Swal.fire({
					icon: 'error',
					title: datos,
					allowOutsideClick: false
				});
			}
		}		
	});
}

function mostrar(id){
	$('#cargandoModal').modal('show');
	$.post("../ajax/rol.php?op=mostrar", {idRol: id}, function(data, status){
		try {
        /* Si el JSON está mal formado se generará una excepción */
        data = JSON.parse(data);
        if (data.error == true) {
          /* Si hemos enviado por JSON un error, lo notificamos */
          console.log('ERROR detectado:', data);
          return;
        }
        /* Trabajamos habitualmente con la respuesta */
        $('#cargandoModal').fadeOut(300,function(){
        	$('#cargandoModal').modal('hide');
        });
		mostrar_form(true);
		$('#idRol').val(data.id_rol);
		$('#nombre').val(data.nombre);
		
		if(data.v_acceso==1){
			$('#vAcceso').prop('checked', true);
		}else{
			$('#vAcceso').prop('checked', false);
		}

		if(data.v_clientes==1){
			$('#vClientes').prop('checked', true);
		}else{
			$('#vClientes').prop('checked', false);
		}

		if(data.v_ventas==1){
			$('#vVentas').prop('checked', true);
		}else{
			$('#vVentas').prop('checked', false);
		}

		if(data.v_cuotas==1){
			$('#vCuotas').prop('checked', true);
		}else{
			$('#vCuotas').prop('checked', false);
		}

		if(data.v_pagos==1){
			$('#vPagos').prop('checked', true);
		}else{
			$('#vPagos').prop('checked', false);
		}

		if(data.v_pedidos==1){
			$('#vPedidos').prop('checked', true);
		}else{
			$('#vPedidos').prop('checked', false);
		}

		if(data.v_reclamos==1){
			$('#vReclamos').prop('checked', true);
		}else{
			$('#vReclamos').prop('checked', false);
		}

		if(data.new_acceso==1){
			$('#newAcceso').prop('checked', true);
		}else{
			$('#newAcceso').prop('checked', false);
		}

		if(data.new_clientes==1){
			$('#newClientes').prop('checked', true);
		}else{
			$('#newClientes').prop('checked', false);
		}

		if(data.new_ventas==1){
			$('#newVentas').prop('checked', true);
		}else{
			$('#newVentas').prop('checked', false);
		}

		if(data.new_pagos==1){
			$('#newPagos').prop('checked', true);
		}else{
			$('#newPagos').prop('checked', false);
		}

		if(data.new_pedidos==1){
			$('#newPedidos').prop('checked', true);
		}else{
			$('#newPedidos').prop('checked', false);
		}

		if(data.new_reclamos==1){
			$('#newReclamos').prop('checked', true);
		}else{
			$('#newReclamos').prop('checked', false);
		}

		if(data.alt_acceso==1){
			$('#altAcceso').prop('checked', true);
		}else{
			$('#altAcceso').prop('checked', false);
		}

		if(data.alt_clientes==1){
			$('#altClientes').prop('checked', true);
		}else{
			$('#altClientes').prop('checked', false);
		}

		if(data.alt_ventas==1){
			$('#altVentas').prop('checked', true);
		}else{
			$('#altVentas').prop('checked', false);
		}

		if(data.alt_cuotas==1){
			$('#altCuotas').prop('checked', true);
		}else{
			$('#altCuotas').prop('checked', false);
		}

		if(data.alt_pagos==1){
			$('#altPagos').prop('checked', true);
		}else{
			$('#altPagos').prop('checked', false);
		}

		if(data.alt_configuracion==1){
			$('#altConfiguracion').prop('checked', true);
		}else{
			$('#altConfiguracion').prop('checked', false);
		}

		if(data.alt_pedidos==1){
			$('#altPedidos').prop('checked', true);
		}else{
			$('#altPedidos').prop('checked', false);
		}

		if(data.alt_reclamos==1){
			$('#altReclamos').prop('checked', true);
		}else{
			$('#altReclamos').prop('checked', false);
		}
      } catch (error) {
        /* Si el JSON está mal, notificamos su contenido */
        console.log('ERROR. Recibido:', data);
      }
	})
}

function limpiar(){
	$('#idRol').val("");
	$('#nombre').val("");
	$('#vAcceso').prop("checked",false);
	$('#vClientes').prop("checked",false);
	$('#vVentas').prop("checked",false);
	$('#vCuotas').prop("checked",false);
	$('#vPagos').prop("checked",false);
	$('#newAcceso').prop("checked",false);
	$('#newClientes').prop("checked",false);
	$('#newVentas').prop("checked",false);
	$('#newPagos').prop("checked",false);
	$('#altAcceso').prop("checked",false);
	$('#altClientes').prop("checked",false);
	$('#altVentas').prop("checked",false);
	$('#altPagos').prop("checked",false);
	$('#altCuotas').prop("checked",false);
	$('#altConfiguracion').prop("checked",false);
}


init();