var tabla;
function init(){
	$('#tituloPagina').text('Sistema de Cobros - Configuración');
	$("#wrapper").show(); 
	$("#preload").hide(); 
	mostrar();
}

$('#btnCancelar').click(function() {
	$(location).attr("href","dashboard");
});


$("#form").on("submit", function(e){ 
	editar(e);
});


//colocar la primer letra de palabra en mayusuclas 
function capitalizar(elemId) {
	var txt = $("#" + elemId).val().toLowerCase();
	$("#" + elemId).val(txt.replace(/^(.)|\s(.)/g, function($1) {
		return $1.toUpperCase(); }));
}


function validar_mostrar_logo(input) {
	var archivo = $("#logoEmpresa").val();
	var extensiones = archivo.substring(archivo.lastIndexOf("."));
	if(extensiones != ".png"){
		Swal.fire({
			icon: 'error',
			title: "El archivo de tipo " + extensiones + " no es válido",
		});
		$("#logoEmpresa").val(null);
	}else{
		var reader = new FileReader();
	    reader.onload = function(e) {
	      // Asignamos el atributo src a la tag de imagen
	      $('#logoEmpresaImg').attr('src', e.target.result);
	    }
	    reader.readAsDataURL(input.files[0]);
	}
}

$('#nombreEmpresa').keyup(function() {
	capitalizar('nombreEmpresa');
});


$('#logoEmpresa').on('change', function() {
	validar_mostrar_logo(this);
});

$('#iconoEmpresa').on('change', function() {
	validar_mostrar_icono(this);
});



function editar(e){
	$('#cargandoModal').modal('show');
	
	e.preventDefault(); //no se activara la accion predeterminada del evento, osea del submit, se va hacer lo que yo le digo
	var formData = new FormData($('#form')[0]); //guardo todos los datos del form en formData
	$.ajax({
		url:'../ajax/configuracion.php?op=editar',//lugar a donde se envia los datos obtenidos del formulario
		type: "POST",
		data: formData, //estos son los datos que envio
		contentType:false,
		processData:false,

		success: function(datos){ 
			 $('#cargandoModal').fadeOut(300,function(){
                $('#cargandoModal').modal('hide');
             });

			if(datos=="¡Registro editado con exito! Los cambios se reflejarán luego de cerrar sesión..." || datos=="¡Registro editado con exito!"){
				Swal.fire({
					icon: 'success',
					title: datos,
					allowOutsideClick: false
				});
				mostrar();
				$('#nombreEmpresaHeader').text($('#nombreEmpresa').val());
				$('#logoEmpresa').val(null);
				$('#iconoEmpresa').val(null);
			}else{
				Swal.fire({
					icon: 'error',
					title: datos,
					allowOutsideClick: false
				});
			}
		}		
	});
}


function mostrar(){
	$('#cargandoModal').modal('show');
	$.post("../ajax/configuracion.php?op=mostrar", function(data, status){
		try {
        /* Si el JSON está mal formado se generará una excepción */
        data = JSON.parse(data);
        if (data.error == true) {
          /* Si hemos enviado por JSON un error, lo notificamos */
          console.log('ERROR detectado:', data);
          return;
        }
        /* Trabajamos habitualmente con la respuesta */
        $('#cargandoModal').fadeOut(300,function(){
        	$('#cargandoModal').modal('hide');
        });
		$('#nombreEmpresa').val(data.nombre_empresa);
		$('#descripcionCortaEmpresa').val(data.descripcion_corta_empresa);
		$('#telefono').val(data.telefono);
		$('#logoEmpresaImg').attr("src","../assets/img/logo-empresa.png"+"?"+Math.random());
		
      } catch (error) {
        /* Si el JSON está mal, notificamos su contenido */
        console.log('ERROR. Recibido:', data);
      }
	})
}







init();