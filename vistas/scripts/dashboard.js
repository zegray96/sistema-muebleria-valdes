function init(){
	$('#tituloPagina').text('Sistema de Cobros - Dashboard');
	cant_clientes_dashboard();
	cant_ventas_dashboard();
	cant_cuotas_venc_hoy_dashboard();
	cant_pagos_hoy_dashboard();
	cant_pedidos_para_hoy_dashboard();
	cant_reclamos_pendientes_dashboard();
	$("#wrapper").show(); 
	$("#preload").hide(); 
}

function cant_clientes_dashboard(){
	$.post("../ajax/cliente.php?op=cant_clientes_dashboard", function(data, status){
		$('#cantClientesDashboard').text(data);	
	});
}

function cant_ventas_dashboard(){
	$.post("../ajax/venta.php?op=cant_ventas_dashboard", function(data, status){
		$('#cantVentasDashboard').text(data);	
	});
}

function cant_cuotas_venc_hoy_dashboard(){
	$.post("../ajax/cuota.php?op=cant_cuotas_venc_hoy_dashboard", function(data, status){
		$('#cantCuotasVencHoyDashboard').text(data);	
	});
}

function cant_pagos_hoy_dashboard(){
	$.post("../ajax/pago.php?op=cant_pagos_hoy_dashboard", function(data, status){
		$('#cantPagosHoyDashboard').text(data);	
	});
}

function cant_pedidos_para_hoy_dashboard(){
	$.post("../ajax/pedido.php?op=cant_pedidos_para_hoy_dashboard", function(data, status){
		$('#cantPedidosParaHoyDashboard').text(data);	
	});
}

function cant_reclamos_pendientes_dashboard(){
	$.post("../ajax/reclamo.php?op=cant_reclamos_pendientes_dashboard", function(data, status){
		$('#cantReclamosPendientesDashboard').text(data);	
	});
}


init();