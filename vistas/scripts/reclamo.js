function init(){
	$('#tituloPagina').text('Sistema de Cobros - Reclamos');
	$("#wrapper").show(); 
	$("#preload").hide(); 
	listar();
	mostrar_form(false);
}

$('#btnNuevo').click(function() {
	mostrar_form(true);
});

$('#btnCancelar').click(function() {
	mostrar_form(false);
});

$("#form").on("submit", function(e){ 
	nuevo_editar(e);
});

$('#btnBuscarCliente').click(function() {
	$('#buscarCliente').modal('show');
});

// formato fecha
function format_fecha(elEvento,idElem){
	var evento = elEvento || window.event;
	if(evento.keyCode == 8){
	} else {
		var fecha = document.getElementById(idElem);
		if(fecha.value.length == 2 || fecha.value.length == 5){
			fecha.value += "/";
		}
	}
}

//colocar la primer letra de palabra en mayusuclas 
function capitalizar(str) {
    return str.replace(/\w\S*/g, function(txt){
        return txt.charAt(0).toUpperCase() + txt.substr(1).toLowerCase();
    });
}

$("#fechaReclamo").keydown(function(event) {
  format_fecha(event,'fechaReclamo');
});

function limpiar(){
	$('#estado').val('');
	$('#estado').selectpicker('refresh');
	$('#fechaReclamo').val('');
	$('#apellidoNombre').val('');
	$('#dni').val('');
	$('#idCliente').val('');
	$('#motivo').val('');
	$('#cantMot').text('0');
}


function mostrar_form(mostrar){
	limpiar();
	if(mostrar){
		$('#fechaReclamo').datepicker({
			format: 'dd/mm/yyyy',
			todayBtn: 'linked',
			todayHighlight: true,
			autoclose: true,
			language: 'es',
		}).datepicker("setDate", new Date());
		$('#listado').hide();
		$('#formulario').show();
		$('#contenedor-cabecera').css("cssText", "display:none !important");
		$('#btnGuardar').prop("disabled",false);
		$('#div-ultima-mod').hide();
	}else{
		$('#listado').show();
		$('#formulario').hide();
		$('#contenedor-cabecera').css("cssText", "display:block !important");

	}
}


function listar(){
	tabla=$('#tblListado').dataTable({
    	//utilizar en caso de que no haya server side
		"aProccessing": true, //Activamos el procesamiento de datatables
		"aServerSide": true, //Paginacion y filtrado relizados por el servidor
		//
		"searching": true, //no me muestra el boton buscar
        "language":{
        		"url": "../assets/json/Spanish.json"    
        },
        "scrollY": '48vh', //tamaño de barra de desplazamiento
		"scrollX": true, //muestra el scroll x
        "lengthMenu": [[25, 100, 1000], [25, 100, 1000]],
		dom: 'Bflrtip', //definimos los elementos del control de la tabla
		buttons:[
			{
				extend: 'excelHtml5',
				exportOptions: {
					columns: [1,2,3,4,5]
				},
				text: '<i class="fas fa-file-excel"></i>',
				className: 'btn btn-success',
				titleAttr: 'Exportar a Excel',
				
			},
			

			{
				extend: 'csvHtml5',
				exportOptions: {
					columns: [1,2,3,4,5]
				},
				text: '<i class="fa fa-file-csv"></i>',
				className: 'btn btn-warning',
				titleAttr: 'Exportar a CSV',
				
			},


			{
				extend: 'print',
				exportOptions: {
					columns: [1,2,3,4,5]
				},
				text: '<i class="fa fa-print"></i>',
				className: 'btn btn-info',
				titleAttr: 'Imprimir',
				
			},

			

		], 

		"ajax":{
			url:'../ajax/reclamo.php?op=listar',
			type: 'get',
			dataType: 'json', 
			error: function(e){
				console.log(e.responseText);
			}
		},

		"bDestroy": true,
		"iDisplayLength":25, //cada cuantos registros realizamos la paginacion
		"order":[[1,"desc"]] //para ordenar los registros
    }).DataTable();
}


// Al abrir modal buscar clientes
$('#buscarCliente').on('shown.bs.modal', function () {
   tablaClientes = $('#tblClientes').dataTable({
   		//utilizar en caso de que no haya server side
		"aProccessing": true, //Activamos el procesamiento de datatables
		"aServerSide": true, //Paginacion y filtrado relizados por el servidor
		//
		"searching": true, //no me muestra el boton buscar
        "language":{
        		"url": "../assets/json/Spanish.json"    
        },
        "scrollY": '48vh', //tamaño de barra de desplazamiento
		"scrollX": true, //muestra el scroll x
        "lengthMenu": [[25, 100, 1000], [25, 100, 1000]],
       	dom: 'frtip', //definimos los elementos del control de la tabla

		"ajax":{
			url:'../ajax/reclamo.php?op=listar_clientes',
			type: 'get',
			dataType: 'json', 
			error: function(e){
				console.log(e.responseText);
			}
		},

		"bDestroy": true,
		"iDisplayLength": 25, //cada cuantos registros realizamos la paginacion
		"order":[[1,"asc"]] //para ordenar los registros
   }).DataTable();
});

// al ocultar buscar cliente
$('#buscarCliente').on('hidden.bs.modal', function (e) {
  var table = $('#tblClientes').DataTable();
  table.clear().draw();
});


// agregar cliente
function agregar_cliente(idCliente,dni,apellidoNombre){
	$('#idCliente').val(idCliente);
	$('#dni').val(dni);
	$('#apellidoNombre').val(apellidoNombre);
	$('#buscarCliente').modal('hide');
}

function nuevo_editar(e){
	$('#cargandoModal').show();
	e.preventDefault();
	var formData = new FormData($('#form')[0]);
	$.ajax({
		url:'../ajax/reclamo.php?op=nuevo_editar',
		type: "POST",
		data: formData, 
		contentType:false,
		processData:false,

		success: function(datos){ 
			 $('#cargandoModal').fadeOut(300,function(){
                $('#cargandoModal').modal('hide');
             });

			if(datos=="¡Registro creado con exito!" || datos=="¡Registro editado con exito!"){
				Swal.fire({
					icon: 'success',
					title: datos,
					allowOutsideClick: false
				});
				mostrar_form(false);
				tabla.ajax.reload(); 
			}else{
				Swal.fire({
					icon: 'error',
					title: datos,
					allowOutsideClick: false
				});
			}
		}		
	});
}

function mostrar(id){
	$('#cargandoModal').modal('show');
	$.post("../ajax/reclamo.php?op=mostrar", {idReclamo: id}, function(data, status){
		try {
        /* Si el JSON está mal formado se generará una excepción */
        data = JSON.parse(data);
        if (data.error == true) {
          /* Si hemos enviado por JSON un error, lo notificamos */
          console.log('ERROR detectado:', data);
          return;
        }
        /* Trabajamos habitualmente con la respuesta */
        $('#cargandoModal').fadeOut(300,function(){
        	$('#cargandoModal').modal('hide');
        });
		mostrar_form(true);
		$('#div-ultima-mod').show();
		$('#ultimaModNombre').text(data.ultimaModNombre);
		var fechaHora=data.fecha_hora_ultima_mod.split(" ");
		$("#ultimaModFecha").text(fechaHora[0]);
		$('#ultimaModHora').text(fechaHora[1]);
		
		$('#idReclamo').val(data.id_reclamo);	
		$('#idCliente').val(data.id_cliente);	
		$('#estado').val(data.estado);
		$('#estado').selectpicker('refresh');
		$('#fechaReclamo').datepicker('setDate',data.fecha_reclamo);
		$('#apellidoNombre').val(data.apellidoNombre);
		$('#dni').val(data.dni);
		$("#cantMot").text(data.motivo.length);
		$('#motivo').val(data.motivo);


      } catch (error) {
        /* Si el JSON está mal, notificamos su contenido */
        console.log('ERROR. Recibido:', data);
      }
	})
}



init();