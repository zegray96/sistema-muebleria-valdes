function init(){
	$('#tituloPagina').text('Sistema de Cobros - Cuotas');	
	$("#wrapper").show(); 
	$("#preload").hide(); 
	mostrar_form_cuota(false);
	$('#textFiltro').text('Vencimientos de hoy');
	actualizar_cuotas_vencidas();

	listar_por_cobrador_cargar_datos();
}

// formato fecha
function format_fecha(elEvento,idElem){
	var evento = elEvento || window.event;
	if(evento.keyCode == 8){
	} else {
		var fecha = document.getElementById(idElem);
		if(fecha.value.length == 2 || fecha.value.length == 5){
			fecha.value += "/";
		}
	}
}

$("#fechaVencimiento").keydown(function(event) {
  format_fecha(event,'fechaVencimiento');
});



function actualizar_cuotas_vencidas(){
	$('#actualizandoCuotasVencidasModal').modal('show');
	$.ajax({
		url:'../ajax/cuota.php?op=actualizar_cuotas_vencidas',//lugar a donde se envia los datos obtenidos del formulario
		type: "POST",
		contentType:false,
		processData:false,

		success: function(datos){ 
			 $('#actualizandoCuotasVencidasModal').fadeOut(300,function(){
                $('#actualizandoCuotasVencidasModal').modal('hide');
             });

			if(datos=="¡Estado de cuotas actualizadas con exito!"){
				Swal.fire({
					icon: 'success',
					title: datos,
					allowOutsideClick: false
				});
				listar_vencimientos_hoy();
			}else{
				Swal.fire({
					icon: 'error',
					title: datos,
					allowOutsideClick: false
				});
			}
		}		
	});
}


// input formato moneda
$('#montoAdicional').keyup(function(event) {

  if(event.which >= 37 && event.which <= 40){
  	event.preventDefault();
  }

  $(this).val(function(index, value) {
  	return value.replace(/\D/g, "").replace(/([0-9])([0-9]{2})$/, '$1.$2').replace(/\B(?=(\d{3})+(?!\d)\.?)/g, ",");
  });

});

$('#btnListarPorFechaVenc').click(function() {
	listar_por_fecha_vencimiento();
});

$('#btnCancelarFechaVenc').click(function() {
	$('#seleccionarRangoFechas').modal('hide');
});

// Rango fechas
$('#div-rango-fechas .input-daterange').datepicker({        
	format: 'dd/mm/yyyy',        
	autoclose: true,     
	todayHighlight: true,   
	todayBtn: 'linked',
	language: 'es',
});  


$('#listarPor').on('change', function() {
	if ($('#listarPor').val()=="Vencimientos de hoy"){
		listar_vencimientos_hoy();
		$('#listarPor').val("");
		$('#listarPor').selectpicker('refresh');
		$('#textFiltro').text('Vencimientos de hoy');
		$('#listarPorCobrador option[value=""]').prop("selected", true);
		$('#listarPorCobrador').selectpicker('refresh');
	}else{
		if ($('#listarPor').val()=="Fecha vencimiento") {
			$('#seleccionarRangoFechas').modal('show');
			$('#listarPor').val("");
			$('#listarPor').selectpicker('refresh');
			$('#fechaIniVencListar').datepicker("setDate", '');
			$('#fechaFinVencListar').datepicker("setDate", '');
		}else{
			if ($('#listarPor').val()=="Cliente") {
				$('#buscarClienteParaFiltrar').modal('show');
				$('#listarPor').val("");
				$('#listarPor').selectpicker('refresh');
			}else{
				if ($('#listarPor').val()=="Todas") {
					listar_todas();
					$('#listarPor').val("");
					$('#listarPor').selectpicker('refresh');
					$('#textFiltro').text('Todas');
					$('#listarPorCobrador option[value=""]').prop("selected", true);
					$('#listarPorCobrador').selectpicker('refresh');
				}	
			}	
		}
	}
});


// CUOTA
$("#formCuota").on("submit", function(e){ 
	editar(e);
});


$('#btnCancelarCuota').click(function() {
	mostrar_form_cuota(false);
});

function limpiar(){

}

function mostrar_form_cuota(mostrar){
	limpiar();
	if(mostrar){

		$('#fechaVencimiento').datepicker({
			format: 'dd/mm/yyyy',
			todayBtn: 'linked',
			todayHighlight: true,
			autoclose: true,
			language: 'es',
		}).datepicker();

		$('#listado').hide();
		$('#formularioCuota').show();
		$('#contenedor-cabecera').css("cssText", "display:none !important");
		$('#btnGuardarCuota').prop("disabled",false);
	}else{
		$('#listado').show();
		$('#formularioCuota').hide();
		$('#contenedor-cabecera').css("cssText", "display:block !important");
	}
}




function editar(e){
	$('#cargandoModal').modal('show');
	
	e.preventDefault(); //no se activara la accion predeterminada del evento, osea del submit, se va hacer lo que yo le digo
	var formData = new FormData($('#formCuota')[0]); //guardo todos los datos del form en formData
	$.ajax({
		url:'../ajax/cuota.php?op=editar',//lugar a donde se envia los datos obtenidos del formulario
		type: "POST",
		data: formData, //estos son los datos que envio
		contentType:false,
		processData:false,

		success: function(datos){ 
			 $('#cargandoModal').fadeOut(300,function(){
                $('#cargandoModal').modal('hide');
             });

			if(datos=="¡Registro editado con exito!"){
				Swal.fire({
					icon: 'success',
					title: datos,
					allowOutsideClick: false
				});
				mostrar_form_cuota(false);
				tabla.ajax.reload(); 
			}else{
				Swal.fire({
					icon: 'error',
					title: datos,
					allowOutsideClick: false
				});
			}
		}		
	});
}



function mostrar_cuota(idVenta,nroCuota){
	$('#cargandoModal').modal('show');
	$.post("../ajax/cuota.php?op=mostrar", {idVenta: idVenta, nroCuota : nroCuota}, function(data, status){
		try {
        /* Si el JSON está mal formado se generará una excepción */
        data = JSON.parse(data);
        if (data.error == true) {
          /* Si hemos enviado por JSON un error, lo notificamos */
          console.log('ERROR detectado:', data);
          return;
        }
        /* Trabajamos habitualmente con la respuesta */
        $('#cargandoModal').fadeOut(300,function(){
			$('#cargandoModal').modal('hide');
		});
		mostrar_form_cuota(true);
		$('#nroCuota').val(data.nro_cuota);
		$('#idVenta').val(data.id_venta);
		$('#nroCuotaFormPago').val(data.nro_cuota);
		$('#idVentaFormPago').val(data.id_venta);

		$('#ultimaModNombre').text(data.ultimaModNombre);
		var fechaHora=data.fecha_hora_ultima_mod.split(" ");
		$("#ultimaModFecha").text(fechaHora[0]);
		$('#ultimaModHora').text(fechaHora[1]);

		if(data.estado=="PENDIENTE"){

			$('#estado').removeClass().addClass('badge badge-warning');
			$('#estado').text(' '+data.estado);

			$.post("../ajax/pago.php?op=listar_pagos&idVenta="+idVenta+"&nroCuota="+nroCuota, function(r){ //parametro r son las opciones que nos devuelve el selectUsuarios
				$("#listPagos").html(r); //aca va el id del elemento del form donde vamos a almacenar
			});	
			
		}else{
			if(data.estado=="PAGADA"){
				

				$('#estado').removeClass().addClass('badge badge-success');
				$('#estado').text(' '+data.estado);

				$.post("../ajax/pago.php?op=listar_pagos&idVenta="+idVenta+"&nroCuota="+nroCuota, function(r){ //parametro r son las opciones que nos devuelve el selectUsuarios
					$("#listPagos").html(r); //aca va el id del elemento del form donde vamos a almacenar
				});	

			}else{
				if(data.estado=="VENCIDA"){
					

					$('#estado').removeClass().addClass('badge badge-danger');
					$('#estado').text(' '+data.estado);
					
					$.post("../ajax/pago.php?op=listar_pagos&idVenta="+idVenta+"&nroCuota="+nroCuota, function(r){
						$("#listPagos").html(r); //aca va el id del elemento del form donde vamos a almacenar
					});	
				}else{
					if(data.estado=="EN PROCESO"){


						$('#estado').removeClass().addClass('badge badge-info');
						$('#estado').text(' '+data.estado);

						$.post("../ajax/pago.php?op=listar_pagos&idVenta="+idVenta+"&nroCuota="+nroCuota, function(r){
						$("#listPagos").html(r); //aca va el id del elemento del form donde vamos a almacenar
					});	
					}
				}


			}
		}

		$('#nroCuotaString').val(data.nro_cuota);
		$('#fechaVencimiento').datepicker("setDate", data.fecha_vencimiento);
		$('#nroComprobanteVentaAsoc').val(data.nroComprobanteVentaAsociado.toString().padStart(8, 0));
		$('#montoCuota').val(Intl.NumberFormat("en-IN", { maximumFractionDigits: 2, minimumFractionDigits: 2 }).format(data.montoCuota));

		$('#dni').val(data.dni);
		$('#apellidoNombre').val(data.apellidoNombre);
		
		$('#montoAdicional').val(Intl.NumberFormat("en-IN", { maximumFractionDigits: 2, minimumFractionDigits: 2 }).format(data.monto_adicional));
		
		$("#cantObs").text(data.observacion.length);
		$('#observacion').val(data.observacion);



		$.post("../ajax/pago.php?op=total_pagado&idVenta="+idVenta+"&nroCuota="+nroCuota, function(data){ 
			$('#montoPendiente').text(Intl.NumberFormat("en-IN", { maximumFractionDigits: 2, minimumFractionDigits: 2 }).format(data));

		});	
		

      } catch (error) {
        /* Si el JSON está mal, notificamos su contenido */
        console.log('ERROR. Recibido:', data);
      }
	})
}




// Al abrir modal buscar clientes para filtrar
$('#buscarClienteParaFiltrar').on('shown.bs.modal', function () {
   tablaClientes = $('#tblClientes').dataTable({
   		//utilizar en caso de que no haya server side
		"aProccessing": true, //Activamos el procesamiento de datatables
		"aServerSide": true, //Paginacion y filtrado relizados por el servidor
		//
		"searching": true, //no me muestra el boton buscar
        "language":{
        		"url": "../assets/json/Spanish.json"    
        },
        "scrollY": '48vh', //tamaño de barra de desplazamiento
		"scrollX": true, //muestra el scroll x
        "lengthMenu": [[25, 100, 1000], [25, 100, 1000]],
       	dom: 'frtip', //definimos los elementos del control de la tabla

		"ajax":{
			url:'../ajax/cuota.php?op=listar_clientes',
			type: 'get',
			dataType: 'json', 
			error: function(e){
				console.log(e.responseText);
			}
		},

		"bDestroy": true,
		"iDisplayLength": 25, //cada cuantos registros realizamos la paginacion
		"order":[[1,"asc"]] //para ordenar los registros
   }).DataTable();
});

// al ocultar buscar cliente para filtrar
$('#buscarClienteParaFiltrar').on('hidden.bs.modal', function (e) {
  var table = $('#tblClientes').DataTable();
  table.clear().draw();
});

function listar_por_fecha_vencimiento(){
	$('#listarPorCobrador option[value=""]').prop("selected", true);
	$('#listarPorCobrador').selectpicker('refresh');
	
	var fechaIni=$('#fechaIniVencListar').val();
	var fechaFin=$('#fechaFinVencListar').val();

	if (fechaIni=="" || fechaFin=="") {
			Swal.fire({
				icon:'error',
				title:'¡Seleccione fechas!',
				allowOutsideClick: false
			});

	}else{
		tabla=$('#tblListado').dataTable({
	    	//utilizar en caso de que no haya server side
			"aProccessing": true, //Activamos el procesamiento de datatables
			"aServerSide": true, //Paginacion y filtrado relizados por el servidor
			//
			"searching": true, //no me muestra el boton buscar
	        "language":{
	        		"url": "../assets/json/Spanish.json"    
	        },
	        "scrollY": '48vh', //tamaño de barra de desplazamiento
			"scrollX": true, //muestra el scroll x
	        "lengthMenu": [[25, 100, 1000], [25, 100, 1000]],
			dom: 'Bflrtip', //definimos los elementos del control de la tabla
			buttons:[
				{
					extend: 'excelHtml5',
					exportOptions: {
						columns: [1,2,3,4,5,6,7,8,9,10]
					},
					text: '<i class="fas fa-file-excel"></i>',
					className: 'btn btn-success',
					titleAttr: 'Exportar a Excel',
					
				},
				

				{
					extend: 'csvHtml5',
					exportOptions: {
						columns: [1,2,3,4,5,6,7,8,9,10]
					},
					text: '<i class="fa fa-file-csv"></i>',
					className: 'btn btn-warning',
					titleAttr: 'Exportar a CSV',
					
				},


				{
					extend: 'print',
					exportOptions: {
						columns: [3,4,5,6,7,9,10]
					},
					text: '<i class="fa fa-print"></i>',
					className: 'btn btn-info',
					titleAttr: 'Imprimir',
					
				},

				

			], 

			"ajax":{
				url:'../ajax/cuota.php?op=listar_por_fecha_vencimiento',
				data: {'fechaIni': fechaIni, 'fechaFin': fechaFin},
				type: 'get',
				dataType: 'json', 
				error: function(e){
					console.log(e.responseText);
				}
			},

			"bDestroy": true,
			"iDisplayLength":25, //cada cuantos registros realizamos la paginacion
			"order":[[1,"desc"]] //para ordenar los registros
	    }).DataTable();
	    $('#seleccionarRangoFechas').modal('hide');
	    $('#textFiltro').text(fechaIni+' - '+fechaFin);
	}	

	
}


function listar_todas(){
	tabla=$('#tblListado').dataTable({
    	//utilizar en caso de que no haya server side
		"aProccessing": true, //Activamos el procesamiento de datatables
		"aServerSide": true, //Paginacion y filtrado relizados por el servidor
		//
		"searching": true, //no me muestra el boton buscar
        "language":{
        		"url": "../assets/json/Spanish.json"    
        },
        "scrollY": '48vh', //tamaño de barra de desplazamiento
		"scrollX": true, //muestra el scroll x
        "lengthMenu": [[25, 100, 1000], [25, 100, 1000]],
		dom: 'Bflrtip', //definimos los elementos del control de la tabla
		buttons:[
			{
					extend: 'excelHtml5',
					exportOptions: {
						columns: [1,2,3,4,5,6,7,8,9,10]
					},
					text: '<i class="fas fa-file-excel"></i>',
					className: 'btn btn-success',
					titleAttr: 'Exportar a Excel',
					
				},
				

				{
					extend: 'csvHtml5',
					exportOptions: {
						columns: [1,2,3,4,5,6,7,8,9,10]
					},
					text: '<i class="fa fa-file-csv"></i>',
					className: 'btn btn-warning',
					titleAttr: 'Exportar a CSV',
					
				},


				{
					extend: 'print',
					exportOptions: {
						columns: [3,4,5,6,7,9,10]
					},
					text: '<i class="fa fa-print"></i>',
					className: 'btn btn-info',
					titleAttr: 'Imprimir',
					
				},

			

		], 

		"ajax":{
			url:'../ajax/cuota.php?op=listar_todas',
			type: 'get',
			dataType: 'json', 
			error: function(e){
				console.log(e.responseText);
			}
		},

		"bDestroy": true,
		"iDisplayLength":25, //cada cuantos registros realizamos la paginacion
		"order":[[1,"desc"]] //para ordenar los registros
    }).DataTable();
}


function listar_vencimientos_hoy(){
	tabla=$('#tblListado').dataTable({
    	//utilizar en caso de que no haya server side
		"aProccessing": true, //Activamos el procesamiento de datatables
		"aServerSide": true, //Paginacion y filtrado relizados por el servidor
		//
		"searching": true, //no me muestra el boton buscar
        "language":{
        		"url": "../assets/json/Spanish.json"    
        },
        "scrollY": '48vh', //tamaño de barra de desplazamiento
		"scrollX": true, //muestra el scroll x
        "lengthMenu": [[25, 100, 1000], [25, 100, 1000]],
		dom: 'Bflrtip', //definimos los elementos del control de la tabla
		buttons:[
			{
					extend: 'excelHtml5',
					exportOptions: {
						columns: [1,2,3,4,5,6,7,8,9,10]
					},
					text: '<i class="fas fa-file-excel"></i>',
					className: 'btn btn-success',
					titleAttr: 'Exportar a Excel',
					
				},
				

				{
					extend: 'csvHtml5',
					exportOptions: {
						columns: [1,2,3,4,5,6,7,8,9,10]
					},
					text: '<i class="fa fa-file-csv"></i>',
					className: 'btn btn-warning',
					titleAttr: 'Exportar a CSV',
					
				},


				{
					extend: 'print',
					exportOptions: {
						columns: [3,4,5,6,7,9,10]
					},
					text: '<i class="fa fa-print"></i>',
					className: 'btn btn-info',
					titleAttr: 'Imprimir',
					
				},

			

		], 

		"ajax":{
			url:'../ajax/cuota.php?op=listar_vencimientos_hoy',
			type: 'get',
			dataType: 'json', 
			error: function(e){
				console.log(e.responseText);
			}
		},

		"bDestroy": true,
		"iDisplayLength":25, //cada cuantos registros realizamos la paginacion
		"order":[[1,"desc"]] //para ordenar los registros
    }).DataTable();
}


function listar_por_cliente(idCliente,apellidoNombre){
	$('#listarPorCobrador option[value=""]').prop("selected", true);
	$('#listarPorCobrador').selectpicker('refresh');
	$('#buscarClienteParaFiltrar').modal('hide');
	tabla=$('#tblListado').dataTable({
    	//utilizar en caso de que no haya server side
		"aProccessing": true, //Activamos el procesamiento de datatables
		"aServerSide": true, //Paginacion y filtrado relizados por el servidor
		//
		"searching": true, //no me muestra el boton buscar
        "language":{
        		"url": "../assets/json/Spanish.json"    
        },
        "scrollY": '48vh', //tamaño de barra de desplazamiento
		"scrollX": true, //muestra el scroll x
        "lengthMenu": [[25, 100, 1000], [25, 100, 1000]],
		dom: 'Bflrtip', //definimos los elementos del control de la tabla
		buttons:[
			{
					extend: 'excelHtml5',
					exportOptions: {
						columns: [1,2,3,4,5,6,7,8,9,10]
					},
					text: '<i class="fas fa-file-excel"></i>',
					className: 'btn btn-success',
					titleAttr: 'Exportar a Excel',
					
				},
				

				{
					extend: 'csvHtml5',
					exportOptions: {
						columns: [1,2,3,4,5,6,7,8,9,10]
					},
					text: '<i class="fa fa-file-csv"></i>',
					className: 'btn btn-warning',
					titleAttr: 'Exportar a CSV',
					
				},


				{
					extend: 'print',
					exportOptions: {
						columns: [3,4,5,6,7,9,10]
					},
					text: '<i class="fa fa-print"></i>',
					className: 'btn btn-info',
					titleAttr: 'Imprimir',
					
				},

			

		], 

		"ajax":{
			url:'../ajax/cuota.php?op=listar_por_cliente',
			data: {'idCliente': idCliente},
			type: 'get',
			dataType: 'json', 
			error: function(e){
				console.log(e.responseText);
			}
		},

		"bDestroy": true,
		"iDisplayLength":25, //cada cuantos registros realizamos la paginacion
		"order":[[1,"desc"]] //para ordenar los registros
    }).DataTable();

    $('#textFiltro').text(apellidoNombre);
}

// PAGO

// input formato moneda
$('#montoPagado').keyup(function(event) {

  if(event.which >= 37 && event.which <= 40){
  	event.preventDefault();
  }

  $(this).val(function(index, value) {
  	return value.replace(/\D/g, "").replace(/([0-9])([0-9]{2})$/, '$1.$2').replace(/\B(?=(\d{3})+(?!\d)\.?)/g, ",");
  });

});

$('#btnNuevoPago').click(function() {
	mostrar_form_pago(true);
});

$('#btnEliminarPago').click(function() {
	eliminar_pago($('#idPago').val(),$('#idVentaFormPago').val(),$('#nroCuotaFormPago').val());
});

$('#btnCancelarPago').click(function() {
	mostrar_form_pago(false);
});

$('#btnImprimirPago').click(function() {
	imprimir_comprobante($('#idPago').val());
});

$("#formPago").on("submit", function(e){ 
	nuevo_pago(e);
});

function mostrar_form_pago(mostrar){
	limpiar_pago();
	if(mostrar){
		$('#pago').modal('show');
		$('#btnGuardarPago').show();
		$('#btnEliminarPago').hide();
		$('#btnImprimirPago').hide();
		$('#montoPagado').prop('readonly',false);
	}else{
		$('#pago').modal('hide');
	}
}

function limpiar_pago(){
	$('#idPago').val("");
	$('#nroComprobante').val("");
	$('#fechaHoraPago').val("");
	$('#montoPagado').val("");
	$('#registradoPor').val("");
}

function nuevo_pago(e){
	$('#cargandoModal').modal('show');	
	e.preventDefault(); //no se activara la accion predeterminada del evento, osea del submit, se va hacer lo que yo le digo
	var formData = new FormData($('#formPago')[0]); //guardo todos los datos del form en formData
	$.ajax({
		url:'../ajax/pago.php?op=nuevo',//lugar a donde se envia los datos obtenidos del formulario
		type: "POST",
		data: formData, //estos son los datos que envio
		contentType:false,
		processData:false,

		success: function(datos){ 
			 $('#cargandoModal').fadeOut(300,function(){
                $('#cargandoModal').modal('hide');
             });
             // cortamos la cadena
             resultado = datos.split(":");
             resultadoOperacion = resultado[0];
             resultadoId = resultado[1];

             if(resultadoOperacion=="¡Registro creado con exito!"){
             	Swal.fire({
             		icon: 'success',
             		title: resultadoOperacion,
             		allowOutsideClick: false
             	});
             	imprimir_comprobante(resultadoId);
             	mostrar_form_pago(false);
             	mostrar_form_cuota(true);
             	mostrar_cuota($('#idVenta').val(),$('#nroCuota').val());
             	tabla.ajax.reload(); 
             }else{
             	Swal.fire({
             		icon: 'error',
             		title: datos,
             		allowOutsideClick: false
             	});
             }
		}		
	});
}

function mostrar_pago(idPago){
	$('#cargandoModal').modal('show');
	$.post("../ajax/pago.php?op=mostrar", {idPago: idPago}, function(data, status){
		try {
        /* Si el JSON está mal formado se generará una excepción */
        data = JSON.parse(data);
        if (data.error == true) {
          /* Si hemos enviado por JSON un error, lo notificamos */
          console.log('ERROR detectado:', data);
          return;
        }
        $('#cargandoModal').fadeOut(300,function(){
        	$('#cargandoModal').modal('hide');
        });
        /* Trabajamos habitualmente con la respuesta */
		mostrar_form_pago(true);
		$('#idPago').val(data.id_pago);
		$('#nroComprobante').val(data.nro_comprobante.padStart(8, 0));
		$('#montoPagado').prop('readonly',true);
		$('#montoPagado').val(Intl.NumberFormat("en-IN", { maximumFractionDigits: 2, minimumFractionDigits: 2 }).format(data.monto_pagado));

		var hora=data.fecha_hora.split(" ");
		hora=hora[1]; 

		$('#fechaHoraPago').val(data.fechaPago+' '+hora);
		$('#registradoPor').val(data.registradoPor);
		$('#btnGuardarPago').hide();
		$('#btnEliminarPago').show();
		$('#btnImprimirPago').show();
		

      } catch (error) {
        /* Si el JSON está mal, notificamos su contenido */
        console.log('ERROR. Recibido:', data);
      }
	})
}

function eliminar_pago(idPago,idVentaFormPago,nroCuotaFormPago){
		if (idPago=="") {
			Swal.fire({
				title: '¡Ocurrió un error!',
				icon: 'error',
			});
		}else{

			Swal.fire({
				title: '¿Esta seguro que desea eliminar el registro?',
				icon: 'warning',
				showCancelButton: true,
				confirmButtonColor: '#3085d6',
				cancelButtonColor: '#d33',
				showConfirmButton:true,
				confirmButtonText: 'Confirmar',
				cancelButtonText: 'Cancelar',
				reverseButtons: true,
				allowOutsideClick: false
			}).then((result) => {
				if (result.value) {
					$("#cargandoModal").modal('show');

					$.post("../ajax/pago.php?op=eliminar", {idPago : idPago, idVentaFormPago : idVentaFormPago, nroCuotaFormPago : nroCuotaFormPago}, function(datos){
						$('#cargandoModal').fadeOut(300,function(){
							$('#cargandoModal').modal('hide');
						});
						if(datos=="¡Registro eliminado con exito!"){
							Swal.fire({
								icon: 'success',
								title: datos,
								allowOutsideClick: false
							});
							mostrar_form_pago(false);
							mostrar_form_cuota(true);
							mostrar_cuota($('#idVenta').val(),$('#nroCuota').val());
							tabla.ajax.reload();
						}else{
							Swal.fire({
								icon: 'error',
								title: datos,
								allowOutsideClick: false
							});
						}						
					});
				}
			});

		}	

}

function imprimir_comprobante(id){
	if (id=="") {
		Swal.fire({
			title: '¡Ocurrió un error!',
			icon: 'error',
		});
	}else{
		window.open('../reportes/comprobante-pago?id='+id, '_blank');
	}
}

function listar_por_cobrador_cargar_datos(){
	//cargamos los items al select
	$.post("../ajax/venta.php?op=listar_por_cobrador_cargar_datos", function(r){ //parametro r son las opciones que nos devuelve el selectUsuarios
		$("#listarPorCobrador").html(r); //aca va el id del elemento del form donde vamos a almacenar
		$('#listarPorCobrador').selectpicker('refresh'); //refresco el select
	});		
}

$('#listarPorCobrador').on('change', function () {
    tabla.columns(8).search(this.value).draw();
});

init();
