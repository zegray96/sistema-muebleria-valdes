var tabla;
var total;
var tablaClientes;
var idFila;
var hoy = new Date(); //fecha de hoy
var idUsuarioSesion;

function init(){
	$('#tituloPagina').text('Sistema de Cobros - Ventas');
	listar_todas();
	mostrar_form(false);
	$('#wrapper').show(); 
	$('#preload').hide(); 

	$("#form").on("submit", function(e){ 
		nuevo_editar(e);
	});

	
	//traemos con ajax la variable de sesion
	$.get("../ajax/venta.php?op=traer_id_usuario", function(data){
		idUsuarioSesion=data;
	});

	$('#textFiltro').text('Todas');

	cargar_cobradores();
	listar_por_cobrador_cargar_datos();

}

// formato fecha
function format_fecha(elEvento,idElem){
	var evento = elEvento || window.event;
	if(evento.keyCode == 8){
	} else {
		var fecha = document.getElementById(idElem);
		if(fecha.value.length == 2 || fecha.value.length == 5){
			fecha.value += "/";
		}
	}
}

$("#fechaVenta").keydown(function(event) {
  format_fecha(event,'fechaVenta');
});

//colocar la primer letra de palabra en mayusuclas 
function capitalizar(str) {
    return str.replace(/\w\S*/g, function(txt){
        return txt.charAt(0).toUpperCase() + txt.substr(1).toLowerCase();
    });
}


$('#montoAbonado').keyup(function(event) {

	if(event.which >= 37 && event.which <= 40){
		event.preventDefault();
	}

	$(this).val(function(index, value) {
		return value.replace(/\D/g, "").replace(/([0-9])([0-9]{2})$/, '$1.$2').replace(/\B(?=(\d{3})+(?!\d)\.?)/g, ",");
	});

	calcular_monto_cuota();

});

$('#descripcion').blur(function() {
	$('#descripcion').val(capitalizar($('#descripcion').val()));
});

// input formato moneda
$('#precioVenta').keyup(function(event) {

  if(event.which >= 37 && event.which <= 40){
  	event.preventDefault();
  }

  $(this).val(function(index, value) {
  	return value.replace(/\D/g, "").replace(/([0-9])([0-9]{2})$/, '$1.$2').replace(/\B(?=(\d{3})+(?!\d)\.?)/g, ",");
  });

});


// Rango fechas
$('#div-rango-fechas .input-daterange').datepicker({        
	format: 'dd/mm/yyyy',        
	autoclose: true,     
	todayHighlight: true,   
	todayBtn: 'linked',
	language: 'es',
});  

$('#listarPor').on('change', function() {
	if ($('#listarPor').val()=="Fecha") {
		$('#seleccionarFechas').modal('show');
		$('#listarPor').val("");
		$('#listarPor').selectpicker('refresh');
		$('#fechaIniListar').datepicker("setDate", '');
		$('#fechaFinListar').datepicker("setDate", '');
	}else{
		if ($('#listarPor').val()=="Cliente") {
			$('#buscarClienteParaFiltrar').modal('show');
			$('#listarPor').val("");
			$('#listarPor').selectpicker('refresh');			
		}else{
			if ($('#listarPor').val()=="Todas") {
				listar_todas();
				$('#listarPor').val("");
				$('#listarPor').selectpicker('refresh');
				$('#textFiltro').text('Todas');
				$('#listarPorCobrador option[value=""]').prop("selected", true);
				$('#listarPorCobrador').selectpicker('refresh');
			}	
		}	
	}
});

// calculo cuotas
$('#cuotas').on('change', function() {
	calcular_monto_cuota();
});

// botones
$('#btnNuevo').click(function() {
	mostrar_form(true);
	$('#idCobrador').val(idUsuarioSesion);
	$('#idCobrador').selectpicker('refresh');
});

$('#btnCancelar').click(function() {
	mostrar_form(false);
	cargar_cobradores();

});

$('#btnBuscarCliente').click(function() {
	$('#buscarClienteParaVenta').modal('show');
});

$('#btnAgregarDetalle').click(function (){
	var descripcion = $('#descripcion').val();
	var cantidad = $('#cantidad').val();
	var precioVentaFormateado = $('#precioVenta').val();
	nuevo_detalle(descripcion,cantidad,precioVentaFormateado);
});

$('#btnListarPorFecha').click(function() {
	listar_por_fecha();
});

$('#btnCancelarFecha').click(function() {
	$('#seleccionarFechas').modal('hide');
});

// fin botones

function calcular_monto_cuota(){
	var total = 0.0;
	//Recorro todos los tr ubicados en el tbody
	$('#tblDetalles tbody').find('tr').each(function () {     
		//Voy incrementando las variables segun la fila ( .eq(0) representa la fila 1 )     
		total+= parseFloat($(this).find('td').eq(1).text());   
		        
	});
	// quitamos separador de miles
	var montoAbonadoSinComa=$('#montoAbonado').val().replace(/[,]/g,'');
	var cuotas=$('#cuotas').val();

	if(cuotas==""){
		alert(cuotas);
	}
	var montoCuota = (total-montoAbonadoSinComa)/cuotas;
	montoCuota=montoCuota.toFixed(2);
	$('#montoCuotaString').val(Intl.NumberFormat("en-IN", { maximumFractionDigits: 2, minimumFractionDigits: 2 }).format(montoCuota));
	$('#montoCuota').val(montoCuota);
}

$('#listarPorCobrador').on('change', function () {
    tabla.columns(6).search(this.value).draw();
});

function listar_todas(){
	tabla=$('#tblListado').dataTable({
    	//utilizar en caso de que no haya server side
		"aProccessing": true, //Activamos el procesamiento de datatables
		"aServerSide": true, //Paginacion y filtrado relizados por el servidor
		//
		"searching": true, //no me muestra el boton buscar
        "language":{
        		"url": "../assets/json/Spanish.json"    
        },
        "scrollY": '48vh', //tamaño de barra de desplazamiento
		"scrollX": true, //muestra el scroll x
        "lengthMenu": [[25, 100, 1000], [25, 100, 1000]],
		dom: 'Bflrtip', //definimos los elementos del control de la tabla
		buttons:[
			{
				extend: 'excelHtml5',
				exportOptions: {
					columns: [1,2,3,4,5,6,7,8]
				},
				text: '<i class="fas fa-file-excel"></i>',
				className: 'btn btn-success',
				titleAttr: 'Exportar a Excel',
				
			},
			

			{
				extend: 'csvHtml5',
				exportOptions: {
					columns: [1,2,3,4,5,6,7,8]
				},
				text: '<i class="fa fa-file-csv"></i>',
				className: 'btn btn-warning',
				titleAttr: 'Exportar a CSV',
				
			},


			{
				extend: 'print',
				exportOptions: {
                   columns: [1,2,3,4,5,6,7,8]
               	},
				text: '<i class="fa fa-print"></i>',
				className: 'btn btn-info',
				titleAttr: 'Imprimir',
				
			},

			

		], 

		"ajax":{
			url:'../ajax/venta.php?op=listar',
			type: 'get',
			dataType: 'json', 
			error: function(e){
				console.log(e.responseText);
			}
		},

		"bDestroy": true,
		"iDisplayLength":25, //cada cuantos registros realizamos la paginacion
		"order":[[1,"desc"]] //para ordenar los registros
    }).DataTable();
}


function listar_por_cliente(idCliente,apellidoNombre){
	$('#buscarClienteParaFiltrar').modal('hide');
	$('#listarPorCobrador option[value=""]').prop("selected", true);
	$('#listarPorCobrador').selectpicker('refresh');

	tabla=$('#tblListado').dataTable({
    	//utilizar en caso de que no haya server side
		"aProccessing": true, //Activamos el procesamiento de datatables
		"aServerSide": true, //Paginacion y filtrado relizados por el servidor
		//
		"searching": true, //no me muestra el boton buscar
        "language":{
        		"url": "../assets/json/Spanish.json"    
        },
        "scrollY": '48vh', //tamaño de barra de desplazamiento
		"scrollX": true, //muestra el scroll x
        "lengthMenu": [[25, 100, 1000], [25, 100, 1000]],
		dom: 'Bflrtip', //definimos los elementos del control de la tabla
		buttons:[
			{
				extend: 'excelHtml5',
				exportOptions: {
					columns: [1,2,3,4,5,6,7,8]
				},
				text: '<i class="fas fa-file-excel"></i>',
				className: 'btn btn-success',
				titleAttr: 'Exportar a Excel',
				
			},
			

			{
				extend: 'csvHtml5',
				exportOptions: {
					columns: [1,2,3,4,5,6,7,8]
				},
				text: '<i class="fa fa-file-csv"></i>',
				className: 'btn btn-warning',
				titleAttr: 'Exportar a CSV',
				
			},


			{
				extend: 'print',
				exportOptions: {
					columns: [1,2,3,4,5,6,7,8]
				},
				text: '<i class="fa fa-print"></i>',
				className: 'btn btn-info',
				titleAttr: 'Imprimir',
				
			},

			

		], 

		"ajax":{
			url:'../ajax/venta.php?op=listar_por_cliente',
			data: {'idCliente': idCliente},
			type: 'get',
			dataType: 'json', 
			error: function(e){
				console.log(e.responseText);
			}
		},

		"bDestroy": true,
		"iDisplayLength":25, //cada cuantos registros realizamos la paginacion
		"order":[[1,"desc"]] //para ordenar los registros
    }).DataTable();

    $('#textFiltro').text(apellidoNombre);
}

function listar_por_fecha(){

	var fechaIni=$('#fechaIniListar').val();
	var fechaFin=$('#fechaFinListar').val();

	if (fechaIni=="" || fechaFin=="") {
			Swal.fire({
				icon:'error',
				title:'¡Seleccione fechas!',
				allowOutsideClick: false
			});

	}else{
		$('#listarPorCobrador option[value=""]').prop("selected", true);
		$('#listarPorCobrador').selectpicker('refresh');

		tabla=$('#tblListado').dataTable({
	    	//utilizar en caso de que no haya server side
			"aProccessing": true, //Activamos el procesamiento de datatables
			"aServerSide": true, //Paginacion y filtrado relizados por el servidor
			//
			"searching": true, //no me muestra el boton buscar
			"language":{
				"url": "../assets/json/Spanish.json"    
			},
	        "scrollY": '48vh', //tamaño de barra de desplazamiento
			"scrollX": true, //muestra el scroll x
			"lengthMenu": [[25, 100, 1000], [25, 100, 1000]],
			dom: 'Bflrtip', //definimos los elementos del control de la tabla
			buttons:[
			{
				extend: 'excelHtml5',
				exportOptions: {
					columns: [1,2,3,4,5,6,7,8]
				},
				text: '<i class="fas fa-file-excel"></i>',
				className: 'btn btn-success',
				titleAttr: 'Exportar a Excel',

			},


			{
				extend: 'csvHtml5',
				exportOptions: {
					columns: [1,2,3,4,5,6,7,8]
				},
				text: '<i class="fa fa-file-csv"></i>',
				className: 'btn btn-warning',
				titleAttr: 'Exportar a CSV',

			},


			{
				extend: 'print',
				exportOptions: {
					columns: [1,2,3,4,5,6,7,8]
				},
				text: '<i class="fa fa-print"></i>',
				className: 'btn btn-info',
				titleAttr: 'Imprimir',

			},



			], 

			"ajax":{
				url:'../ajax/venta.php?op=listar_por_fecha',
				data: {'fechaIni': fechaIni, 'fechaFin': fechaFin},
				type: 'get',
				dataType: 'json', 
				error: function(e){
					console.log(e.responseText);
				}
			},

			"bDestroy": true,
			"iDisplayLength":25, //cada cuantos registros realizamos la paginacion
			"order":[[4,"desc"]] //para ordenar los registros
		}).DataTable();

		$('#seleccionarFechas').modal('hide');
		$('#textFiltro').text(fechaIni+' - '+fechaFin);
	}

	
}


function mostrar_form(mostrar){
	limpiar();
	if(mostrar){

      $('#fechaVenta').datepicker({
      	format: 'dd/mm/yyyy',
      	todayBtn: 'linked',
      	todayHighlight: true,
      	autoclose: true,
      	language: 'es',
      }).datepicker("setDate", new Date());
		
		$('#listado').hide();
		$('#formulario').show();
  		$('#datos-articulo').show();
  		$('#div-ultima-mod').hide();
		$('#contenedor-cabecera').css("cssText", "display:none !important");
	}else{
		$('#listado').show();
		$('#formulario').hide();
		$('#contenedor-cabecera').css("cssText", "display:block !important");

	}
}

function limpiar(){
	idFila=0;
	$("#idVenta").val("");
	$("#ptoVenta").val("");
	$("#nroComprobante").val("");
	$("#nroComprobanteString").val("");
	$("#fechaVenta").val("");
	$("#idCliente").val("");
	$("#dni").val("");
	$("#apellidoNombre").val("");
	$("#domicilio").val("");

	$("#descripcion").val("");
	$("#precioVenta").val("");
	$("#cantidad").val("");

	$(".filas").remove();

	$("#diaCobro").val("");
	$("#diaCobro").selectpicker('refresh');
	$("#montoAbonado").val("");
	$("#cuotas").val("1");
	$("#cuotas").selectpicker('refresh');
	$("#montoCuota").val("");
	$("#montoCuotaString").val("");

	
	
	$('#total').val("0");
	$('#totalString').val("0.00");
	total=0.0;
	cont=0;

}

// Al abrir modal buscar clientes para venta
$('#buscarClienteParaVenta').on('shown.bs.modal', function () {
   tablaClientes = $('#tblClientesVenta').dataTable({
   		//utilizar en caso de que no haya server side
		"aProccessing": true, //Activamos el procesamiento de datatables
		"aServerSide": true, //Paginacion y filtrado relizados por el servidor
		//
		"searching": true, //no me muestra el boton buscar
        "language":{
        		"url": "../assets/json/Spanish.json"    
        },
        "scrollY": '48vh', //tamaño de barra de desplazamiento
		"scrollX": true, //muestra el scroll x
        "lengthMenu": [[25, 100, 1000], [25, 100, 1000]],
       	dom: 'frtip', //definimos los elementos del control de la tabla

		"ajax":{
			url:'../ajax/venta.php?op=listar_clientes_para_venta',
			type: 'get',
			dataType: 'json', 
			error: function(e){
				console.log(e.responseText);
			}
		},

		"bDestroy": true,
		"iDisplayLength": 25, //cada cuantos registros realizamos la paginacion
		"order":[[1,"asc"]] //para ordenar los registros
   }).DataTable();
});

// al ocultar buscar cliente para venta
$('#buscarClienteParaVenta').on('hidden.bs.modal', function (e) {
  var table = $('#tblClientesVenta').DataTable();
  table.clear().draw();
});



// Al abrir modal buscar clientes para filtrar
$('#buscarClienteParaFiltrar').on('shown.bs.modal', function () {
   tablaClientes = $('#tblClientesFiltrar').dataTable({
   		//utilizar en caso de que no haya server side
		"aProccessing": true, //Activamos el procesamiento de datatables
		"aServerSide": true, //Paginacion y filtrado relizados por el servidor
		//
		"searching": true, //no me muestra el boton buscar
        "language":{
        		"url": "../assets/json/Spanish.json"    
        },
		"scrollY": '48vh', //tamaño de barra de desplazamiento
		"scrollX": true, //muestra el scroll x
        "lengthMenu": [[25, 100, 1000], [25, 100, 1000]],
       	dom: 'frtip', //definimos los elementos del control de la tabla

		"ajax":{
			url:'../ajax/venta.php?op=listar_clientes_para_filtrar',
			type: 'get',
			dataType: 'json', 
			error: function(e){
				console.log(e.responseText);
			}
		},

		"bDestroy": true,
		"iDisplayLength": 25, //cada cuantos registros realizamos la paginacion
		"order":[[1,"asc"]] //para ordenar los registros
   }).DataTable();
});

// al ocultar buscar cliente para filtrar
$('#buscarClienteParaFiltrar').on('hidden.bs.modal', function (e) {
  var table = $('#tblClientesFiltrar').DataTable();
  table.clear().draw();
});






// agregar cliente
function agregar_cliente(idCliente,dni,apellidoNombre,domicilio,telefono){
	$('#idCliente').val(idCliente);
	$('#dni').val(dni);
	$('#apellidoNombre').val(apellidoNombre);
	$('#domicilio').val(domicilio);
	$('#buscarClienteParaVenta').modal('hide');
}

//funcion mostrar
function mostrar(id){
	$('#cargandoModal').modal('show');

	$.post("../ajax/venta.php?op=mostrar", {idVenta: id}, function(data, status){
		
		try {
        /* Si el JSON está mal formado se generará una excepción */
        data = JSON.parse(data);

        if (data.error == true) {
          /* Si hemos enviado por JSON un error, lo notificamos */
          console.log('ERROR detectado:', data);
          return;
        }
        /* Trabajamos habitualmente con la respuesta */
        $('#cargandoModal').fadeOut(300,function(){
			$('#cargandoModal').modal('hide');
		});
		mostrar_form(true);
		$('#div-ultima-mod').show();
		$('#ultimaModNombre').text(data.ultimaModNombre);
		var fechaHora=data.fecha_hora_ultima_mod.split(" ");
		$("#ultimaModFecha").text(fechaHora[0]);
		$('#ultimaModHora').text(fechaHora[1]); 

		$("#idVenta").val(data.id_venta);
		$("#ptoVenta").val("0001")
		$("#nroComprobante").val(data.nro_comprobante);
		$("#nroComprobanteString").val(data.nro_comprobante.padStart(8, 0));
		// Bootstrap Date Picker
		$('#fechaVenta').datepicker("setDate", data.fecha_venta);
		

		$("#idCliente").val(data.id_cliente);
		$('#idCliente').selectpicker('refresh');
		$("#dni").val(data.dni);
		$("#apellidoNombre").val(data.apellidoNombre);
		$("#domicilio").val(data.domicilio);

		$("#idCobrador").val(data.id_cobrador);
		$("#idCobrador").selectpicker('refresh');
		
		$("#diaCobro").val(data.dia_cobro);
		$('#diaCobro').selectpicker('refresh');
		$("#montoAbonado").val(Intl.NumberFormat("en-IN", { maximumFractionDigits: 2, minimumFractionDigits: 2 }).format(data.monto_abonado));
		$("#cuotas").val(data.cuotas);
		$('#cuotas').selectpicker('refresh');

		$("#montoCuota").val(data.monto_cuota);
		$('#montoCuotaString').val(Intl.NumberFormat("en-IN", { maximumFractionDigits: 2, minimumFractionDigits: 2 }).format(data.monto_cuota));


		$("#totalString").val(Intl.NumberFormat("en-IN", { maximumFractionDigits: 2, minimumFractionDigits: 2 }).format(data.total_venta));
		$("#total").val(data.total_venta);


		$.post("../ajax/venta.php?op=listar_detalles&id="+id, function(r){ //parametro r son las opciones que nos devuelve el selectUsuarios
			$("#tblDetalles").html(r); //aca va el id del elemento del form donde vamos a almacenar
			idFila = document.getElementById("idFila").value;
			idFila++;
		});	

      } catch (error) {
        /* Si el JSON está mal, notificamos su contenido */
        console.log('ERROR. Recibido:', data);
      }

		
		
	})
}

//nuevo detalle
function nuevo_detalle(descripcion,cantidad,precioVentaFormateado){
	// quitamos separador de miles
	var precioVenta = precioVentaFormateado.replace(/[,]/g,'');

	if (isNaN(precioVenta) || cantidad<=0) {
		Swal.fire({
			icon: 'warning',
			title: '¡Introduzca un valor valido!',
			allowOutsideClick: false
		});
	}else{
		if(descripcion=="" || cantidad=="" || precioVenta==""){
			Swal.fire({
				icon: 'warning',
				title: '¡Complete datos de artículo!',
				allowOutsideClick: false
			});
		}else{

			var encontrado = buscar_en_detalle(descripcion);
			if(encontrado==true){
				Swal.fire({
					icon: 'warning',
					title: '¡Artículo ya existe en detalle!',
					allowOutsideClick: false
				});
			}else{
				var subtotal = precioVenta * cantidad;
				var subtotalFormateado = Intl.NumberFormat("en-IN", { maximumFractionDigits: 2, minimumFractionDigits: 2 }).format(subtotal);

				var fila='<tr class="filas" id="fila'+idFila+'">'+
				'<td style="display: none"><input class="precioVenta" type="hidden" name="precioVenta[] id="precioVenta[]" value="'+precioVenta+'">'+precioVenta+'</td>'+
				'<td style="display: none"><input type="hidden" name="subtotal[] id="subtotal[]" value="'+subtotal+'">'+subtotal+'</td>'+
				'<td><button type="button" class="btn btn-danger" onclick="borrar_detalle('+idFila+')"><i class="fas fa-trash-alt"></i></button>'+
				' <button type="button" class="btn btn-info" onclick="ver_detalle('+idFila+')"><i class="fas fa-eye"></i></button></td>'+
				'<td><input type="hidden" name="cantidad[] id="cantidad[]" value="'+cantidad+'">'+cantidad+'</td>'+
				'<td><input class="descripcion" type="hidden" name="descripcion[] id="descripcion[]" value="'+descripcion+'">'+descripcion+'</td>'+
				'<td>'+'$'+precioVentaFormateado+'</td>'+
				'<td>'+'$'+subtotalFormateado+'</td>'+
				'</tr>';
				idFila++;
				$("#tblDetalles").append(fila);
				calcular_total();
				calcular_monto_cuota();

				$('#descripcion').val("");
				$('#cantidad').val("");
				$('#precioVenta').val("");

				$('#descripcion').focus();
			}
			
		}
	
	}

	


}

//borrar detalle 
function borrar_detalle(i){

	$("#fila"+i).remove();
	calcular_total();
	calcular_monto_cuota();
}

function ver_detalle(i){
	$('#tblDetalles tr').on('click', function(){
		var descripcion = $(this).find('td:nth-child(5)').text();
		var precioVentaFormateado= $(this).find('td:nth-child(6)').text();
		var cantidad= $(this).find('td:nth-child(4)').text();

		$('#descripcion').val(descripcion);
		$('#precioVenta').val(precioVentaFormateado.replace(/[$]/g,''));
		$('#cantidad').val(cantidad);
		borrar_detalle(i);
	});	
}

function calcular_total(){
	total = 0.0;
	//Recorro todos los tr ubicados en el tbody
	$('#tblDetalles tbody').find('tr').each(function () {     
		//Voy incrementando las variables segun la fila ( .eq(0) representa la fila 1 )     
		total+= parseFloat($(this).find('td').eq(1).text());   
		        
	});


	$("#totalString").val(Intl.NumberFormat("en-IN", { maximumFractionDigits: 2, minimumFractionDigits: 2 }).format(total));

	$("#total").val(total);
}


function detalle_vacio(){
  let filas = $('#tblDetalles').find('tbody tr').length;
  
  if(filas > 0) {
    return false;
  }
  else {
    return true;
  }
}

function buscar_en_detalle(descripcion){
	var encontrado=false;

	if ($('table#tblDetalles tbody tr').length > 0){
		$('table#tblDetalles tbody tr').each(function(){
			if ($(this).find('input.descripcion').val() == descripcion){     
				encontrado = true;
			}
		});      
	}
	return encontrado;        
}

function nuevo_editar(e){
	$('#cargandoModal').modal('show');
	e.preventDefault();
	var detalleVacio = detalle_vacio();

	if (detalleVacio==true){
		$('#cargandoModal').fadeOut(300,function(){
			$('#cargandoModal').modal('hide');
		});
		Swal.fire({
			icon: 'error',
			title: '¡Detalle no puede estar vacio!',
			allowOutsideClick: false
		});

	}else{
		
		var formData = new FormData($("#form")[0]); //guardo todos los datos del form en formData

		$.ajax({
			url:'../ajax/venta.php?op=nuevo_editar',//lugar a donde se envia los datos obtenidos del formulario
			type: "POST",
			data: formData, //estos son los datos que envio
			contentType:false,
			processData:false,

			success: function(datos){ //si la accion se hace de forma correcta se hace esto
				$('#cargandoModal').fadeOut(300,function(){
					$('#cargandoModal').modal('hide');
				});
				// cortamos la cadena
				resultado = datos.split(":");
				resultadoOperacion = resultado[0];
				resultadoId = resultado[1];
				if(resultadoOperacion=="¡Registro creado con exito!" || resultadoOperacion=="¡Registro editado con exito!"){
					Swal.fire({
						icon: 'success',
						title: resultadoOperacion,
						allowOutsideClick: false
					});
					imprimir_comprobante(resultadoId);
					mostrar_form(false);
					tabla.ajax.reload(); 
				}else{
					Swal.fire({
						icon: 'error',
						title: datos,
						allowOutsideClick: false
					});
				}
			}
		});
	}

	
}


function cargar_cobradores(){
	//cargamos los items al select
	$.post("../ajax/venta.php?op=cargar_cobradores", function(r){ //parametro r son las opciones que nos devuelve el selectUsuarios
		$("#idCobrador").html(r); //aca va el id del elemento del form donde vamos a almacenar
		$('#idCobrador').selectpicker('refresh'); //refresco el select
	});	

	
}

function listar_por_cobrador_cargar_datos(){
	//cargamos los items al select
	$.post("../ajax/venta.php?op=listar_por_cobrador_cargar_datos", function(r){ //parametro r son las opciones que nos devuelve el selectUsuarios
		$("#listarPorCobrador").html(r); //aca va el id del elemento del form donde vamos a almacenar
		$('#listarPorCobrador').selectpicker('refresh'); //refresco el select
	});		
}


function imprimir_comprobante(id){
	window.open('../reportes/comprobante-venta?id='+id, '_blank');
}



init();