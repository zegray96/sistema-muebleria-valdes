var tabla;
function init(){
	$('#tituloPagina').text('Sistema de Cobros - Clientes');
	listar();
	mostrar_form(false);
	$("#wrapper").show(); 
	$("#preload").hide(); 

}


$('#btnNuevo').click(function() {
	mostrar_form(true);
});

$('#btnCancelar').click(function() {
	mostrar_form(false);
});

$("#form").on("submit", function(e){ 
	nuevo_editar(e);
});

//colocar la primer letra de palabra en mayusuclas 
function capitalizar(str) {
    return str.replace(/\w\S*/g, function(txt){
        return txt.charAt(0).toUpperCase() + txt.substr(1).toLowerCase();
    });
}

$('#apellidoNombre').blur(function() {
	$('#apellidoNombre').val(capitalizar($('#apellidoNombre').val()));
});

$('#barrio').blur(function() {
	$('#barrio').val(capitalizar($('#barrio').val()));
});

$('#domicilio').blur(function() {
	$('#domicilio').val(capitalizar($('#domicilio').val()));
});

function listar(){
	tabla=$('#tblListado').dataTable({
    	//utilizar en caso de que no haya server side
		"aProccessing": true, //Activamos el procesamiento de datatables
		"aServerSide": true, //Paginacion y filtrado relizados por el servidor
		//
		"searching": true, //no me muestra el boton buscar
        "language":{
        		"url": "../assets/json/Spanish.json"    
        },
        "scrollY": '48vh', //tamaño de barra de desplazamiento
		"scrollX": true, //muestra el scroll x
        "lengthMenu": [[25, 100, 1000], [25, 100, 1000]],
		dom: 'Bflrtip', //definimos los elementos del control de la tabla
		buttons:[
			{
				extend: 'excelHtml5',
				exportOptions: {
					columns: [1,2,3,4,5,6]
				},
				text: '<i class="fas fa-file-excel"></i>',
				className: 'btn btn-success',
				titleAttr: 'Exportar a Excel',
				
			},
			

			{
				extend: 'csvHtml5',
				exportOptions: {
					columns: [1,2,3,4,5,6]
				},
				text: '<i class="fa fa-file-csv"></i>',
				className: 'btn btn-warning',
				titleAttr: 'Exportar a CSV',
				
			},


			{
				extend: 'print',
				exportOptions: {
					columns: [1,2,3,4,5,6]
				},
				text: '<i class="fa fa-print"></i>',
				className: 'btn btn-info',
				titleAttr: 'Imprimir',
				
			},

			

		], 

		"ajax":{
			url:'../ajax/cliente.php?op=listar',
			type: 'get',
			dataType: 'json', 
			error: function(e){
				console.log(e.responseText);
			}
		},

		"bDestroy": true,
		"iDisplayLength":25, //cada cuantos registros realizamos la paginacion
		"order":[[1,"asc"]] //para ordenar los registros
    }).DataTable();
}


function mostrar_form(mostrar){
	limpiar();
	if(mostrar){
		$('#listado').hide();
		$('#formulario').show();
		$('#contenedor-cabecera').css("cssText", "display:none !important");
		$('#btnGuardar').prop("disabled",false);
	}else{
		$('#listado').show();
		$('#formulario').hide();
		$('#contenedor-cabecera').css("cssText", "display:block !important");

	}
}

function nuevo_editar(e){
	$('#cargandoModal').modal('show');
	
	e.preventDefault(); //no se activara la accion predeterminada del evento, osea del submit, se va hacer lo que yo le digo
	var formData = new FormData($('#form')[0]); //guardo todos los datos del form en formData
	$.ajax({
		url:'../ajax/cliente.php?op=nuevo_editar',//lugar a donde se envia los datos obtenidos del formulario
		type: "POST",
		data: formData, //estos son los datos que envio
		contentType:false,
		processData:false,

		success: function(datos){ 
			 $('#cargandoModal').fadeOut(300,function(){
                $('#cargandoModal').modal('hide');
             });

			if(datos=="¡Registro creado con exito!" || datos=="¡Registro editado con exito!"){
				Swal.fire({
					icon: 'success',
					title: datos,
					allowOutsideClick: false
				});
				mostrar_form(false);
				tabla.ajax.reload(); 
			}else{
				Swal.fire({
					icon: 'error',
					title: datos,
					allowOutsideClick: false
				});
			}
		}		
	});
}

function mostrar(id){
	$('#cargandoModal').modal('show');
	$.post("../ajax/cliente.php?op=mostrar", {idCliente: id}, function(data, status){
		try {
        /* Si el JSON está mal formado se generará una excepción */
        data = JSON.parse(data);
        if (data.error == true) {
          /* Si hemos enviado por JSON un error, lo notificamos */
          console.log('ERROR detectado:', data);
          return;
        }
        /* Trabajamos habitualmente con la respuesta */
        $('#cargandoModal').fadeOut(300,function(){
        	$('#cargandoModal').modal('hide');
        });
		mostrar_form(true);
		$('#idCliente').val(data.id_cliente);
		$('#apellidoNombre').val(data.apellido_nombre);
		$('#dni').val(data.dni);
		$('#barrio').val(data.barrio);
		$('#domicilio').val(data.domicilio);
		$('#telefono').val(data.telefono);
      } catch (error) {
        /* Si el JSON está mal, notificamos su contenido */
        console.log('ERROR. Recibido:', data);
      }
	})
}

function limpiar(){
	$('#idCliente').val("");
	$('#apellidoNombre').val("");
	$('#dni').val("");
	$('#telefono').val("");
	$('#domicilio').val("");
	$('#barrio').val("");
}

//formato dni con puntos
function format(input){
	var num = input.value.replace(/\./g,'');
	if(!isNaN(num)){
	num = num.toString().split('').reverse().join('').replace(/(?=\d*\.?)(\d{3})/g,'$1.');
	num = num.split('').reverse().join('').replace(/^[\.]/,'');
	input.value = num;
	}else{ 
		//me borra el caracter si no es numero
		input.value = input.value.replace(/[^\d\.]*/g,'');
	}
}




init();