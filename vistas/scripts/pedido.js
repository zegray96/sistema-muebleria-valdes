function init(){
	$('#tituloPagina').text('Sistema de Cobros - Pedidos');
	$("#wrapper").show(); 
	$("#preload").hide(); 
	listar_pedidos_para_hoy();
	mostrar_form(false);
	$('#textFiltro').text('Pedidos para hoy');
}

$('#btnNuevo').click(function() {
	mostrar_form(true);
});

$('#btnCancelar').click(function() {
	mostrar_form(false);
});

$("#form").on("submit", function(e){ 
	nuevo_editar(e);
});

$('#btnListarPorFecha').click(function() {
	listar_por_fecha();
});

$('#btnCancelarFecha').click(function() {
	$('#seleccionarFechas').modal('hide');
});

// Rango fechas
$('#div-rango-fechas .input-daterange').datepicker({        
	format: 'dd/mm/yyyy',        
	autoclose: true,     
	todayHighlight: true,   
	todayBtn: 'linked',
	language: 'es',
});  

$('#listarPor').on('change', function() {
	if ($('#listarPor').val()=="fecha") {
		$('#seleccionarFechas').modal('show');
		$('#listarPor').val("");
		$('#listarPor').selectpicker('refresh');
		$('#fechaIniListar').datepicker("setDate", '');
		$('#fechaFinListar').datepicker("setDate", '');

	}else{
		if ($('#listarPor').val()=="todos") {
			listar_todos();
			$('#listarPor').val("");
			$('#listarPor').selectpicker('refresh');
			$('#textFiltro').text('Todos');
		}else{
			if ($('#listarPor').val()=="pedidos para hoy") {
				listar_pedidos_para_hoy();
				$('#listarPor').val("");
				$('#listarPor').selectpicker('refresh');
				$('#textFiltro').text('Pedidos para hoy');
			}
		}	
	}
});

// formato fecha
function format_fecha(elEvento,idElem){
	var evento = elEvento || window.event;
	if(evento.keyCode == 8){
	} else {
		var fecha = document.getElementById(idElem);
		if(fecha.value.length == 2 || fecha.value.length == 5){
			fecha.value += "/";
		}
	}
}

//colocar la primer letra de palabra en mayusuclas 
function capitalizar(str) {
    return str.replace(/\w\S*/g, function(txt){
        return txt.charAt(0).toUpperCase() + txt.substr(1).toLowerCase();
    });
}



$("#fechaEntrega").keydown(function(event) {
  format_fecha(event,'fechaEntrega');
});

$('#apellidoNombre').blur(function() {
	$('#apellidoNombre').val(capitalizar($('#apellidoNombre').val()));
});

$('#domicilio').blur(function() {
	$('#domicilio').val(capitalizar($('#domicilio').val()));
});


function limpiar(){
	$('#estado').val('');
	$('#estado').selectpicker('refresh');
	$('#apellidoNombre').val('');
	$('#domicilio').val('');
	$('#telefono').val('');
	$('#descripcion').val('');
	$('#cantDesc').text('0');
	$('#fechaEntrega').val('');
}

function mostrar_form(mostrar){
	limpiar();
	if(mostrar){

		$('#fechaEntrega').datepicker({
			format: 'dd/mm/yyyy',
			todayBtn: 'linked',
			todayHighlight: true,
			autoclose: true,
			language: 'es',
		});

		$('#listado').hide();
		$('#formulario').show();
		$('#contenedor-cabecera').css("cssText", "display:none !important");
		$('#btnGuardar').prop("disabled",false);
		$('#div-ultima-mod').hide();
	}else{
		$('#listado').show();
		$('#formulario').hide();
		$('#contenedor-cabecera').css("cssText", "display:block !important");

	}
}

function listar_todos(){
	tabla=$('#tblListado').dataTable({
    	//utilizar en caso de que no haya server side
		"aProccessing": true, //Activamos el procesamiento de datatables
		"aServerSide": true, //Paginacion y filtrado relizados por el servidor
		//
		"searching": true, //no me muestra el boton buscar
        "language":{
        		"url": "../assets/json/Spanish.json"    
        },
        "scrollY": '48vh', //tamaño de barra de desplazamiento
		"scrollX": true, //muestra el scroll x
        "lengthMenu": [[25, 100, 1000], [25, 100, 1000]],
		dom: 'Bflrtip', //definimos los elementos del control de la tabla
		buttons:[
			{
				extend: 'excelHtml5',
				exportOptions: {
					columns: [1,2,3,4,5,6]
				},
				text: '<i class="fas fa-file-excel"></i>',
				className: 'btn btn-success',
				titleAttr: 'Exportar a Excel',
				
			},
			

			{
				extend: 'csvHtml5',
				exportOptions: {
					columns: [1,2,3,4,5,6]
				},
				text: '<i class="fa fa-file-csv"></i>',
				className: 'btn btn-warning',
				titleAttr: 'Exportar a CSV',
				
			},


			{
				extend: 'print',
				exportOptions: {
					columns: [1,2,3,4,5,6]
				},
				text: '<i class="fa fa-print"></i>',
				className: 'btn btn-info',
				titleAttr: 'Imprimir',
				
			},

			

		], 

		"ajax":{
			url:'../ajax/pedido.php?op=listar_todos',
			type: 'get',
			dataType: 'json', 
			error: function(e){
				console.log(e.responseText);
			}
		},

		"bDestroy": true,
		"iDisplayLength":25, //cada cuantos registros realizamos la paginacion
		"order":[[1,"asc"]] //para ordenar los registros
    }).DataTable();
}

function listar_pedidos_para_hoy(){
	tabla=$('#tblListado').dataTable({
    	//utilizar en caso de que no haya server side
		"aProccessing": true, //Activamos el procesamiento de datatables
		"aServerSide": true, //Paginacion y filtrado relizados por el servidor
		//
		"searching": true, //no me muestra el boton buscar
        "language":{
        		"url": "../assets/json/Spanish.json"    
        },
        "scrollY": '48vh', //tamaño de barra de desplazamiento
		"scrollX": true, //muestra el scroll x
        "lengthMenu": [[25, 100, 1000], [25, 100, 1000]],
		dom: 'Bflrtip', //definimos los elementos del control de la tabla
		buttons:[
			{
				extend: 'excelHtml5',
				exportOptions: {
					columns: [1,2,3,4,5,6]
				},
				text: '<i class="fas fa-file-excel"></i>',
				className: 'btn btn-success',
				titleAttr: 'Exportar a Excel',
				
			},
			

			{
				extend: 'csvHtml5',
				exportOptions: {
					columns: [1,2,3,4,5,6]
				},
				text: '<i class="fa fa-file-csv"></i>',
				className: 'btn btn-warning',
				titleAttr: 'Exportar a CSV',
				
			},


			{
				extend: 'print',
				exportOptions: {
					columns: [1,2,3,4,5,6]
				},
				text: '<i class="fa fa-print"></i>',
				className: 'btn btn-info',
				titleAttr: 'Imprimir',
				
			},

			

		], 

		"ajax":{
			url:'../ajax/pedido.php?op=listar_pedidos_para_hoy',
			type: 'get',
			dataType: 'json', 
			error: function(e){
				console.log(e.responseText);
			}
		},

		"bDestroy": true,
		"iDisplayLength":25, //cada cuantos registros realizamos la paginacion
		"order":[[1,"asc"]] //para ordenar los registros
    }).DataTable();
}


function listar_por_fecha(){
	var fechaIni=$('#fechaIniListar').val();
	var fechaFin=$('#fechaFinListar').val();

	if (fechaIni=="" || fechaFin=="") {
			Swal.fire({
				icon:'error',
				title:'¡Seleccione fechas!',
				allowOutsideClick: false
			});

	}else{
		tabla=$('#tblListado').dataTable({
	    	//utilizar en caso de que no haya server side
			"aProccessing": true, //Activamos el procesamiento de datatables
			"aServerSide": true, //Paginacion y filtrado relizados por el servidor
			//
			"searching": true, //no me muestra el boton buscar
			"language":{
				"url": "../assets/json/Spanish.json"    
			},
	        "scrollY": '48vh', //tamaño de barra de desplazamiento
			"scrollX": true, //muestra el scroll x
			"lengthMenu": [[25, 100, 1000], [25, 100, 1000]],
			dom: 'Bflrtip', //definimos los elementos del control de la tabla
			buttons:[
				{
					extend: 'excelHtml5',
					exportOptions: {
						columns: [1,2,3,4,5,6]
					},
					text: '<i class="fas fa-file-excel"></i>',
					className: 'btn btn-success',
					titleAttr: 'Exportar a Excel',
					
				},
				

				{
					extend: 'csvHtml5',
					exportOptions: {
						columns: [1,2,3,4,5,6]
					},
					text: '<i class="fa fa-file-csv"></i>',
					className: 'btn btn-warning',
					titleAttr: 'Exportar a CSV',
					
				},


				{
					extend: 'print',
					exportOptions: {
						columns: [1,2,3,4,5,6]
					},
					text: '<i class="fa fa-print"></i>',
					className: 'btn btn-info',
					titleAttr: 'Imprimir',
					
				},



			], 

			"ajax":{
				url:'../ajax/pedido.php?op=listar_por_fecha',
				data: {'fechaIni': fechaIni, 'fechaFin': fechaFin},
				type: 'get',
				dataType: 'json', 
				error: function(e){
					console.log(e.responseText);
				}
			},

			"bDestroy": true,
			"iDisplayLength":25, //cada cuantos registros realizamos la paginacion
			"order":[[1,"desc"]] //para ordenar los registros
		}).DataTable();

		$('#seleccionarFechas').modal('hide');
		$('#textFiltro').text(fechaIni+' - '+fechaFin);
	}

	
}

function nuevo_editar(e){
	$('#cargandoModal').modal('show');
	
	e.preventDefault(); //no se activara la accion predeterminada del evento, osea del submit, se va hacer lo que yo le digo
	var formData = new FormData($('#form')[0]); //guardo todos los datos del form en formData
	$.ajax({
		url:'../ajax/pedido.php?op=nuevo_editar',//lugar a donde se envia los datos obtenidos del formulario
		type: "POST",
		data: formData, //estos son los datos que envio
		contentType:false,
		processData:false,

		success: function(datos){ 
			 $('#cargandoModal').fadeOut(300,function(){
                $('#cargandoModal').modal('hide');
             });

			if(datos=="¡Registro creado con exito!" || datos=="¡Registro editado con exito!"){
				Swal.fire({
					icon: 'success',
					title: datos,
					allowOutsideClick: false
				});
				mostrar_form(false);
				tabla.ajax.reload(); 
			}else{
				Swal.fire({
					icon: 'error',
					title: datos,
					allowOutsideClick: false
				});
			}
		}		
	});
}

function mostrar(id){
	$('#cargandoModal').modal('show');
	$.post("../ajax/pedido.php?op=mostrar", {idPedido: id}, function(data, status){
		try {
        /* Si el JSON está mal formado se generará una excepción */
        data = JSON.parse(data);
        if (data.error == true) {
          /* Si hemos enviado por JSON un error, lo notificamos */
          console.log('ERROR detectado:', data);
          return;
        }
        /* Trabajamos habitualmente con la respuesta */
        $('#cargandoModal').fadeOut(300,function(){
        	$('#cargandoModal').modal('hide');
        });
		mostrar_form(true);
		$('#div-ultima-mod').show();
		$('#ultimaModNombre').text(data.ultimaModNombre);
		var fechaHora=data.fecha_hora_ultima_mod.split(" ");
		$("#ultimaModFecha").text(fechaHora[0]);
		$('#ultimaModHora').text(fechaHora[1]); 
		
		$('#idPedido').val(data.id_pedido);
		// Bootstrap Date Picker
		$('#fechaEntrega').datepicker("setDate", data.fecha_entrega);
		$('#estado').val(data.estado);
		$('#estado').selectpicker('refresh');
		$('#apellidoNombre').val(data.apellido_nombre);
		$('#domicilio').val(data.domicilio);
		$('#telefono').val(data.telefono);
		$("#cantDesc").text(data.descripcion.length);
		$('#descripcion').val(data.descripcion);

      } catch (error) {
        /* Si el JSON está mal, notificamos su contenido */
        console.log('ERROR. Recibido:', data);
      }
	})
}

init();