function init(){
	$('#tituloPagina').text('Sistema de Cobros - Login');
	$("#container").show(); 
	$("#preload").hide();
}

$('#btnVerClave').click(function (){
	
	var tipo = document.getElementById("clave");
    if(tipo.type == "password"){
        tipo.type = "text";
        $("#verClave").removeClass("fas fa-eye");
        $("#verClave").addClass("fas fa-eye-slash");
    }else{
        tipo.type = "password";
        $("#verClave").removeClass("fas fa-eye-slash");
        $("#verClave").addClass("fas fa-eye");
    }
})


init();