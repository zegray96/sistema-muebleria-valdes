var tabla;
function init(){
	$('#tituloPagina').text('Sistema de Cobros - Mi Cuenta');
	$("#wrapper").show(); 
	$("#preload").hide(); 
}

$('#btnCancelar').click(function() {
	$(location).attr("href","dashboard");
});

$('#btnCancelarModificarClave').click(function() {
	$('#modificarClave').modal('hide');
	$('#claveModificar').val("");
});

$("#form").on("submit", function(e){ 
	editar(e);
});

$("#formModificarClave").on("submit", function(e){ 
	modificar_clave(e);
});

//colocar la primer letra de palabra en mayusuclas 
function capitalizar(elemId) {
	var txt = $("#" + elemId).val().toLowerCase();
	$("#" + elemId).val(txt.replace(/^(.)|\s(.)/g, function($1) {
		return $1.toUpperCase(); }));
}

$("#apellidoNombre").keyup(function() {
	capitalizar('apellidoNombre');
});


function editar(e){
	$('#cargandoModal').modal('show');
	
	e.preventDefault(); //no se activara la accion predeterminada del evento, osea del submit, se va hacer lo que yo le digo
	var formData = new FormData($('#form')[0]); //guardo todos los datos del form en formData
	$.ajax({
		url:'../ajax/usuario.php?op=modificar_cuenta',//lugar a donde se envia los datos obtenidos del formulario
		type: "POST",
		data: formData, //estos son los datos que envio
		contentType:false,
		processData:false,

		success: function(datos){ 
			 $('#cargandoModal').fadeOut(300,function(){
                $('#cargandoModal').modal('hide');
             });

			if(datos=="¡Registro editado con exito! Los cambios se reflejarán luego de cerrar sesión..." || datos=="¡Registro editado con exito!"){
				Swal.fire({
					icon: 'success',
					title: datos,
					allowOutsideClick: false
				});
				 
			}else{
				Swal.fire({
					icon: 'error',
					title: datos,
					allowOutsideClick: false
				});
			}
		}		
	});
}


function modificar_clave(e){
	$('#cargandoModal').modal('show');
	
	e.preventDefault(); //no se activara la accion predeterminada del evento, osea del submit, se va hacer lo que yo le digo
	var formData = new FormData($('#formModificarClave')[0]); //guardo todos los datos del form en formData
	$.ajax({
		url:'../ajax/usuario.php?op=modificar_clave_mi_cuenta',//lugar a donde se envia los datos obtenidos del formulario
		type: "POST",
		data: formData, //estos son los datos que envio
		contentType:false,
		processData:false,

		success: function(datos){ 
			 $('#cargandoModal').fadeOut(300,function(){
                $('#cargandoModal').modal('hide');
             });

			if(datos=="¡Clave modificada con exito!"){
				Swal.fire({
					icon: 'success',
					title: datos,
					allowOutsideClick: false
				});
				$('#modificarClave').modal('hide');
				$('#claveModificar').val("");
				$('#repetirClaveModificar').val("");
				
			}else{
				Swal.fire({
					icon: 'error',
					title: datos,
					allowOutsideClick: false
				});
			}
		}		
	});
}





init();