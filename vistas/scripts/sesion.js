$('#frmLogin').on('submit',function(e){
	e.preventDefault();
	var formData = new FormData($('#frmLogin')[0]);
	$.ajax({
		type: 'POST', 
		url: '../ajax/sesion.php?op=iniciar_sesion',
		data: formData,
		contentType:false,
		processData:false,
		success: function (data) { 
			if(data != "null"){
				$(location).attr("href","dashboard");
			}else{
				Swal.fire({
				  icon: 'error',
				  title: '¡Usuario o Clave incorrectos!',
				  allowOutsideClick: false
				});
			}
		} 
	});

})

$('#btnCerrarSesion').click(function() {
  Swal.fire({
	  title: '¿Esta seguro de Cerrar Sesión?',
	  icon: 'question',
	  showCancelButton: true,
	  confirmButtonColor: '#3085d6',
	  cancelButtonColor: '#d33',
	  confirmButtonText: 'Aceptar',
	  cancelButtonText: 'Cancelar',
	  reverseButtons: true,
	  allowOutsideClick: false
	}).then((result) => {
	  if (result.isConfirmed) {
	    $.ajax({
		url: '../ajax/sesion.php?op=cerrar_sesion',
			success: function (data) { 
				$(location).attr("href","login");
			} 
		});
	  }
	});
})